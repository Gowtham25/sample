package com.cybg.treasury.migration.datatransformer.csvmodel;

/**
 * @author theo on 13/12/2016.
 */
public interface CSVInput {

  String getFileName();

  String getObjectType();
}
