package com.cybg.treasury.migration.datatransformer.transform.mxml;

import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.trade.CSVTrade;
import com.cybg.treasury.migration.murex.jaxb.model.Trade;

/**
 * @author theo on 13/12/2016.
 */
public interface MxML2CSVTradeTranslator<T extends CSVTrade> {

  T translate(String bundleName, Trade trade);

  List<String> getSupportedTypologies();
}
