package com.cybg.treasury.migration.datatransformer.camel.processor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVOutput;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.springframework.stereotype.Component;

/**
 * @author theo on 06/01/2017.
 */
@Component
public class BindyMarshalProcessor implements Processor {

  @Override
  @SuppressWarnings("unchecked")
  public void process(Exchange exchange) throws Exception {
    List<CSVOutput> csvOutputsMaps = exchange.getIn().getBody(List.class);
    if(!csvOutputsMaps.isEmpty()) {
      List<HashMap<String, CSVOutput>> csvOutputs = toListOfMaps(exchange.getIn().getBody(List.class));
      BindyCsvDataFormat bindyDF = new BindyCsvDataFormat(csvOutputsMaps.get(0).getClass().getPackage().getName());
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      bindyDF.marshal(exchange, csvOutputs, buffer);
      manipulateStream(buffer);
      exchange.getIn().setBody(buffer.toByteArray());
      exchange.getIn().setHeader(Exchange.FILE_NAME, csvOutputsMaps.get(0).getFileName());
    }
  }

  private List<HashMap<String, CSVOutput>> toListOfMaps(List<CSVOutput> csvOutputs) {
    List<HashMap<String, CSVOutput>> listOfMaps = new ArrayList<>();
    csvOutputs.forEach(csvOutput -> {
      HashMap<String, CSVOutput> innerHashMap = new HashMap<>();
      innerHashMap.put(csvOutput.getClass().getCanonicalName(), csvOutput);
      listOfMaps.add(innerHashMap);
    });
    return listOfMaps;
  }

  /**
   * The reason we need to manipulate the output stream before deserializing to csv is because camel-bindy does not escape
   * strings containing commas when the delimiter is comma. Hence as a work around we specify # separator, manually
   * replace commas with ;comma; and finally replace # with ,
   */
  private void manipulateStream(ByteArrayOutputStream buffer) throws IOException {
    String output = new String(buffer.toByteArray());
    output = output.replace(",", ";comma;");
    output = output.replace("#", ",");
    buffer.reset();
    buffer.write(output.getBytes());
  }
}
