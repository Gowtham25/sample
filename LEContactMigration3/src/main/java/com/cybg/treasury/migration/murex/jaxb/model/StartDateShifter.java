//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7-b41 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.02.09 at 04:35:52 PM GMT 
//


package com.cybg.treasury.migration.murex.jaxb.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}dateShifterLabel"/>
 *         &lt;element ref="{}businessObjectId"/>
 *         &lt;element ref="{}dateShifterDirection"/>
 *         &lt;element ref="{}shift"/>
 *         &lt;element ref="{}shifterAlgorithm"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dateShifterLabel",
    "businessObjectId",
    "dateShifterDirection",
    "shift",
    "shifterAlgorithm"
})
@XmlRootElement(name = "startDateShifter")
public class StartDateShifter {

    @XmlElement(required = true)
    protected String dateShifterLabel;
    @XmlElement(required = true)
    protected BusinessObjectId businessObjectId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String dateShifterDirection;
    @XmlElement(required = true)
    protected Shift shift;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "NCName")
    protected String shifterAlgorithm;

    /**
     * Gets the value of the dateShifterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateShifterLabel() {
        return dateShifterLabel;
    }

    /**
     * Sets the value of the dateShifterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateShifterLabel(String value) {
        this.dateShifterLabel = value;
    }

    /**
     * Gets the value of the businessObjectId property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessObjectId }
     *     
     */
    public BusinessObjectId getBusinessObjectId() {
        return businessObjectId;
    }

    /**
     * Sets the value of the businessObjectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessObjectId }
     *     
     */
    public void setBusinessObjectId(BusinessObjectId value) {
        this.businessObjectId = value;
    }

    /**
     * Gets the value of the dateShifterDirection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateShifterDirection() {
        return dateShifterDirection;
    }

    /**
     * Sets the value of the dateShifterDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateShifterDirection(String value) {
        this.dateShifterDirection = value;
    }

    /**
     * Gets the value of the shift property.
     * 
     * @return
     *     possible object is
     *     {@link Shift }
     *     
     */
    public Shift getShift() {
        return shift;
    }

    /**
     * Sets the value of the shift property.
     * 
     * @param value
     *     allowed object is
     *     {@link Shift }
     *     
     */
    public void setShift(Shift value) {
        this.shift = value;
    }

    /**
     * Gets the value of the shifterAlgorithm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShifterAlgorithm() {
        return shifterAlgorithm;
    }

    /**
     * Sets the value of the shifterAlgorithm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShifterAlgorithm(String value) {
        this.shifterAlgorithm = value;
    }

}
