package com.cybg.treasury.migration.datatransformer.csvmodel.refdata.sdioutput;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVOutput;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/**
 * @author theo on 05/01/2017.
 */
@CsvRecord(separator = "#", generateHeaderColumns = true)
public class SDIOutput implements CSVOutput {

  @DataField(pos = 1) private String Action = "NEW";
  @DataField(pos = 2) private String BeneficiaryShortName;
  @DataField(pos = 3) private String Reference;
  @DataField(pos = 4) private String Role;
  @DataField(pos = 5) private String Contact;
  @DataField(pos = 6) private String SDISettlementType;
  @DataField(pos = 7) private String Products;
  @DataField(pos = 8) private String PayRec;
  @DataField(pos = 9) private String Method;
  @DataField(pos = 10) private String ProcessingOrg;
  @DataField(pos = 11) private String Preferred;
  @DataField(pos = 12) private String Priority;
  @DataField(pos = 13) private String Currencies;
  @DataField(pos = 14) private String EffectiveFrom;
  @DataField(pos = 15) private String EffectiveTo;
  @DataField(pos = 16) private String ByTradeDate;
  @DataField(pos = 17) private String TradeCounterParty;
  @DataField(pos = 18) private String AgentCode;
  @DataField(pos = 19) private String AgentAccount;
  @DataField(pos = 20) private String AgentContact;
  @DataField(pos = 21) private String AgentName;
  @DataField(pos = 22) private String AgentGLAc;
  @DataField(pos = 23) private String AgentSubAccount;
  @DataField(pos = 24) private String AgentIdentifier;
  @DataField(pos = 25) private String MessageToAgent;
  @DataField(pos = 26) private String FirstIntermediaryPresent;
  @DataField(pos = 27) private String Intermediary1Code;
  @DataField(pos = 28) private String Intermediary1Account;
  @DataField(pos = 29) private String Intermediary1Contact;
  @DataField(pos = 30) private String Intermediary1GLAc;
  @DataField(pos = 31) private String Intermediary1Name;
  @DataField(pos = 32) private String Intermediary1SubAccount;
  @DataField(pos = 33) private String Intermediary1Identifier;
  @DataField(pos = 34) private String MessageToIntermediary1;
  @DataField(pos = 35) private String SecondIntermediaryPresent;
  @DataField(pos = 36) private String Intermediary2Code;
  @DataField(pos = 37) private String Intermediary2Account;
  @DataField(pos = 38) private String Intermediary2Contact;
  @DataField(pos = 39) private String Intermediary2SubAccount;
  @DataField(pos = 40) private String Intermediary2Identifier;
  @DataField(pos = 41) private String MessageToIntermediary2;
  @DataField(pos = 42) private String Direct;
  @DataField(pos = 43) private String DDA;
  @DataField(pos = 44) private String LinkSDI;
  @DataField(pos = 45) private String LinkedSDI;

  public void setAction(String action) {
    Action = action;
  }

  public void setBeneficiaryShortName(String beneficiaryShortName) {
    BeneficiaryShortName = beneficiaryShortName;
  }

  public void setReference(String reference) {
    Reference = reference;
  }

  public void setRole(String role) {
    Role = role;
  }

  public void setContact(String contact) {
    Contact = contact;
  }

  public void setSDISettlementType(String SDISettlementType) {
    this.SDISettlementType = SDISettlementType;
  }

  public void setProducts(String products) {
    Products = products;
  }

  public void setPayRec(String payRec) {
    PayRec = payRec;
  }

  public void setMethod(String method) {
    Method = method;
  }

  public void setProcessingOrg(String processingOrg) {
    ProcessingOrg = processingOrg;
  }

  public void setPreferred(String preferred) {
    Preferred = preferred;
  }

  public void setPriority(String priority) {
    Priority = priority;
  }

  public void setCurrencies(String currencies) {
    Currencies = currencies;
  }

  public void setEffectiveFrom(String effectiveFrom) {
    EffectiveFrom = effectiveFrom;
  }

  public void setEffectiveTo(String effectiveTo) {
    EffectiveTo = effectiveTo;
  }

  public void setByTradeDate(String byTradeDate) {
    ByTradeDate = byTradeDate;
  }

  public void setTradeCounterParty(String tradeCounterParty) {
    TradeCounterParty = tradeCounterParty;
  }

  public void setAgentCode(String agentCode) {
    AgentCode = agentCode;
  }

  public void setAgentAccount(String agentAccount) {
    AgentAccount = agentAccount;
  }

  public void setAgentContact(String agentContact) {
    AgentContact = agentContact;
  }

  public void setAgentName(String agentName) {
    AgentName = agentName;
  }

  public void setAgentGLAc(String agentGLAc) {
    AgentGLAc = agentGLAc;
  }

  public void setAgentSubAccount(String agentSubAccount) {
    AgentSubAccount = agentSubAccount;
  }

  public void setAgentIdentifier(String agentIdentifier) {
    AgentIdentifier = agentIdentifier;
  }

  public void setMessageToAgent(String messageToAgent) {
    MessageToAgent = messageToAgent;
  }

  public void setFirstIntermediaryPresent(String firstIntermediaryPresent) {
    FirstIntermediaryPresent = firstIntermediaryPresent;
  }

  public void setIntermediary1Code(String intermediary1Code) {
    Intermediary1Code = intermediary1Code;
  }

  public void setIntermediary1Account(String intermediary1Account) {
    Intermediary1Account = intermediary1Account;
  }

  public void setIntermediary1Contact(String intermediary1Contact) {
    Intermediary1Contact = intermediary1Contact;
  }

  public void setIntermediary1GLAc(String intermediary1GLAc) {
    Intermediary1GLAc = intermediary1GLAc;
  }

  public void setIntermediary1Name(String intermediary1Name) {
    Intermediary1Name = intermediary1Name;
  }

  public void setIntermediary1SubAccount(String intermediary1SubAccount) {
    Intermediary1SubAccount = intermediary1SubAccount;
  }

  public void setIntermediary1Identifier(String intermediary1Identifier) {
    Intermediary1Identifier = intermediary1Identifier;
  }

  public void setMessageToIntermediary1(String messageToIntermediary1) {
    MessageToIntermediary1 = messageToIntermediary1;
  }

  public void setSecondIntermediaryPresent(String secondIntermediaryPresent) {
    SecondIntermediaryPresent = secondIntermediaryPresent;
  }

  public void setIntermediary2Code(String intermediary2Code) {
    Intermediary2Code = intermediary2Code;
  }

  public void setIntermediary2Account(String intermediary2Account) {
    Intermediary2Account = intermediary2Account;
  }

  public void setIntermediary2Contact(String intermediary2Contact) {
    Intermediary2Contact = intermediary2Contact;
  }

  public void setIntermediary2SubAccount(String intermediary2SubAccount) {
    Intermediary2SubAccount = intermediary2SubAccount;
  }

  public void setIntermediary2Identifier(String intermediary2Identifier) {
    Intermediary2Identifier = intermediary2Identifier;
  }

  public void setMessageToIntermediary2(String messageToIntermediary2) {
    MessageToIntermediary2 = messageToIntermediary2;
  }

  public void setDirect(String direct) {
    Direct = direct;
  }

  public void setDDA(String DDA) {
    this.DDA = DDA;
  }

  public void setLinkSDI(String linkSDI) {
    LinkSDI = linkSDI;
  }

  public void setLinkedSDI(String linkedSDI) {
    LinkedSDI = linkedSDI;
  }

  @Override
  public String getFileName() {
    return "CALYPSOSDI_CSV.csv";
  }
}
