package com.cybg.treasury.migration.datatransformer.exception;

/**
 * @author theo on 09/01/2017.
 */
public class DataTransformerException extends RuntimeException {

  public DataTransformerException() {
  }

  public DataTransformerException(String s) {
    super(s);
  }
}
