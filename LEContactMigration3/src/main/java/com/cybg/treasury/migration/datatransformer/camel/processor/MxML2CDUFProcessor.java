package com.cybg.treasury.migration.datatransformer.camel.processor;

import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.trade.CSVTrade;
import com.cybg.treasury.migration.datatransformer.transform.mxml.MxMLMessage2CSVTranslator;
import com.cybg.treasury.migration.murex.jaxb.model.MxML;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author theo on 13/12/2016.
 */
@Component
public class MxML2CDUFProcessor implements Processor {

  private MxMLMessage2CSVTranslator mxMLMessage2CSVTranslator;

  @Autowired
  public MxML2CDUFProcessor(MxMLMessage2CSVTranslator mxMLMessage2CSVTranslator) {
    this.mxMLMessage2CSVTranslator = mxMLMessage2CSVTranslator;
  }

  @Override
  public void process(Exchange exchange) throws Exception {
    MxML mxML = exchange.getIn().getBody(MxML.class);
    List<CSVTrade> csvTrades = mxMLMessage2CSVTranslator.translate(mxML);
    exchange.getIn().setBody(csvTrades);
  }
}
