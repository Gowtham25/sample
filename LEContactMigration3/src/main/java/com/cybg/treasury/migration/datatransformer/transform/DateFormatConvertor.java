package com.cybg.treasury.migration.datatransformer.transform;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateFormatConvertor {
  public String convertDateFormat(String date, String fromFormat, String toFormat) {
    try {
      SimpleDateFormat fromFormatter = new SimpleDateFormat(fromFormat);
      SimpleDateFormat toFormatter = new SimpleDateFormat(toFormat);
      return toFormatter.format(fromFormatter.parse(date));
    } catch(ParseException e) {
      throw new RuntimeException(String.format("Failed to parse date %s from format %s to format %s", date, fromFormat, toFormat));
    }
  }
}
