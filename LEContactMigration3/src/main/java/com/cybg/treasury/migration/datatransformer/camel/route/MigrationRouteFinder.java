package com.cybg.treasury.migration.datatransformer.camel.route;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MigrationRouteFinder {
  private Map<String, MigrationRouteBuilder> migrationDataTypeToRouteMap = new HashMap<>();

  @Autowired
  public MigrationRouteFinder(List<MigrationRouteBuilder> routeBuilders) {
    populateRouteFinder(routeBuilders);
  }

  private void populateRouteFinder(List<MigrationRouteBuilder> routeBuilders) {
    routeBuilders.forEach(routeBuilder -> migrationDataTypeToRouteMap.put(routeBuilder.getSupportedType(),
      routeBuilder));
  }

  @SuppressWarnings("unchecked")
  public MigrationRouteBuilder find(String migrationDataType) {
    MigrationRouteBuilder migrationRouteBuilder = migrationDataTypeToRouteMap.get(migrationDataType);
    return migrationRouteBuilder;
  }
}
