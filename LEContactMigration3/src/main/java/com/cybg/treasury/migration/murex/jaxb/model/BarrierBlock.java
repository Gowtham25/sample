//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7-b41 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.02.09 at 04:35:52 PM GMT 
//


package com.cybg.treasury.migration.murex.jaxb.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}comment"/>
 *         &lt;choice>
 *           &lt;element ref="{}doubleBarrier"/>
 *           &lt;element ref="{}singleBarrier"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "comment",
    "doubleBarrier",
    "singleBarrier"
})
@XmlRootElement(name = "barrierBlock")
public class BarrierBlock {

    @XmlElement(required = true)
    protected Comment comment;
    protected DoubleBarrier doubleBarrier;
    protected SingleBarrier singleBarrier;

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link Comment }
     *     
     */
    public Comment getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Comment }
     *     
     */
    public void setComment(Comment value) {
        this.comment = value;
    }

    /**
     * Gets the value of the doubleBarrier property.
     * 
     * @return
     *     possible object is
     *     {@link DoubleBarrier }
     *     
     */
    public DoubleBarrier getDoubleBarrier() {
        return doubleBarrier;
    }

    /**
     * Sets the value of the doubleBarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DoubleBarrier }
     *     
     */
    public void setDoubleBarrier(DoubleBarrier value) {
        this.doubleBarrier = value;
    }

    /**
     * Gets the value of the singleBarrier property.
     * 
     * @return
     *     possible object is
     *     {@link SingleBarrier }
     *     
     */
    public SingleBarrier getSingleBarrier() {
        return singleBarrier;
    }

    /**
     * Sets the value of the singleBarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link SingleBarrier }
     *     
     */
    public void setSingleBarrier(SingleBarrier value) {
        this.singleBarrier = value;
    }

}
