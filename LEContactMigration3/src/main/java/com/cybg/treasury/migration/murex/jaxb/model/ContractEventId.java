//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7-b41 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.02.09 at 04:35:52 PM GMT 
//


package com.cybg.treasury.migration.murex.jaxb.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for contractEventId complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="contractEventId">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}contractEventId"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contractEventId", propOrder = {
    "contractEventId"
})
@XmlSeeAlso({
    ContractEventOriginReference.class,
    OriginContractEventReference.class,
    LastContractEventReference.class,
    ContractEventReference.class
})
public class ContractEventId {

    @XmlElement(required = true)
    protected ContractEventId2 contractEventId;

    /**
     * Gets the value of the contractEventId property.
     * 
     * @return
     *     possible object is
     *     {@link ContractEventId2 }
     *     
     */
    public ContractEventId2 getContractEventId() {
        return contractEventId;
    }

    /**
     * Sets the value of the contractEventId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractEventId2 }
     *     
     */
    public void setContractEventId(ContractEventId2 value) {
        this.contractEventId = value;
    }

}
