package com.cybg.treasury.migration.datatransformer.csvmodel.trade;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/**
 * @author theo on 16/12/2016.
 */
@CsvRecord(separator = ",", generateHeaderColumns = true)
public class FXNDF implements CSVTrade {

  @DataField(pos = 0) private String Action = "NEW";
  @DataField(pos = 1) private String TradeId;
  @DataField(pos = 2) private String ExternalRefId;
  @DataField(pos = 3) private String InternalRefId;
  @DataField(pos = 4) private String Counterparty;
  @DataField(pos = 5) private String CounterPartyRole;
  @DataField(pos = 6) private String Book;
  @DataField(pos = 7) private String BundleType;
  @DataField(pos = 8) private String BundleName;
  @DataField(pos = 9) private String BuySell;
  @DataField(pos = 10) private String TradeSettlementCurrency;
  @DataField(pos = 11) private String Currency;
  @DataField(pos = 12) private String MirrorBook;
  @DataField(pos = 13) private String TradeDate;
  @DataField(pos = 14) private String TraderName;
  @DataField(pos = 15) private String SalesPerson;
  @DataField(pos = 16) private String Comment;
  @DataField(pos = 17) private String SettlementDate;
  @DataField(pos = 18) private String ProductType;
  @DataField(pos = 19) private String ProductSubType;
  @DataField(pos = 20) private String PrimaryCurrency;
  @DataField(pos = 21) private String PrimaryAmount;
  @DataField(pos = 22) private String SecondaryCurrency;
  @DataField(pos = 23) private String SecondaryAmount;
  @DataField(pos = 24) private String NegotiableCurrency;
  @DataField(pos = 25) private String SpotRate;
  @DataField(pos = 26) private String ForwardPoints;
  @DataField(pos = 27) private String ForwardRate;
  @DataField(pos = 28) private String MarginPoint;
  @DataField(pos = 29) private String FinalRate;
  @DataField(pos = 30) private String ResetDate;
  @DataField(pos = 31) private String FixingSplitB;
  @DataField(pos = 32) private String FXResetRate;
  @DataField(pos = 33) private String QuantoCurrency;
  @DataField(pos = 34) private String QuantoResetDate;
  @DataField(pos = 35) private String QuantoFixingSplitB;
  @DataField(pos = 36) private String QuantoFXResetRate;
  @DataField(pos = 37) private String FXSettlementCurrency;
  @DataField(pos = 38) private String Back2BackTradeB;
  @DataField(pos = 39) private String CurrencySplitTradeB;
  @DataField(pos = 40) private String SpotRiskTransferB;
  @DataField(pos = 41) private String FwdRiskTransferB;
  @DataField(pos = 42) private String B2BTransferBook;
  @DataField(pos = 43) private String B2BTradeTransferMarginB;
  @DataField(pos = 44) private String B2BTradePVForwardAmountB;
  @DataField(pos = 45) private String CurrencySplitBaseBook;
  @DataField(pos = 46) private String CurrencySplitQuotingBook;
  @DataField(pos = 47) private String SpotRiskTransferBook;
  @DataField(pos = 48) private String SpotRiskTransferUseNotionalB;
  @DataField(pos = 49) private String FwdRiskTransferBook;
  @DataField(pos = 50) private String RollCounterparty;
  @DataField(pos = 51) private String RollBook;
  @DataField(pos = 52) private String RollDateTime;
  @DataField(pos = 53) private String RollNegotiatedCcy;
  @DataField(pos = 54) private String RollAmount;
  @DataField(pos = 55) private String RollReason;
  @DataField(pos = 56) private String RollNovateTo;
  @DataField(pos = 57) private String RolloverType;
  @DataField(pos = 58) private String RollFundingRate;
  @DataField(pos = 59) private String RollFundingMarging;
  @DataField(pos = 60) private String RollTerminationDate;
  @DataField(pos = 61) private String RollCurrentSpot;
  @DataField(pos = 62) private String RollCustomerNearRate;
  @DataField(pos = 63) private String RollCustomerFarRate;
  @DataField(pos = 64) private String RollNearPoints;
  @DataField(pos = 65) private String RollFarPoints;
  @DataField(pos = 66) private String RollForwardRate;
  @DataField(pos = 67) private String RollFarMarginPoints;
  @DataField(pos = 68) private String RollSalesMarginPoints;
  @DataField(pos = 69) private String RollFXReset;
  @DataField(pos = 70) private String RollFXResetDate;
  @DataField(pos = 71) private String RollQuantoFXResetDate;
  @DataField(pos = 72) private String RollQuantoFXResetName;

  public String getAction() {
    return Action;
  }

  public void setAction(String action) {
    Action = action;
  }

  public String getTradeId() {
    return TradeId;
  }

  public void setTradeId(String tradeId) {
    TradeId = tradeId;
  }

  public String getExternalRefId() {
    return ExternalRefId;
  }

  public void setExternalRefId(String externalRefId) {
    ExternalRefId = externalRefId;
  }

  public String getInternalRefId() {
    return InternalRefId;
  }

  public void setInternalRefId(String internalRefId) {
    InternalRefId = internalRefId;
  }

  public String getCounterparty() {
    return Counterparty;
  }

  public void setCounterparty(String counterparty) {
    Counterparty = counterparty;
  }

  public String getCounterPartyRole() {
    return CounterPartyRole;
  }

  public void setCounterPartyRole(String counterPartyRole) {
    CounterPartyRole = counterPartyRole;
  }

  public String getBook() {
    return Book;
  }

  public void setBook(String book) {
    Book = book;
  }

  public String getBundleType() {
    return BundleType;
  }

  public void setBundleType(String bundleType) {
    BundleType = bundleType;
  }

  public String getBundleName() {
    return BundleName;
  }

  public void setBundleName(String bundleName) {
    BundleName = bundleName;
  }

  public String getBuySell() {
    return BuySell;
  }

  public void setBuySell(String buySell) {
    BuySell = buySell;
  }

  public String getTradeSettlementCurrency() {
    return TradeSettlementCurrency;
  }

  public void setTradeSettlementCurrency(String tradeSettlementCurrency) {
    TradeSettlementCurrency = tradeSettlementCurrency;
  }

  public String getCurrency() {
    return Currency;
  }

  public void setCurrency(String currency) {
    Currency = currency;
  }

  public String getMirrorBook() {
    return MirrorBook;
  }

  public void setMirrorBook(String mirrorBook) {
    MirrorBook = mirrorBook;
  }

  public String getTradeDate() {
    return TradeDate;
  }

  public void setTradeDate(String tradeDate) {
    TradeDate = tradeDate;
  }

  public String getTraderName() {
    return TraderName;
  }

  public void setTraderName(String traderName) {
    TraderName = traderName;
  }

  public String getSalesPerson() {
    return SalesPerson;
  }

  public void setSalesPerson(String salesPerson) {
    SalesPerson = salesPerson;
  }

  public String getComment() {
    return Comment;
  }

  public void setComment(String comment) {
    Comment = comment;
  }

  public String getSettlementDate() {
    return SettlementDate;
  }

  public void setSettlementDate(String settlementDate) {
    SettlementDate = settlementDate;
  }

  public String getProductType() {
    return ProductType;
  }

  public void setProductType(String productType) {
    ProductType = productType;
  }

  public String getProductSubType() {
    return ProductSubType;
  }

  public void setProductSubType(String productSubType) {
    ProductSubType = productSubType;
  }

  public String getPrimaryCurrency() {
    return PrimaryCurrency;
  }

  public void setPrimaryCurrency(String primaryCurrency) {
    PrimaryCurrency = primaryCurrency;
  }

  public String getPrimaryAmount() {
    return PrimaryAmount;
  }

  public void setPrimaryAmount(String primaryAmount) {
    PrimaryAmount = primaryAmount;
  }

  public String getSecondaryCurrency() {
    return SecondaryCurrency;
  }

  public void setSecondaryCurrency(String secondaryCurrency) {
    SecondaryCurrency = secondaryCurrency;
  }

  public String getSecondaryAmount() {
    return SecondaryAmount;
  }

  public void setSecondaryAmount(String secondaryAmount) {
    SecondaryAmount = secondaryAmount;
  }

  public String getNegotiableCurrency() {
    return NegotiableCurrency;
  }

  public void setNegotiableCurrency(String negotiableCurrency) {
    NegotiableCurrency = negotiableCurrency;
  }

  public String getSpotRate() {
    return SpotRate;
  }

  public void setSpotRate(String spotRate) {
    SpotRate = spotRate;
  }

  public String getForwardPoints() {
    return ForwardPoints;
  }

  public void setForwardPoints(String forwardPoints) {
    ForwardPoints = forwardPoints;
  }

  public String getForwardRate() {
    return ForwardRate;
  }

  public void setForwardRate(String forwardRate) {
    ForwardRate = forwardRate;
  }

  public String getMarginPoint() {
    return MarginPoint;
  }

  public void setMarginPoint(String marginPoint) {
    MarginPoint = marginPoint;
  }

  public String getFinalRate() {
    return FinalRate;
  }

  public void setFinalRate(String finalRate) {
    FinalRate = finalRate;
  }

  public String getResetDate() {
    return ResetDate;
  }

  public void setResetDate(String resetDate) {
    ResetDate = resetDate;
  }

  public String getFixingSplitB() {
    return FixingSplitB;
  }

  public void setFixingSplitB(String fixingSplitB) {
    FixingSplitB = fixingSplitB;
  }

  public String getFXResetRate() {
    return FXResetRate;
  }

  public void setFXResetRate(String FXResetRate) {
    this.FXResetRate = FXResetRate;
  }

  public String getQuantoCurrency() {
    return QuantoCurrency;
  }

  public void setQuantoCurrency(String quantoCurrency) {
    QuantoCurrency = quantoCurrency;
  }

  public String getQuantoResetDate() {
    return QuantoResetDate;
  }

  public void setQuantoResetDate(String quantoResetDate) {
    QuantoResetDate = quantoResetDate;
  }

  public String getQuantoFixingSplitB() {
    return QuantoFixingSplitB;
  }

  public void setQuantoFixingSplitB(String quantoFixingSplitB) {
    QuantoFixingSplitB = quantoFixingSplitB;
  }

  public String getQuantoFXResetRate() {
    return QuantoFXResetRate;
  }

  public void setQuantoFXResetRate(String quantoFXResetRate) {
    QuantoFXResetRate = quantoFXResetRate;
  }

  public String getFXSettlementCurrency() {
    return FXSettlementCurrency;
  }

  public void setFXSettlementCurrency(String FXSettlementCurrency) {
    this.FXSettlementCurrency = FXSettlementCurrency;
  }

  public String getBack2BackTradeB() {
    return Back2BackTradeB;
  }

  public void setBack2BackTradeB(String back2BackTradeB) {
    Back2BackTradeB = back2BackTradeB;
  }

  public String getCurrencySplitTradeB() {
    return CurrencySplitTradeB;
  }

  public void setCurrencySplitTradeB(String currencySplitTradeB) {
    CurrencySplitTradeB = currencySplitTradeB;
  }

  public String getSpotRiskTransferB() {
    return SpotRiskTransferB;
  }

  public void setSpotRiskTransferB(String spotRiskTransferB) {
    SpotRiskTransferB = spotRiskTransferB;
  }

  public String getFwdRiskTransferB() {
    return FwdRiskTransferB;
  }

  public void setFwdRiskTransferB(String fwdRiskTransferB) {
    FwdRiskTransferB = fwdRiskTransferB;
  }

  public String getB2BTransferBook() {
    return B2BTransferBook;
  }

  public void setB2BTransferBook(String b2BTransferBook) {
    B2BTransferBook = b2BTransferBook;
  }

  public String getB2BTradeTransferMarginB() {
    return B2BTradeTransferMarginB;
  }

  public void setB2BTradeTransferMarginB(String b2BTradeTransferMarginB) {
    B2BTradeTransferMarginB = b2BTradeTransferMarginB;
  }

  public String getB2BTradePVForwardAmountB() {
    return B2BTradePVForwardAmountB;
  }

  public void setB2BTradePVForwardAmountB(String b2BTradePVForwardAmountB) {
    B2BTradePVForwardAmountB = b2BTradePVForwardAmountB;
  }

  public String getCurrencySplitBaseBook() {
    return CurrencySplitBaseBook;
  }

  public void setCurrencySplitBaseBook(String currencySplitBaseBook) {
    CurrencySplitBaseBook = currencySplitBaseBook;
  }

  public String getCurrencySplitQuotingBook() {
    return CurrencySplitQuotingBook;
  }

  public void setCurrencySplitQuotingBook(String currencySplitQuotingBook) {
    CurrencySplitQuotingBook = currencySplitQuotingBook;
  }

  public String getSpotRiskTransferBook() {
    return SpotRiskTransferBook;
  }

  public void setSpotRiskTransferBook(String spotRiskTransferBook) {
    SpotRiskTransferBook = spotRiskTransferBook;
  }

  public String getSpotRiskTransferUseNotionalB() {
    return SpotRiskTransferUseNotionalB;
  }

  public void setSpotRiskTransferUseNotionalB(String spotRiskTransferUseNotionalB) {
    SpotRiskTransferUseNotionalB = spotRiskTransferUseNotionalB;
  }

  public String getFwdRiskTransferBook() {
    return FwdRiskTransferBook;
  }

  public void setFwdRiskTransferBook(String fwdRiskTransferBook) {
    FwdRiskTransferBook = fwdRiskTransferBook;
  }

  public String getRollCounterparty() {
    return RollCounterparty;
  }

  public void setRollCounterparty(String rollCounterparty) {
    RollCounterparty = rollCounterparty;
  }

  public String getRollBook() {
    return RollBook;
  }

  public void setRollBook(String rollBook) {
    RollBook = rollBook;
  }

  public String getRollDateTime() {
    return RollDateTime;
  }

  public void setRollDateTime(String rollDateTime) {
    RollDateTime = rollDateTime;
  }

  public String getRollNegotiatedCcy() {
    return RollNegotiatedCcy;
  }

  public void setRollNegotiatedCcy(String rollNegotiatedCcy) {
    RollNegotiatedCcy = rollNegotiatedCcy;
  }

  public String getRollAmount() {
    return RollAmount;
  }

  public void setRollAmount(String rollAmount) {
    RollAmount = rollAmount;
  }

  public String getRollReason() {
    return RollReason;
  }

  public void setRollReason(String rollReason) {
    RollReason = rollReason;
  }

  public String getRollNovateTo() {
    return RollNovateTo;
  }

  public void setRollNovateTo(String rollNovateTo) {
    RollNovateTo = rollNovateTo;
  }

  public String getRolloverType() {
    return RolloverType;
  }

  public void setRolloverType(String rolloverType) {
    RolloverType = rolloverType;
  }

  public String getRollFundingRate() {
    return RollFundingRate;
  }

  public void setRollFundingRate(String rollFundingRate) {
    RollFundingRate = rollFundingRate;
  }

  public String getRollFundingMarging() {
    return RollFundingMarging;
  }

  public void setRollFundingMarging(String rollFundingMarging) {
    RollFundingMarging = rollFundingMarging;
  }

  public String getRollTerminationDate() {
    return RollTerminationDate;
  }

  public void setRollTerminationDate(String rollTerminationDate) {
    RollTerminationDate = rollTerminationDate;
  }

  public String getRollCurrentSpot() {
    return RollCurrentSpot;
  }

  public void setRollCurrentSpot(String rollCurrentSpot) {
    RollCurrentSpot = rollCurrentSpot;
  }

  public String getRollCustomerNearRate() {
    return RollCustomerNearRate;
  }

  public void setRollCustomerNearRate(String rollCustomerNearRate) {
    RollCustomerNearRate = rollCustomerNearRate;
  }

  public String getRollCustomerFarRate() {
    return RollCustomerFarRate;
  }

  public void setRollCustomerFarRate(String rollCustomerFarRate) {
    RollCustomerFarRate = rollCustomerFarRate;
  }

  public String getRollNearPoints() {
    return RollNearPoints;
  }

  public void setRollNearPoints(String rollNearPoints) {
    RollNearPoints = rollNearPoints;
  }

  public String getRollFarPoints() {
    return RollFarPoints;
  }

  public void setRollFarPoints(String rollFarPoints) {
    RollFarPoints = rollFarPoints;
  }

  public String getRollForwardRate() {
    return RollForwardRate;
  }

  public void setRollForwardRate(String rollForwardRate) {
    RollForwardRate = rollForwardRate;
  }

  public String getRollFarMarginPoints() {
    return RollFarMarginPoints;
  }

  public void setRollFarMarginPoints(String rollFarMarginPoints) {
    RollFarMarginPoints = rollFarMarginPoints;
  }

  public String getRollSalesMarginPoints() {
    return RollSalesMarginPoints;
  }

  public void setRollSalesMarginPoints(String rollSalesMarginPoints) {
    RollSalesMarginPoints = rollSalesMarginPoints;
  }

  public String getRollFXReset() {
    return RollFXReset;
  }

  public void setRollFXReset(String rollFXReset) {
    RollFXReset = rollFXReset;
  }

  public String getRollFXResetDate() {
    return RollFXResetDate;
  }

  public void setRollFXResetDate(String rollFXResetDate) {
    RollFXResetDate = rollFXResetDate;
  }

  public String getRollQuantoFXResetDate() {
    return RollQuantoFXResetDate;
  }

  public void setRollQuantoFXResetDate(String rollQuantoFXResetDate) {
    RollQuantoFXResetDate = rollQuantoFXResetDate;
  }

  public String getRollQuantoFXResetName() {
    return RollQuantoFXResetName;
  }

  public void setRollQuantoFXResetName(String rollQuantoFXResetName) {
    RollQuantoFXResetName = rollQuantoFXResetName;
  }

  @Override
  public String getCsvFileName() {
    return "FX_NDF.csv";
  }
}
