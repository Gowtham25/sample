package com.cybg.treasury.migration.datatransformer.csvmodel.refdata.legalentityinput;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVInput;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.springframework.stereotype.Component;

/**
 * @author theo on 05/01/2017.
 */
@CsvRecord(separator = ",", skipFirstLine = true)
@Component
public class LegalEntityInput implements CSVInput {

  @DataField(pos = 1)  private String client_ID;
  @DataField(pos = 2)  private String long_name;
  @DataField(pos = 3)  private String short_name;
  @DataField(pos = 4)  private String status_code;
  @DataField(pos = 5)  private String legality;
  @DataField(pos = 6)  private String business_type;
  @DataField(pos = 7)  private String country_code;
  @DataField(pos = 8)  private String country_of_risk;
  @DataField(pos = 9)  private String netting_city_code;
  @DataField(pos = 10) private String CRS_rating_allowable;
  @DataField(pos = 11) private String is_trading_unit;
  @DataField(pos = 12) private String is_financial_for_settlement;

  public LegalEntityInput() {
  }

  public LegalEntityInput(String client_ID, String long_name, String short_name, String status_code, String legality, String business_type, String country_code, String country_of_risk, String netting_city_code, String CRS_rating_allowable, String is_trading_unit, String is_financial_for_settlement) {
    this.client_ID = client_ID;
    this.long_name = long_name;
    this.short_name = short_name;
    this.status_code = status_code;
    this.legality = legality;
    this.business_type = business_type;
    this.country_code = country_code;
    this.country_of_risk = country_of_risk;
    this.netting_city_code = netting_city_code;
    this.CRS_rating_allowable = CRS_rating_allowable;
    this.is_trading_unit = is_trading_unit;
    this.is_financial_for_settlement = is_financial_for_settlement;
  }

  public String getClientID() {
    return client_ID;
  }

  public String getLongName() {
    return long_name;
  }

  public String getShortName() {
    return short_name;
  }

  public String getStatusCode() {
    return status_code;
  }

  public String getLegality() {
    return legality;
  }

  public String getBusinessType() {
    return business_type;
  }

  public String getCountryCode() {
    return country_code;
  }

  public String getCountryOfRisk() {
    return country_of_risk;
  }

  public String getNettingCityCode() {
    return netting_city_code;
  }

  public String getCRSRatingAllowable() {
    return CRS_rating_allowable;
  }

  public String getIsTradingUnit() {
    return is_trading_unit;
  }

  public String getIsFinancialForSettlement() {
    return is_financial_for_settlement;
  }

  @Override
  public String getFileName() {
    return "LegalEntity.csv";
  }

  @Override
  public String getObjectType() {
    return "LegalEntity";
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    LegalEntityInput that = (LegalEntityInput)o;

    if(client_ID != null ? !client_ID.equals(that.client_ID) : that.client_ID != null) return false;
    if(long_name != null ? !long_name.equals(that.long_name) : that.long_name != null) return false;
    if(short_name != null ? !short_name.equals(that.short_name) : that.short_name != null) return false;
    if(status_code != null ? !status_code.equals(that.status_code) : that.status_code != null) return false;
    if(legality != null ? !legality.equals(that.legality) : that.legality != null) return false;
    if(business_type != null ? !business_type.equals(that.business_type) : that.business_type != null) return false;
    if(country_code != null ? !country_code.equals(that.country_code) : that.country_code != null) return false;
    if(country_of_risk != null ? !country_of_risk.equals(that.country_of_risk) : that.country_of_risk != null)
      return false;
    if(netting_city_code != null ? !netting_city_code.equals(that.netting_city_code) : that.netting_city_code != null)
      return false;
    if(CRS_rating_allowable != null ? !CRS_rating_allowable.equals(that.CRS_rating_allowable) : that.CRS_rating_allowable != null)
      return false;
    if(is_trading_unit != null ? !is_trading_unit.equals(that.is_trading_unit) : that.is_trading_unit != null)
      return false;
    return is_financial_for_settlement != null ? is_financial_for_settlement.equals(that.is_financial_for_settlement) : that.is_financial_for_settlement == null;

  }

  @Override
  public int hashCode() {
    int result = client_ID != null ? client_ID.hashCode() : 0;
    result = 31 * result + (long_name != null ? long_name.hashCode() : 0);
    result = 31 * result + (short_name != null ? short_name.hashCode() : 0);
    result = 31 * result + (status_code != null ? status_code.hashCode() : 0);
    result = 31 * result + (legality != null ? legality.hashCode() : 0);
    result = 31 * result + (business_type != null ? business_type.hashCode() : 0);
    result = 31 * result + (country_code != null ? country_code.hashCode() : 0);
    result = 31 * result + (country_of_risk != null ? country_of_risk.hashCode() : 0);
    result = 31 * result + (netting_city_code != null ? netting_city_code.hashCode() : 0);
    result = 31 * result + (CRS_rating_allowable != null ? CRS_rating_allowable.hashCode() : 0);
    result = 31 * result + (is_trading_unit != null ? is_trading_unit.hashCode() : 0);
    result = 31 * result + (is_financial_for_settlement != null ? is_financial_for_settlement.hashCode() : 0);
    return result;
  }
}
