package com.cybg.treasury.migration.datatransformer.transform.csv;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVInput;
import com.cybg.treasury.migration.datatransformer.csvmodel.CSVOutput;

public interface CsvToCsvDataTransformer<T1 extends CSVInput, T2 extends CSVOutput> {

  T2 transform(T1 csvInput);

  String getSupportedType();
}