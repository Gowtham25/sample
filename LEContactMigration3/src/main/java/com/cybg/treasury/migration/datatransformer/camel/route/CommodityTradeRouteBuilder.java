package com.cybg.treasury.migration.datatransformer.camel.route;

import java.util.concurrent.CountDownLatch;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CommodityTradeRouteBuilder extends RouteBuilder implements MigrationRouteBuilder {
  private final String inputDirectory;
  private final String outputDirectory;
  private final Processor bindyUnmarshalProcessor;
  private final Processor csv2CDUFProcessor;
  private final Processor bindyMarshalProcessor;
  private final Processor commodityFileProcessor;
  private final Processor commodityTradeProcessor;
  private final Processor errorLoggingProcessor;
  private final CountDownLatch latch;

  @Autowired
  public CommodityTradeRouteBuilder(@Value("#{environment['commodity.input.dir']}") String inputDirectory,
                                     @Value("#{environment['migrationdata.output.dir']}") String outputDirectory,
                                     Processor bindyUnmarshalProcessor,
                                     Processor csv2CDUFProcessor,
                                     Processor bindyMarshalProcessor,
                                     Processor commodityFileProcessor,
                                     Processor commodityTradeProcessor,
                                     Processor errorLoggingProcessor,
                                     CountDownLatch latch) {
    this.inputDirectory = inputDirectory;
    this.outputDirectory = outputDirectory;
    this.bindyUnmarshalProcessor = bindyUnmarshalProcessor;
    this.csv2CDUFProcessor = csv2CDUFProcessor;
    this.bindyMarshalProcessor = bindyMarshalProcessor;
    this.commodityFileProcessor = commodityFileProcessor;
    this.commodityTradeProcessor = commodityTradeProcessor;
    this.errorLoggingProcessor = errorLoggingProcessor;
    this.latch = latch;
  }

  @Override
  public void configure() throws Exception {
    onException(Exception.class)
      .logStackTrace(true)
      .process(errorLoggingProcessor)
      .stop();

    from(inputDirectory + "?move=.done&moveFailed=.error")
      .process(commodityFileProcessor)
      .process(bindyUnmarshalProcessor)
      .process(commodityTradeProcessor)
      .split(simple("${body}"))
      .process(csv2CDUFProcessor)
      .process(bindyMarshalProcessor)
      .to(outputDirectory)
      .process(exchange -> latch.countDown());
  }

  @Override
  public String getSupportedType() {
    return "Commodity";
  }
}
