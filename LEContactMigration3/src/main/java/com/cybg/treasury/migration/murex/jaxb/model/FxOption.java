//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7-b41 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.02.09 at 04:35:52 PM GMT 
//


package com.cybg.treasury.migration.murex.jaxb.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}fxBarrierOption" minOccurs="0"/>
 *         &lt;element ref="{}fxVanillaOption" minOccurs="0"/>
 *         &lt;sequence minOccurs="0">
 *           &lt;element ref="{}settlement"/>
 *           &lt;element ref="{}inputData"/>
 *         &lt;/sequence>
 *         &lt;element ref="{}deltaHedge" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fxBarrierOption",
    "fxVanillaOption",
    "settlement",
    "inputData",
    "deltaHedge"
})
@XmlRootElement(name = "fxOption")
public class FxOption {

    protected FxBarrierOption fxBarrierOption;
    protected FxVanillaOption fxVanillaOption;
    protected Settlement settlement;
    protected InputData inputData;
    protected DeltaHedge deltaHedge;

    /**
     * Gets the value of the fxBarrierOption property.
     * 
     * @return
     *     possible object is
     *     {@link FxBarrierOption }
     *     
     */
    public FxBarrierOption getFxBarrierOption() {
        return fxBarrierOption;
    }

    /**
     * Sets the value of the fxBarrierOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link FxBarrierOption }
     *     
     */
    public void setFxBarrierOption(FxBarrierOption value) {
        this.fxBarrierOption = value;
    }

    /**
     * Gets the value of the fxVanillaOption property.
     * 
     * @return
     *     possible object is
     *     {@link FxVanillaOption }
     *     
     */
    public FxVanillaOption getFxVanillaOption() {
        return fxVanillaOption;
    }

    /**
     * Sets the value of the fxVanillaOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link FxVanillaOption }
     *     
     */
    public void setFxVanillaOption(FxVanillaOption value) {
        this.fxVanillaOption = value;
    }

    /**
     * Gets the value of the settlement property.
     * 
     * @return
     *     possible object is
     *     {@link Settlement }
     *     
     */
    public Settlement getSettlement() {
        return settlement;
    }

    /**
     * Sets the value of the settlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link Settlement }
     *     
     */
    public void setSettlement(Settlement value) {
        this.settlement = value;
    }

    /**
     * Gets the value of the inputData property.
     * 
     * @return
     *     possible object is
     *     {@link InputData }
     *     
     */
    public InputData getInputData() {
        return inputData;
    }

    /**
     * Sets the value of the inputData property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputData }
     *     
     */
    public void setInputData(InputData value) {
        this.inputData = value;
    }

    /**
     * Gets the value of the deltaHedge property.
     * 
     * @return
     *     possible object is
     *     {@link DeltaHedge }
     *     
     */
    public DeltaHedge getDeltaHedge() {
        return deltaHedge;
    }

    /**
     * Sets the value of the deltaHedge property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeltaHedge }
     *     
     */
    public void setDeltaHedge(DeltaHedge value) {
        this.deltaHedge = value;
    }

}
