package com.cybg.treasury.migration.datatransformer.transform.mxml;

import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.trade.CashLoanDeposit;
import com.cybg.treasury.migration.murex.jaxb.model.Trade;

/**
 * @author theo on 14/12/2016.
 */
public class CashLoanDepositTradeTranslator extends AbstractTradeTranslator<CashLoanDeposit> {
  @Override
  public CashLoanDeposit translate(String bundleName, Trade trade) {
   CashLoanDeposit csvTrade = new CashLoanDeposit();
    csvTrade.setBundleName(bundleName);
    //TODO implement
    return csvTrade;
  }

  @Override
  public List<String> getSupportedTypologies() {
    return null;
  }
}
