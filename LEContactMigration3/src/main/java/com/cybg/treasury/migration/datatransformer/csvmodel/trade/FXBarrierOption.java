package com.cybg.treasury.migration.datatransformer.csvmodel.trade;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/**
 * @author theo on 14/12/2016.
 */
@CsvRecord(separator = ",", generateHeaderColumns = true)
public class FXBarrierOption implements CSVTrade {

  @DataField(pos = 0) private String Action = "NEW";
  @DataField(pos = 1) private String ExternalReference;
  @DataField(pos = 2) private String InternalReference;
  @DataField(pos = 3) private String Book;
  @DataField(pos = 4) private String Counterparty;
  @DataField(pos = 5) private String CounterPartyRole = "CounterParty";
  @DataField(pos = 6) private String BundleType = "PricingSheet";
  @DataField(pos = 7) private String BundleName;
  @DataField(pos = 8) private String BuySell;
  @DataField(pos = 9) private String Notional;
  @DataField(pos = 10) private String TradeDatetime;
  @DataField(pos = 11) private String TradeSettleDate;
  @DataField(pos = 12) private String StartDate;
  @DataField(pos = 13) private String MaturityDate;
  @DataField(pos = 14) private String Trader;
  @DataField(pos = 15) private String SalesPerson;
  @DataField(pos = 16) private String TemplateName;
  @DataField(pos = 17) private String Comments;
  @DataField(pos = 18) private String HolidayCode;
  @DataField(pos = 19) private String OptionStyle = "BARRIER";
  @DataField(pos = 20) private String ExerciseType;
  @DataField(pos = 21) private String OptionType;
  @DataField(pos = 22) private String PrimaryCurrency;
  @DataField(pos = 23) private String SecondaryCurrency;
  @DataField(pos = 24) private String PrimaryAmount;
  @DataField(pos = 25) private String QuotingAmount;
  @DataField(pos = 26) private String Strike;
  @DataField(pos = 27) private String FirstExerciseDate;
  @DataField(pos = 28) private String DeliveryHolidays;
  @DataField(pos = 29) private String FXReset;
  @DataField(pos = 30) private String AutoExercise;
  @DataField(pos = 31) private String ExpiryTime;
  @DataField(pos = 32) private String ExpiryTimeZone;
  @DataField(pos = 33) private String SettlementType;
  @DataField(pos = 34) private String SettlementCurrency;
  @DataField(pos = 35) private String UpStartDate;
  @DataField(pos = 36) private String UpEndDate;
  @DataField(pos = 37) private String DownStartDate;
  @DataField(pos = 38) private String DownEndDate;
  @DataField(pos = 39) private String UpBarrierLevel;
  @DataField(pos = 40) private String UpBarrierType;
  @DataField(pos = 41) private String DownBarrierLevel;
  @DataField(pos = 42) private String DownBarrierType;
  @DataField(pos = 43) private String Rebate;
  @DataField(pos = 44) private String RebateCurrency;
  @DataField(pos = 45) private String RebateCondition;
  @DataField(pos = 46) private String RebateType;
  @DataField(pos = 47) private String PayOut;
  @DataField(pos = 48) private String PayOutCurrency;
  @DataField(pos = 49) private String TimingType;
  @DataField(pos = 50) private String TermType;
  @DataField(pos = 51) private String DurationType;
  @DataField(pos = 52, name = "keyword.StrategyType") private String strategyTypeKeyword;
  @DataField(pos = 53, name="keyword.PSStrategyName") private String psStrategyNameKeyword;
  @DataField(pos = 54, columnName = "Fee.Type") private String feeType;
  @DataField(pos = 55, columnName = "Fee.Amount") private String feeAmount;
  @DataField(pos = 56, columnName = "Fee.Direction") private String feeDirection;
  @DataField(pos = 57, columnName = "Fee.Currency") private String feeCurrency;
  @DataField(pos = 58, columnName = "Fee.LegalEntity") private String feeLegalEntity;
  @DataField(pos = 59, columnName = "Fee.StartDate") private String feeStartDate;
  @DataField(pos = 60, columnName = "Fee.EndDate") private String feeEndDate;
  @DataField(pos = 61, columnName = "Fee.KnownDate") private String feeKnownDate;
  @DataField(pos = 62, columnName = "Fee.Date") private String feeDate;
  @DataField(pos = 63) private String TerminationReason;
  @DataField(pos = 64) private String TerminationDate;
  @DataField(pos = 65) private String TerminationEffectiveDate;
  @DataField(pos = 66) private String TerminationPercent;
  @DataField(pos = 67) private String TerminationAmount;
  @DataField(pos = 68) private String TerminationFFCPOption;
  @DataField(pos = 69) private String NovationType;
  @DataField(pos = 70) private String NovationDate;
  @DataField(pos = 71) private String NovationEffectiveDate;
  @DataField(pos = 72) private String NovationPercent;
  @DataField(pos = 73) private String NovationAmount;
  @DataField(pos = 74) private String NovationCounterparty;
  @DataField(pos = 75) private String NovationCounterpartyRole;
  @DataField(pos = 76) private String NovationFFCPOption;

  public String getAction() {
    return Action;
  }

  public void setAction(String action) {
    Action = action;
  }

  public String getExternalReference() {
    return ExternalReference;
  }

  public void setExternalReference(String externalReference) {
    ExternalReference = externalReference;
  }

  public String getInternalReference() {
    return InternalReference;
  }

  public void setInternalReference(String internalReference) {
    InternalReference = internalReference;
  }

  public String getBook() {
    return Book;
  }

  public void setBook(String book) {
    Book = book;
  }

  public String getCounterparty() {
    return Counterparty;
  }

  public void setCounterparty(String counterparty) {
    Counterparty = counterparty;
  }

  public String getCounterPartyRole() {
    return CounterPartyRole;
  }

  public void setCounterPartyRole(String counterPartyRole) {
    CounterPartyRole = counterPartyRole;
  }

  public String getBundleType() {
    return BundleType;
  }

  public void setBundleType(String bundleType) {
    BundleType = bundleType;
  }

  public String getBundleName() {
    return BundleName;
  }

  public void setBundleName(String bundleName) {
    BundleName = bundleName;
  }

  public String getBuySell() {
    return BuySell;
  }

  public void setBuySell(String buySell) {
    BuySell = buySell;
  }

  public String getNotional() {
    return Notional;
  }

  public void setNotional(String notional) {
    Notional = notional;
  }

  public String getTradeDatetime() {
    return TradeDatetime;
  }

  public void setTradeDatetime(String tradeDatetime) {
    TradeDatetime = tradeDatetime;
  }

  public String getTradeSettleDate() {
    return TradeSettleDate;
  }

  public void setTradeSettleDate(String tradeSettleDate) {
    TradeSettleDate = tradeSettleDate;
  }

  public String getStartDate() {
    return StartDate;
  }

  public void setStartDate(String startDate) {
    StartDate = startDate;
  }

  public String getMaturityDate() {
    return MaturityDate;
  }

  public void setMaturityDate(String maturityDate) {
    MaturityDate = maturityDate;
  }

  public String getTrader() {
    return Trader;
  }

  public void setTrader(String trader) {
    Trader = trader;
  }

  public String getSalesPerson() {
    return SalesPerson;
  }

  public void setSalesPerson(String salesPerson) {
    SalesPerson = salesPerson;
  }

  public String getTemplateName() {
    return TemplateName;
  }

  public void setTemplateName(String templateName) {
    TemplateName = templateName;
  }

  public String getComments() {
    return Comments;
  }

  public void setComments(String comments) {
    Comments = comments;
  }

  public String getHolidayCode() {
    return HolidayCode;
  }

  public void setHolidayCode(String holidayCode) {
    HolidayCode = holidayCode;
  }

  public String getOptionStyle() {
    return OptionStyle;
  }

  public void setOptionStyle(String optionStyle) {
    OptionStyle = optionStyle;
  }

  public String getExerciseType() {
    return ExerciseType;
  }

  public void setExerciseType(String exerciseType) {
    ExerciseType = exerciseType;
  }

  public String getOptionType() {
    return OptionType;
  }

  public void setOptionType(String optionType) {
    OptionType = optionType;
  }

  public String getPrimaryCurrency() {
    return PrimaryCurrency;
  }

  public void setPrimaryCurrency(String primaryCurrency) {
    PrimaryCurrency = primaryCurrency;
  }

  public String getSecondaryCurrency() {
    return SecondaryCurrency;
  }

  public void setSecondaryCurrency(String secondaryCurrency) {
    SecondaryCurrency = secondaryCurrency;
  }

  public String getPrimaryAmount() {
    return PrimaryAmount;
  }

  public void setPrimaryAmount(String primaryAmount) {
    PrimaryAmount = primaryAmount;
  }

  public String getQuotingAmount() {
    return QuotingAmount;
  }

  public void setQuotingAmount(String quotingAmount) {
    QuotingAmount = quotingAmount;
  }

  public String getStrike() {
    return Strike;
  }

  public void setStrike(String strike) {
    Strike = strike;
  }

  public String getFirstExerciseDate() {
    return FirstExerciseDate;
  }

  public void setFirstExerciseDate(String firstExerciseDate) {
    FirstExerciseDate = firstExerciseDate;
  }

  public String getDeliveryHolidays() {
    return DeliveryHolidays;
  }

  public void setDeliveryHolidays(String deliveryHolidays) {
    DeliveryHolidays = deliveryHolidays;
  }

  public String getFXReset() {
    return FXReset;
  }

  public void setFXReset(String FXReset) {
    this.FXReset = FXReset;
  }

  public String getAutoExercise() {
    return AutoExercise;
  }

  public void setAutoExercise(String autoExercise) {
    AutoExercise = autoExercise;
  }

  public String getExpiryTime() {
    return ExpiryTime;
  }

  public void setExpiryTime(String expiryTime) {
    ExpiryTime = expiryTime;
  }

  public String getExpiryTimeZone() {
    return ExpiryTimeZone;
  }

  public void setExpiryTimeZone(String expiryTimeZone) {
    ExpiryTimeZone = expiryTimeZone;
  }

  public String getSettlementType() {
    return SettlementType;
  }

  public void setSettlementType(String settlementType) {
    SettlementType = settlementType;
  }

  public String getSettlementCurrency() {
    return SettlementCurrency;
  }

  public void setSettlementCurrency(String settlementCurrency) {
    SettlementCurrency = settlementCurrency;
  }

  public String getUpStartDate() {
    return UpStartDate;
  }

  public void setUpStartDate(String upStartDate) {
    UpStartDate = upStartDate;
  }

  public String getUpEndDate() {
    return UpEndDate;
  }

  public void setUpEndDate(String upEndDate) {
    UpEndDate = upEndDate;
  }

  public String getDownStartDate() {
    return DownStartDate;
  }

  public void setDownStartDate(String downStartDate) {
    DownStartDate = downStartDate;
  }

  public String getDownEndDate() {
    return DownEndDate;
  }

  public void setDownEndDate(String downEndDate) {
    DownEndDate = downEndDate;
  }

  public String getUpBarrierLevel() {
    return UpBarrierLevel;
  }

  public void setUpBarrierLevel(String upBarrierLevel) {
    UpBarrierLevel = upBarrierLevel;
  }

  public String getUpBarrierType() {
    return UpBarrierType;
  }

  public void setUpBarrierType(String upBarrierType) {
    UpBarrierType = upBarrierType;
  }

  public String getDownBarrierLevel() {
    return DownBarrierLevel;
  }

  public void setDownBarrierLevel(String downBarrierLevel) {
    DownBarrierLevel = downBarrierLevel;
  }

  public String getDownBarrierType() {
    return DownBarrierType;
  }

  public void setDownBarrierType(String downBarrierType) {
    DownBarrierType = downBarrierType;
  }

  public String getRebate() {
    return Rebate;
  }

  public void setRebate(String rebate) {
    Rebate = rebate;
  }

  public String getRebateCurrency() {
    return RebateCurrency;
  }

  public void setRebateCurrency(String rebateCurrency) {
    RebateCurrency = rebateCurrency;
  }

  public String getRebateCondition() {
    return RebateCondition;
  }

  public void setRebateCondition(String rebateCondition) {
    RebateCondition = rebateCondition;
  }

  public String getRebateType() {
    return RebateType;
  }

  public void setRebateType(String rebateType) {
    RebateType = rebateType;
  }

  public String getPayOut() {
    return PayOut;
  }

  public void setPayOut(String payOut) {
    PayOut = payOut;
  }

  public String getPayOutCurrency() {
    return PayOutCurrency;
  }

  public void setPayOutCurrency(String payOutCurrency) {
    PayOutCurrency = payOutCurrency;
  }

  public String getTimingType() {
    return TimingType;
  }

  public void setTimingType(String timingType) {
    TimingType = timingType;
  }

  public String getTermType() {
    return TermType;
  }

  public void setTermType(String termType) {
    TermType = termType;
  }

  public String getDurationType() {
    return DurationType;
  }

  public void setDurationType(String durationType) {
    DurationType = durationType;
  }

  public String getStrategyTypeKeyword() {
    return strategyTypeKeyword;
  }

  public void setStrategyTypeKeyword(String strategyTypeKeyword) {
    this.strategyTypeKeyword = strategyTypeKeyword;
  }

  public String getPsStrategyNameKeyword() {
    return psStrategyNameKeyword;
  }

  public void setPsStrategyNameKeyword(String psStrategyNameKeyword) {
    this.psStrategyNameKeyword = psStrategyNameKeyword;
  }

  public String getFeeType() {
    return feeType;
  }

  public void setFeeType(String feeType) {
    this.feeType = feeType;
  }

  public String getFeeAmount() {
    return feeAmount;
  }

  public void setFeeAmount(String feeAmount) {
    this.feeAmount = feeAmount;
  }

  public String getFeeDirection() {
    return feeDirection;
  }

  public void setFeeDirection(String feeDirection) {
    this.feeDirection = feeDirection;
  }

  public String getFeeCurrency() {
    return feeCurrency;
  }

  public void setFeeCurrency(String feeCurrency) {
    this.feeCurrency = feeCurrency;
  }

  public String getFeeLegalEntity() {
    return feeLegalEntity;
  }

  public void setFeeLegalEntity(String feeLegalEntity) {
    this.feeLegalEntity = feeLegalEntity;
  }

  public String getFeeStartDate() {
    return feeStartDate;
  }

  public void setFeeStartDate(String feeStartDate) {
    this.feeStartDate = feeStartDate;
  }

  public String getFeeEndDate() {
    return feeEndDate;
  }

  public void setFeeEndDate(String feeEndDate) {
    this.feeEndDate = feeEndDate;
  }

  public String getFeeKnownDate() {
    return feeKnownDate;
  }

  public void setFeeKnownDate(String feeKnownDate) {
    this.feeKnownDate = feeKnownDate;
  }

  public String getFeeDate() {
    return feeDate;
  }

  public void setFeeDate(String feeDate) {
    this.feeDate = feeDate;
  }

  public String getTerminationReason() {
    return TerminationReason;
  }

  public void setTerminationReason(String terminationReason) {
    TerminationReason = terminationReason;
  }

  public String getTerminationDate() {
    return TerminationDate;
  }

  public void setTerminationDate(String terminationDate) {
    TerminationDate = terminationDate;
  }

  public String getTerminationEffectiveDate() {
    return TerminationEffectiveDate;
  }

  public void setTerminationEffectiveDate(String terminationEffectiveDate) {
    TerminationEffectiveDate = terminationEffectiveDate;
  }

  public String getTerminationPercent() {
    return TerminationPercent;
  }

  public void setTerminationPercent(String terminationPercent) {
    TerminationPercent = terminationPercent;
  }

  public String getTerminationAmount() {
    return TerminationAmount;
  }

  public void setTerminationAmount(String terminationAmount) {
    TerminationAmount = terminationAmount;
  }

  public String getTerminationFFCPOption() {
    return TerminationFFCPOption;
  }

  public void setTerminationFFCPOption(String terminationFFCPOption) {
    TerminationFFCPOption = terminationFFCPOption;
  }

  public String getNovationType() {
    return NovationType;
  }

  public void setNovationType(String novationType) {
    NovationType = novationType;
  }

  public String getNovationDate() {
    return NovationDate;
  }

  public void setNovationDate(String novationDate) {
    NovationDate = novationDate;
  }

  public String getNovationEffectiveDate() {
    return NovationEffectiveDate;
  }

  public void setNovationEffectiveDate(String novationEffectiveDate) {
    NovationEffectiveDate = novationEffectiveDate;
  }

  public String getNovationPercent() {
    return NovationPercent;
  }

  public void setNovationPercent(String novationPercent) {
    NovationPercent = novationPercent;
  }

  public String getNovationAmount() {
    return NovationAmount;
  }

  public void setNovationAmount(String novationAmount) {
    NovationAmount = novationAmount;
  }

  public String getNovationCounterparty() {
    return NovationCounterparty;
  }

  public void setNovationCounterparty(String novationCounterparty) {
    NovationCounterparty = novationCounterparty;
  }

  public String getNovationCounterpartyRole() {
    return NovationCounterpartyRole;
  }

  public void setNovationCounterpartyRole(String novationCounterpartyRole) {
    NovationCounterpartyRole = novationCounterpartyRole;
  }

  public String getNovationFFCPOption() {
    return NovationFFCPOption;
  }

  public void setNovationFFCPOption(String novationFFCPOption) {
    NovationFFCPOption = novationFFCPOption;
  }

  @Override
  public String getCsvFileName() {
    return "FXOPTION_BARRIER_CSV.csv";
  }

}
