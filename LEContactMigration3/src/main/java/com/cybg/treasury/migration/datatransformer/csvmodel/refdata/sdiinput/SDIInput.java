package com.cybg.treasury.migration.datatransformer.csvmodel.refdata.sdiinput;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVInput;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.springframework.stereotype.Component;

/**
 * @author theo on 05/01/2017.
 */
@CsvRecord(separator = ",", skipFirstLine = true)
@Component
public class SDIInput implements CSVInput {

  @DataField(pos = 1)  private String client_ID;
  @DataField(pos = 2)  private String short_name;
  @DataField(pos = 3)  private String long_name;
  @DataField(pos = 4)  private String status_code;
  @DataField(pos = 5)  private String business_type;
  @DataField(pos = 6)  private String account_ID;
  @DataField(pos = 7)  private String account_number;
  @DataField(pos = 8)  private String account_type;
  @DataField(pos = 9)  private String account2_number;
  @DataField(pos = 10) private String agent_short_name;
  @DataField(pos = 11) private String bnk1_id;
  @DataField(pos = 12) private String bnk1_name;
  @DataField(pos = 13) private String bnk2_id;
  @DataField(pos = 14) private String bnk2_name;
  @DataField(pos = 15) private String currency;
  @DataField(pos = 16) private String details_of_charges;
  @DataField(pos = 17) private String effective_date;
  @DataField(pos = 18) private String account_expiry_date;
  @DataField(pos = 19) private String has_special_instructions;
  @DataField(pos = 20) private String intermediary_short_name;
  @DataField(pos = 21) private String is_cover_message_required;
  @DataField(pos = 22) private String is_nostro_agent_to_receive;
  @DataField(pos = 23) private String is_stp;
  @DataField(pos = 24) private String account_remark;
  @DataField(pos = 25) private String remittance_information;
  @DataField(pos = 26) private String sender_to_recieve_information;
  @DataField(pos = 27) private String tp_beneficiary;
  @DataField(pos = 28) private String tp_reimbursement_institution;
  @DataField(pos = 29) private String settlement_instruction_ID;
  @DataField(pos = 30) private String si_payment_direction;
  @DataField(pos = 31) private String settlement_method;
  @DataField(pos = 32) private String si_remark;
  @DataField(pos = 33) private String si_expiry_date;
  @DataField(pos = 34) private String si_default_ID;
  @DataField(pos = 35) private String si_def_payment_direction;
  @DataField(pos = 36) private String netting_effective_date;
  @DataField(pos = 37) private String netting_expiry_date;
  @DataField(pos = 38) private String code;
  @DataField(pos = 39) private String UFTW;

  public String getClientID() {
    return client_ID;
  }

  public String getShortName() {
    return short_name;
  }

  public String getLongName() {
    return long_name;
  }

  public String getStatusCode() {
    return status_code;
  }

  public String getBusinessType() {
    return business_type;
  }

  public String getAccountID() {
    return account_ID;
  }

  public String getAccountNumber() {
    return account_number;
  }

  public String getAccountType() {
    return account_type;
  }

  public String getAccount2Number() {
    return account2_number;
  }

  public String getAgentShortName() {
    return agent_short_name;
  }

  public String getBnk1Id() {
    return bnk1_id;
  }

  public String getBnk1Name() {
    return bnk1_name;
  }

  public String getBnk2Id() {
    return bnk2_id;
  }

  public String getBnk2Name() {
    return bnk2_name;
  }

  public String getCurrency() {
    return currency;
  }

  public String getDetailsOfCharges() {
    return details_of_charges;
  }

  public String getEffectiveDate() {
    return effective_date;
  }

  public String getAccountExpiryDate() {
    return account_expiry_date;
  }

  public String getHasSpecialInstructions() {
    return has_special_instructions;
  }

  public String getIntermediaryShortName() {
    return intermediary_short_name;
  }

  public String getIsCoverMessageRequired() {
    return is_cover_message_required;
  }

  public String getIsNostroAgentToReceive() {
    return is_nostro_agent_to_receive;
  }

  public String getIsStp() {
    return is_stp;
  }

  public String getAccountRemark() {
    return account_remark;
  }

  public String getRemittanceInformation() {
    return remittance_information;
  }

  public String getSenderToRecieveInformation() {
    return sender_to_recieve_information;
  }

  public String getTpBeneficiary() {
    return tp_beneficiary;
  }

  public String getTpReimbursementInstitution() {
    return tp_reimbursement_institution;
  }

  public String getSettlementInstructionID() {
    return settlement_instruction_ID;
  }

  public String getSi_payment_direction() {
    return si_payment_direction;
  }

  public String getSettlement_method() {
    return settlement_method;
  }

  public String getSiRemark() {
    return si_remark;
  }

  public String getSiExpiryDate() {
    return si_expiry_date;
  }

  public String getSiDefaultID() {
    return si_default_ID;
  }

  public String getSiDefPaymentDirection() {
    return si_def_payment_direction;
  }

  public String getNettingEffectiveDate() {
    return netting_effective_date;
  }

  public String getNettingExpiryDate() {
    return netting_expiry_date;
  }

  public String getCode() {
    return code;
  }

  public String getUFTW() {
    return UFTW;
  }

  @Override
  public String getFileName() {
    return "SDI.csv";
  }

  @Override
  public String getObjectType() {
    return "SDI";
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    SDIInput sdiInput = (SDIInput)o;

    if(client_ID != null ? !client_ID.equals(sdiInput.client_ID) : sdiInput.client_ID != null) return false;
    if(short_name != null ? !short_name.equals(sdiInput.short_name) : sdiInput.short_name != null) return false;
    if(long_name != null ? !long_name.equals(sdiInput.long_name) : sdiInput.long_name != null) return false;
    if(status_code != null ? !status_code.equals(sdiInput.status_code) : sdiInput.status_code != null) return false;
    if(business_type != null ? !business_type.equals(sdiInput.business_type) : sdiInput.business_type != null)
      return false;
    if(account_ID != null ? !account_ID.equals(sdiInput.account_ID) : sdiInput.account_ID != null) return false;
    if(account_number != null ? !account_number.equals(sdiInput.account_number) : sdiInput.account_number != null)
      return false;
    if(account_type != null ? !account_type.equals(sdiInput.account_type) : sdiInput.account_type != null) return false;
    if(account2_number != null ? !account2_number.equals(sdiInput.account2_number) : sdiInput.account2_number != null)
      return false;
    if(agent_short_name != null ? !agent_short_name.equals(sdiInput.agent_short_name) : sdiInput.agent_short_name != null)
      return false;
    if(bnk1_id != null ? !bnk1_id.equals(sdiInput.bnk1_id) : sdiInput.bnk1_id != null) return false;
    if(bnk1_name != null ? !bnk1_name.equals(sdiInput.bnk1_name) : sdiInput.bnk1_name != null) return false;
    if(bnk2_id != null ? !bnk2_id.equals(sdiInput.bnk2_id) : sdiInput.bnk2_id != null) return false;
    if(bnk2_name != null ? !bnk2_name.equals(sdiInput.bnk2_name) : sdiInput.bnk2_name != null) return false;
    if(currency != null ? !currency.equals(sdiInput.currency) : sdiInput.currency != null) return false;
    if(details_of_charges != null ? !details_of_charges.equals(sdiInput.details_of_charges) : sdiInput.details_of_charges != null)
      return false;
    if(effective_date != null ? !effective_date.equals(sdiInput.effective_date) : sdiInput.effective_date != null)
      return false;
    if(account_expiry_date != null ? !account_expiry_date.equals(sdiInput.account_expiry_date) : sdiInput.account_expiry_date != null)
      return false;
    if(has_special_instructions != null ? !has_special_instructions.equals(sdiInput.has_special_instructions) : sdiInput.has_special_instructions != null)
      return false;
    if(intermediary_short_name != null ? !intermediary_short_name.equals(sdiInput.intermediary_short_name) : sdiInput.intermediary_short_name != null)
      return false;
    if(is_cover_message_required != null ? !is_cover_message_required.equals(sdiInput.is_cover_message_required) : sdiInput.is_cover_message_required != null)
      return false;
    if(is_nostro_agent_to_receive != null ? !is_nostro_agent_to_receive.equals(sdiInput.is_nostro_agent_to_receive) : sdiInput.is_nostro_agent_to_receive != null)
      return false;
    if(is_stp != null ? !is_stp.equals(sdiInput.is_stp) : sdiInput.is_stp != null) return false;
    if(account_remark != null ? !account_remark.equals(sdiInput.account_remark) : sdiInput.account_remark != null)
      return false;
    if(remittance_information != null ? !remittance_information.equals(sdiInput.remittance_information) : sdiInput.remittance_information != null)
      return false;
    if(sender_to_recieve_information != null ? !sender_to_recieve_information.equals(sdiInput.sender_to_recieve_information) : sdiInput.sender_to_recieve_information != null)
      return false;
    if(tp_beneficiary != null ? !tp_beneficiary.equals(sdiInput.tp_beneficiary) : sdiInput.tp_beneficiary != null)
      return false;
    if(tp_reimbursement_institution != null ? !tp_reimbursement_institution.equals(sdiInput.tp_reimbursement_institution) : sdiInput.tp_reimbursement_institution != null)
      return false;
    if(settlement_instruction_ID != null ? !settlement_instruction_ID.equals(sdiInput.settlement_instruction_ID) : sdiInput.settlement_instruction_ID != null)
      return false;
    if(si_payment_direction != null ? !si_payment_direction.equals(sdiInput.si_payment_direction) : sdiInput.si_payment_direction != null)
      return false;
    if(settlement_method != null ? !settlement_method.equals(sdiInput.settlement_method) : sdiInput.settlement_method != null)
      return false;
    if(si_remark != null ? !si_remark.equals(sdiInput.si_remark) : sdiInput.si_remark != null) return false;
    if(si_expiry_date != null ? !si_expiry_date.equals(sdiInput.si_expiry_date) : sdiInput.si_expiry_date != null)
      return false;
    if(si_default_ID != null ? !si_default_ID.equals(sdiInput.si_default_ID) : sdiInput.si_default_ID != null)
      return false;
    if(si_def_payment_direction != null ? !si_def_payment_direction.equals(sdiInput.si_def_payment_direction) : sdiInput.si_def_payment_direction != null)
      return false;
    if(netting_effective_date != null ? !netting_effective_date.equals(sdiInput.netting_effective_date) : sdiInput.netting_effective_date != null)
      return false;
    if(netting_expiry_date != null ? !netting_expiry_date.equals(sdiInput.netting_expiry_date) : sdiInput.netting_expiry_date != null)
      return false;
    if(code != null ? !code.equals(sdiInput.code) : sdiInput.code != null) return false;
    return UFTW != null ? UFTW.equals(sdiInput.UFTW) : sdiInput.UFTW == null;

  }

  @Override
  public int hashCode() {
    int result = client_ID != null ? client_ID.hashCode() : 0;
    result = 31 * result + (short_name != null ? short_name.hashCode() : 0);
    result = 31 * result + (long_name != null ? long_name.hashCode() : 0);
    result = 31 * result + (status_code != null ? status_code.hashCode() : 0);
    result = 31 * result + (business_type != null ? business_type.hashCode() : 0);
    result = 31 * result + (account_ID != null ? account_ID.hashCode() : 0);
    result = 31 * result + (account_number != null ? account_number.hashCode() : 0);
    result = 31 * result + (account_type != null ? account_type.hashCode() : 0);
    result = 31 * result + (account2_number != null ? account2_number.hashCode() : 0);
    result = 31 * result + (agent_short_name != null ? agent_short_name.hashCode() : 0);
    result = 31 * result + (bnk1_id != null ? bnk1_id.hashCode() : 0);
    result = 31 * result + (bnk1_name != null ? bnk1_name.hashCode() : 0);
    result = 31 * result + (bnk2_id != null ? bnk2_id.hashCode() : 0);
    result = 31 * result + (bnk2_name != null ? bnk2_name.hashCode() : 0);
    result = 31 * result + (currency != null ? currency.hashCode() : 0);
    result = 31 * result + (details_of_charges != null ? details_of_charges.hashCode() : 0);
    result = 31 * result + (effective_date != null ? effective_date.hashCode() : 0);
    result = 31 * result + (account_expiry_date != null ? account_expiry_date.hashCode() : 0);
    result = 31 * result + (has_special_instructions != null ? has_special_instructions.hashCode() : 0);
    result = 31 * result + (intermediary_short_name != null ? intermediary_short_name.hashCode() : 0);
    result = 31 * result + (is_cover_message_required != null ? is_cover_message_required.hashCode() : 0);
    result = 31 * result + (is_nostro_agent_to_receive != null ? is_nostro_agent_to_receive.hashCode() : 0);
    result = 31 * result + (is_stp != null ? is_stp.hashCode() : 0);
    result = 31 * result + (account_remark != null ? account_remark.hashCode() : 0);
    result = 31 * result + (remittance_information != null ? remittance_information.hashCode() : 0);
    result = 31 * result + (sender_to_recieve_information != null ? sender_to_recieve_information.hashCode() : 0);
    result = 31 * result + (tp_beneficiary != null ? tp_beneficiary.hashCode() : 0);
    result = 31 * result + (tp_reimbursement_institution != null ? tp_reimbursement_institution.hashCode() : 0);
    result = 31 * result + (settlement_instruction_ID != null ? settlement_instruction_ID.hashCode() : 0);
    result = 31 * result + (si_payment_direction != null ? si_payment_direction.hashCode() : 0);
    result = 31 * result + (settlement_method != null ? settlement_method.hashCode() : 0);
    result = 31 * result + (si_remark != null ? si_remark.hashCode() : 0);
    result = 31 * result + (si_expiry_date != null ? si_expiry_date.hashCode() : 0);
    result = 31 * result + (si_default_ID != null ? si_default_ID.hashCode() : 0);
    result = 31 * result + (si_def_payment_direction != null ? si_def_payment_direction.hashCode() : 0);
    result = 31 * result + (netting_effective_date != null ? netting_effective_date.hashCode() : 0);
    result = 31 * result + (netting_expiry_date != null ? netting_expiry_date.hashCode() : 0);
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (UFTW != null ? UFTW.hashCode() : 0);
    return result;
  }
}
