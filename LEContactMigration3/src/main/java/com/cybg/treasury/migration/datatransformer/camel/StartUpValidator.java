package com.cybg.treasury.migration.datatransformer.camel;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
// TODO replace this class by a generic file validator to be used by the routes
public class StartUpValidator {
  private static final Logger log = LoggerFactory.getLogger(StartUpValidator.class);
  private final String inputDirectory;
  private final String inputFileName;

  @Autowired
  public StartUpValidator(@Value("#{environment['migrationdata.input.dir']}") String inputDirectory,
                            @Value("#{environment['migrationdata.input.filename']}") String inputFileName){
      this.inputDirectory = inputDirectory;
      this.inputFileName = inputFileName;
    }

  public boolean validate() {
    boolean result = true;
    Path path = Paths.get(URI.create(inputDirectory)).resolve(inputFileName);

    if (!path.toFile().exists()) {
      result = false;
      log.error(String.format("Filename %s does not exist", path.toString()));
    }
    return result;
  }
}
