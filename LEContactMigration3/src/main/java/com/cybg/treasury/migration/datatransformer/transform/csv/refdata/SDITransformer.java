package com.cybg.treasury.migration.datatransformer.transform.csv.refdata;

import com.cybg.treasury.migration.datatransformer.csvmodel.refdata.sdiinput.SDIInput;
import com.cybg.treasury.migration.datatransformer.csvmodel.refdata.sdioutput.SDIOutput;
import com.cybg.treasury.migration.datatransformer.transform.csv.CsvToCsvDataTransformer;
import org.springframework.stereotype.Component;

/**
 * @author theo on 05/01/2017.
 */
@Component
public class SDITransformer implements CsvToCsvDataTransformer<SDIInput, SDIOutput> {

  @Override
  public SDIOutput transform(SDIInput sdiInput) {
    SDIOutput sdiOutput = new SDIOutput();
//    sdiOutput.setReference();
//    sdiOutput.setRole();
//    sdiOutput.setContact();
//    sdiOutput.setSDISettlementType();
//    sdiOutput.setProducts();
//    sdiOutput.setPayRec();
//    sdiOutput.setMethod();
//    sdiOutput.setProcessingOrg();
//    sdiOutput.setPreferred();
//    sdiOutput.setPriority();
//    sdiOutput.setCurrencies();
//    sdiOutput.setEffectiveFrom();
//    sdiOutput.setEffectiveTo();
//    sdiOutput.setByTradeDate();
//    sdiOutput.setTradeCounterParty();
//    sdiOutput.setAgentCode();
//    sdiOutput.setAgentAccount();
//    sdiOutput.setAgentGLAc();
//    sdiOutput.setAgentSubAccount();
//    sdiOutput.setAgentIdentifier();
//    sdiOutput.setMessageToAgent();
//    sdiOutput.setFirstIntermediaryPresent();
//    sdiOutput.setIntermediary1Code();
//    sdiOutput.setIntermediary1Account();
//    sdiOutput.setIntermediary1GLAc();
//    sdiOutput.setIntermediary1Name();
//    sdiOutput.setIntermediary1SubAccount();
//    sdiOutput.setIntermediary1Identifier();
//    sdiOutput.setMessageToIntermediary1();
//    sdiOutput.setSecondIntermediaryPresent();
//    sdiOutput.setIntermediary2Code();
//    sdiOutput.setIntermediary2Account();
//    sdiOutput.setIntermediary2SubAccount();
//    sdiOutput.setIntermediary2Identifier();
//    sdiOutput.setMessageToIntermediary2();
//    sdiOutput.setDirect();
//    sdiOutput.setDDA();
//    sdiOutput.setLinkSDI();
//    sdiOutput.setLinkedSDI();
    return sdiOutput;
  }

  @Override
  public String getSupportedType() {
    return "SDI";
  }
}
