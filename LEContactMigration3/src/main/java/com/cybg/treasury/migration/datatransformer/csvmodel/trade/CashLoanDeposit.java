package com.cybg.treasury.migration.datatransformer.csvmodel.trade;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/**
 * @author theo on 14/12/2016.
 */
@CsvRecord(separator = ",", generateHeaderColumns = true)
public class CashLoanDeposit implements CSVTrade {

  @DataField(pos = 0) private String Action;
  @DataField(pos = 1) private String ExternalRefId;
  @DataField(pos = 2) private String InternalRefId;
  @DataField(pos = 3) private String Counterparty;
  @DataField(pos = 4) private String CounterPartyRole;
  @DataField(pos = 5) private String Book;
  @DataField(pos = 6) private String MirrorBook;
  @DataField(pos = 7) private String BundleType = "PricingSheet";
  @DataField(pos = 8) private String BundleName;
  @DataField(pos = 9) private String Currency;
  @DataField(pos = 10) private String Nominal;
  @DataField(pos = 11) private String BuySell;
  @DataField(pos = 12) private String TradeDate;
  @DataField(pos = 13) private String SettlementDate;
  @DataField(pos = 14) private String StartDate;
  @DataField(pos = 15) private String MaturityDate;
  @DataField(pos = 16) private String TraderName;
  @DataField(pos = 17) private String SalesPerson;
  @DataField(pos = 18) private String Comment;
  @DataField(pos = 19) private String TemplateName;
  @DataField(pos = 20) private String RateType;
  @DataField(pos = 21) private String LoanDeposit;
  @DataField(pos = 22, columnName = "Currency") private String anotherCurrency;
  @DataField(pos = 23) private String Rate;
  @DataField(pos = 24) private String SalesMargin;
  @DataField(pos = 25) private String Discount;
  @DataField(pos = 26) private String DiscountMethod;
  @DataField(pos = 27) private String Capitalized;
  @DataField(pos = 28) private String RoundingMethod;
  @DataField(pos = 29) private String Index;
  @DataField(pos = 30) private String IndexSource;
  @DataField(pos = 31) private String IndexFactor;
  @DataField(pos = 32) private String Spread;
  @DataField(pos = 33) private String Tenor;
  @DataField(pos = 34) private String StubPeriod;
  @DataField(pos = 35) private String SpecificFirstDate;
  @DataField(pos = 36) private String SpecificLastDate;
  @DataField(pos = 37) private String ResetPeriod;
  @DataField(pos = 38) private String FirstReset;
  @DataField(pos = 39) private String FirstRate;
  @DataField(pos = 40) private String HolidayCode;
  @DataField(pos = 41) private String Compound;
  @DataField(pos = 42) private String CompoundMethod;
  @DataField(pos = 43) private String CompoundFrequency;
  @DataField(pos = 44) private String ResetFrequency;
  @DataField(pos = 45) private String ResetMethod;
  @DataField(pos = 46) private String SamplePeriodRule;
  @DataField(pos = 47) private String CustomSamplePeriod;
  @DataField(pos = 48) private String ResetLag;
  @DataField(pos = 49) private String ResetOffsetBusDayB;
  @DataField(pos = 50) private String ResetDateRoll;
  @DataField(pos = 51) private String ResetHolidays;
  @DataField(pos = 52) private String Floater;
  @DataField(pos = 53) private String FloaterType;
  @DataField(pos = 54) private String Cap;
  @DataField(pos = 55) private String Floor;
  @DataField(pos = 56) private String Frequency;
  @DataField(pos = 57) private String PaymentLag;
  @DataField(pos = 58) private String CouponOffsetBusDayB;
  @DataField(pos = 59) private String RollDay;
  @DataField(pos = 60) private String DateRoll;
  @DataField(pos = 61) private String DayCount;
  @DataField(pos = 62) private String PrincipalExchange;
  @DataField(pos = 63) private String AccrualAdj;
  @DataField(pos = 64) private String AmortType;
  @DataField(pos = 65) private String BaseAmount;
  @DataField(pos = 66) private String AmortStartDate;
  @DataField(pos = 67) private String AmortEndDate;
  @DataField(pos = 68) private String Increment;
  @DataField(pos = 69) private String AmortFrequency;
  @DataField(pos = 70) private String Operator;
  @DataField(pos = 71) private String TermAsDate;
  @DataField(pos = 72) private String TermDate;
  @DataField(pos = 73) private String UseLoanTerm;
  @DataField(pos = 74) private String LoanTerm;
  @DataField(pos = 75) private String OpenTerm;
  @DataField(pos = 76 ,columnName = "Fee.Type") private String feeType;
  @DataField(pos = 77 ,columnName = "Fee.Amount") private String feeStartDate;
  @DataField(pos = 78 ,columnName = "Fee.Direction") private String feeEndDate;
  @DataField(pos = 79 ,columnName = "Fee.Currency") private String feeKnownDate;
  @DataField(pos = 80 ,columnName = "Fee.LegalEntity") private String feeLegalEntity;
  @DataField(pos = 81 ,columnName = "Fee.Date") private String feeDate;
  @DataField(pos = 82, columnName = "keyword.Broker") private String brokerKeyword;

  public String getAction() {
    return Action;
  }

  public void setAction(String action) {
    Action = action;
  }

  public String getExternalRefId() {
    return ExternalRefId;
  }

  public void setExternalRefId(String externalRefId) {
    ExternalRefId = externalRefId;
  }

  public String getInternalRefId() {
    return InternalRefId;
  }

  public void setInternalRefId(String internalRefId) {
    InternalRefId = internalRefId;
  }

  public String getCounterparty() {
    return Counterparty;
  }

  public void setCounterparty(String counterparty) {
    Counterparty = counterparty;
  }

  public String getCounterPartyRole() {
    return CounterPartyRole;
  }

  public void setCounterPartyRole(String counterPartyRole) {
    CounterPartyRole = counterPartyRole;
  }

  public String getBook() {
    return Book;
  }

  public void setBook(String book) {
    Book = book;
  }

  public String getMirrorBook() {
    return MirrorBook;
  }

  public void setMirrorBook(String mirrorBook) {
    MirrorBook = mirrorBook;
  }

  public String getBundleType() {
    return BundleType;
  }

  public void setBundleType(String bundleType) {
    BundleType = bundleType;
  }

  public String getBundleName() {
    return BundleName;
  }

  public void setBundleName(String bundleName) {
    BundleName = bundleName;
  }

  public String getCurrency() {
    return Currency;
  }

  public void setCurrency(String currency) {
    Currency = currency;
  }

  public String getNominal() {
    return Nominal;
  }

  public void setNominal(String nominal) {
    Nominal = nominal;
  }

  public String getBuySell() {
    return BuySell;
  }

  public void setBuySell(String buySell) {
    BuySell = buySell;
  }

  public String getTradeDate() {
    return TradeDate;
  }

  public void setTradeDate(String tradeDate) {
    TradeDate = tradeDate;
  }

  public String getSettlementDate() {
    return SettlementDate;
  }

  public void setSettlementDate(String settlementDate) {
    SettlementDate = settlementDate;
  }

  public String getStartDate() {
    return StartDate;
  }

  public void setStartDate(String startDate) {
    StartDate = startDate;
  }

  public String getMaturityDate() {
    return MaturityDate;
  }

  public void setMaturityDate(String maturityDate) {
    MaturityDate = maturityDate;
  }

  public String getTraderName() {
    return TraderName;
  }

  public void setTraderName(String traderName) {
    TraderName = traderName;
  }

  public String getSalesPerson() {
    return SalesPerson;
  }

  public void setSalesPerson(String salesPerson) {
    SalesPerson = salesPerson;
  }

  public String getComment() {
    return Comment;
  }

  public void setComment(String comment) {
    Comment = comment;
  }

  public String getTemplateName() {
    return TemplateName;
  }

  public void setTemplateName(String templateName) {
    TemplateName = templateName;
  }

  public String getRateType() {
    return RateType;
  }

  public void setRateType(String rateType) {
    RateType = rateType;
  }

  public String getLoanDeposit() {
    return LoanDeposit;
  }

  public void setLoanDeposit(String loanDeposit) {
    LoanDeposit = loanDeposit;
  }

  public String getAnotherCurrency() {
    return anotherCurrency;
  }

  public void setAnotherCurrency(String anotherCurrency) {
    this.anotherCurrency = anotherCurrency;
  }

  public String getRate() {
    return Rate;
  }

  public void setRate(String rate) {
    Rate = rate;
  }

  public String getSalesMargin() {
    return SalesMargin;
  }

  public void setSalesMargin(String salesMargin) {
    SalesMargin = salesMargin;
  }

  public String getDiscount() {
    return Discount;
  }

  public void setDiscount(String discount) {
    Discount = discount;
  }

  public String getDiscountMethod() {
    return DiscountMethod;
  }

  public void setDiscountMethod(String discountMethod) {
    DiscountMethod = discountMethod;
  }

  public String getCapitalized() {
    return Capitalized;
  }

  public void setCapitalized(String capitalized) {
    Capitalized = capitalized;
  }

  public String getRoundingMethod() {
    return RoundingMethod;
  }

  public void setRoundingMethod(String roundingMethod) {
    RoundingMethod = roundingMethod;
  }

  public String getIndex() {
    return Index;
  }

  public void setIndex(String index) {
    Index = index;
  }

  public String getIndexSource() {
    return IndexSource;
  }

  public void setIndexSource(String indexSource) {
    IndexSource = indexSource;
  }

  public String getIndexFactor() {
    return IndexFactor;
  }

  public void setIndexFactor(String indexFactor) {
    IndexFactor = indexFactor;
  }

  public String getSpread() {
    return Spread;
  }

  public void setSpread(String spread) {
    Spread = spread;
  }

  public String getTenor() {
    return Tenor;
  }

  public void setTenor(String tenor) {
    Tenor = tenor;
  }

  public String getStubPeriod() {
    return StubPeriod;
  }

  public void setStubPeriod(String stubPeriod) {
    StubPeriod = stubPeriod;
  }

  public String getSpecificFirstDate() {
    return SpecificFirstDate;
  }

  public void setSpecificFirstDate(String specificFirstDate) {
    SpecificFirstDate = specificFirstDate;
  }

  public String getSpecificLastDate() {
    return SpecificLastDate;
  }

  public void setSpecificLastDate(String specificLastDate) {
    SpecificLastDate = specificLastDate;
  }

  public String getResetPeriod() {
    return ResetPeriod;
  }

  public void setResetPeriod(String resetPeriod) {
    ResetPeriod = resetPeriod;
  }

  public String getFirstReset() {
    return FirstReset;
  }

  public void setFirstReset(String firstReset) {
    FirstReset = firstReset;
  }

  public String getFirstRate() {
    return FirstRate;
  }

  public void setFirstRate(String firstRate) {
    FirstRate = firstRate;
  }

  public String getHolidayCode() {
    return HolidayCode;
  }

  public void setHolidayCode(String holidayCode) {
    HolidayCode = holidayCode;
  }

  public String getCompound() {
    return Compound;
  }

  public void setCompound(String compound) {
    Compound = compound;
  }

  public String getCompoundMethod() {
    return CompoundMethod;
  }

  public void setCompoundMethod(String compoundMethod) {
    CompoundMethod = compoundMethod;
  }

  public String getCompoundFrequency() {
    return CompoundFrequency;
  }

  public void setCompoundFrequency(String compoundFrequency) {
    CompoundFrequency = compoundFrequency;
  }

  public String getResetFrequency() {
    return ResetFrequency;
  }

  public void setResetFrequency(String resetFrequency) {
    ResetFrequency = resetFrequency;
  }

  public String getResetMethod() {
    return ResetMethod;
  }

  public void setResetMethod(String resetMethod) {
    ResetMethod = resetMethod;
  }

  public String getSamplePeriodRule() {
    return SamplePeriodRule;
  }

  public void setSamplePeriodRule(String samplePeriodRule) {
    SamplePeriodRule = samplePeriodRule;
  }

  public String getCustomSamplePeriod() {
    return CustomSamplePeriod;
  }

  public void setCustomSamplePeriod(String customSamplePeriod) {
    CustomSamplePeriod = customSamplePeriod;
  }

  public String getResetLag() {
    return ResetLag;
  }

  public void setResetLag(String resetLag) {
    ResetLag = resetLag;
  }

  public String getResetOffsetBusDayB() {
    return ResetOffsetBusDayB;
  }

  public void setResetOffsetBusDayB(String resetOffsetBusDayB) {
    ResetOffsetBusDayB = resetOffsetBusDayB;
  }

  public String getResetDateRoll() {
    return ResetDateRoll;
  }

  public void setResetDateRoll(String resetDateRoll) {
    ResetDateRoll = resetDateRoll;
  }

  public String getResetHolidays() {
    return ResetHolidays;
  }

  public void setResetHolidays(String resetHolidays) {
    ResetHolidays = resetHolidays;
  }

  public String getFloater() {
    return Floater;
  }

  public void setFloater(String floater) {
    Floater = floater;
  }

  public String getFloaterType() {
    return FloaterType;
  }

  public void setFloaterType(String floaterType) {
    FloaterType = floaterType;
  }

  public String getCap() {
    return Cap;
  }

  public void setCap(String cap) {
    Cap = cap;
  }

  public String getFloor() {
    return Floor;
  }

  public void setFloor(String floor) {
    Floor = floor;
  }

  public String getFrequency() {
    return Frequency;
  }

  public void setFrequency(String frequency) {
    Frequency = frequency;
  }

  public String getPaymentLag() {
    return PaymentLag;
  }

  public void setPaymentLag(String paymentLag) {
    PaymentLag = paymentLag;
  }

  public String getCouponOffsetBusDayB() {
    return CouponOffsetBusDayB;
  }

  public void setCouponOffsetBusDayB(String couponOffsetBusDayB) {
    CouponOffsetBusDayB = couponOffsetBusDayB;
  }

  public String getRollDay() {
    return RollDay;
  }

  public void setRollDay(String rollDay) {
    RollDay = rollDay;
  }

  public String getDateRoll() {
    return DateRoll;
  }

  public void setDateRoll(String dateRoll) {
    DateRoll = dateRoll;
  }

  public String getDayCount() {
    return DayCount;
  }

  public void setDayCount(String dayCount) {
    DayCount = dayCount;
  }

  public String getPrincipalExchange() {
    return PrincipalExchange;
  }

  public void setPrincipalExchange(String principalExchange) {
    PrincipalExchange = principalExchange;
  }

  public String getAccrualAdj() {
    return AccrualAdj;
  }

  public void setAccrualAdj(String accrualAdj) {
    AccrualAdj = accrualAdj;
  }

  public String getAmortType() {
    return AmortType;
  }

  public void setAmortType(String amortType) {
    AmortType = amortType;
  }

  public String getBaseAmount() {
    return BaseAmount;
  }

  public void setBaseAmount(String baseAmount) {
    BaseAmount = baseAmount;
  }

  public String getAmortStartDate() {
    return AmortStartDate;
  }

  public void setAmortStartDate(String amortStartDate) {
    AmortStartDate = amortStartDate;
  }

  public String getAmortEndDate() {
    return AmortEndDate;
  }

  public void setAmortEndDate(String amortEndDate) {
    AmortEndDate = amortEndDate;
  }

  public String getIncrement() {
    return Increment;
  }

  public void setIncrement(String increment) {
    Increment = increment;
  }

  public String getAmortFrequency() {
    return AmortFrequency;
  }

  public void setAmortFrequency(String amortFrequency) {
    AmortFrequency = amortFrequency;
  }

  public String getOperator() {
    return Operator;
  }

  public void setOperator(String operator) {
    Operator = operator;
  }

  public String getTermAsDate() {
    return TermAsDate;
  }

  public void setTermAsDate(String termAsDate) {
    TermAsDate = termAsDate;
  }

  public String getTermDate() {
    return TermDate;
  }

  public void setTermDate(String termDate) {
    TermDate = termDate;
  }

  public String getUseLoanTerm() {
    return UseLoanTerm;
  }

  public void setUseLoanTerm(String useLoanTerm) {
    UseLoanTerm = useLoanTerm;
  }

  public String getLoanTerm() {
    return LoanTerm;
  }

  public void setLoanTerm(String loanTerm) {
    LoanTerm = loanTerm;
  }

  public String getOpenTerm() {
    return OpenTerm;
  }

  public void setOpenTerm(String openTerm) {
    OpenTerm = openTerm;
  }

  public String getFeeType() {
    return feeType;
  }

  public void setFeeType(String feeType) {
    this.feeType = feeType;
  }

  public String getFeeStartDate() {
    return feeStartDate;
  }

  public void setFeeStartDate(String feeStartDate) {
    this.feeStartDate = feeStartDate;
  }

  public String getFeeEndDate() {
    return feeEndDate;
  }

  public void setFeeEndDate(String feeEndDate) {
    this.feeEndDate = feeEndDate;
  }

  public String getFeeKnownDate() {
    return feeKnownDate;
  }

  public void setFeeKnownDate(String feeKnownDate) {
    this.feeKnownDate = feeKnownDate;
  }

  public String getFeeLegalEntity() {
    return feeLegalEntity;
  }

  public void setFeeLegalEntity(String feeLegalEntity) {
    this.feeLegalEntity = feeLegalEntity;
  }

  public String getFeeDate() {
    return feeDate;
  }

  public void setFeeDate(String feeDate) {
    this.feeDate = feeDate;
  }

  public String getBrokerKeyword() {
    return brokerKeyword;
  }

  public void setBrokerKeyword(String brokerKeyword) {
    this.brokerKeyword = brokerKeyword;
  }

  @Override
  public String getCsvFileName() {
    return "Cash_New.csv";
  }
}
