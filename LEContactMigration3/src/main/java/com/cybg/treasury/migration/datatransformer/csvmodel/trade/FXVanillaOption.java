package com.cybg.treasury.migration.datatransformer.csvmodel.trade;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",", generateHeaderColumns = true)
public class FXVanillaOption implements CSVTrade {

  @DataField(pos = 0) private String Action = "NEW";
  @DataField(pos = 1) private String ExternalReference;
  @DataField(pos = 2) private String InternalReference;
  @DataField(pos = 3) private String TradeId;
  @DataField(pos = 4) private String CounterPartyRole = "CounterParty";
  @DataField(pos = 5) private String Book;
  @DataField(pos = 6) private String Counterparty;
  @DataField(pos = 7) private String BundleType = "PricingSheet";
  @DataField(pos = 8) private String BundleName;
  @DataField(pos = 9) private String BuySell;
  @DataField(pos = 10) private String Notional;
  @DataField(pos = 11) private String TradeDatetime;
  @DataField(pos = 12) private String TradeSettleDate;
  @DataField(pos = 13) private String StartDate;
  @DataField(pos = 14) private String MaturityDate;
  @DataField(pos = 15) private String Trader;
  @DataField(pos = 16) private String SalesPerson;
  @DataField(pos = 17) private String TemplateName;
  @DataField(pos = 18) private String Comments;
  @DataField(pos = 19) private String HolidayCode;
  @DataField(pos = 20) private String OptionStyle = "VANILLA";
  @DataField(pos = 21) private String ExerciseType;
  @DataField(pos = 22) private String OptionType;
  @DataField(pos = 23) private String PrimaryCurrency;
  @DataField(pos = 24) private String SecondaryCurrency;
  @DataField(pos = 25) private String PrimaryAmount;
  @DataField(pos = 26) private String QuotingAmount;
  @DataField(pos = 27) private String Strike;
  @DataField(pos = 28) private String FirstExerciseDate;
  @DataField(pos = 29) private String DeliveryHolidays;
  @DataField(pos = 30) private String FXReset;
  @DataField(pos = 31) private String AutoExercise;
  @DataField(pos = 32) private String ExpiryTime;
  @DataField(pos = 33) private String ExpiryTimeZone;
  @DataField(pos = 34) private String SettlementType;
  @DataField(pos = 35) private String SettlementCurrency;
  @DataField(pos = 36, columnName = "keyword.PSStrategy") private String psStrategyKeyword;
  @DataField(pos = 37, columnName = "Fee.Type") private String feeType;
  @DataField(pos = 38, columnName = "Fee.Amount") private String feeAmount;
  @DataField(pos = 39, columnName = "Fee.Direction") private String feeDirection;
  @DataField(pos = 40, columnName = "Fee.Currency") private String feeCurrency;
  @DataField(pos = 41, columnName = "Fee.LegalEntity") private String feeLegalEntity;
  @DataField(pos = 42, columnName = "Fee.StartDate") private String feeStartDate;
  @DataField(pos = 43, columnName = "Fee.EndDate") private String feeEndDate;
  @DataField(pos = 44, columnName = "Fee.KnownDate") private String feeKnownDate;
  @DataField(pos = 45, columnName = "Fee.Date")  private String feeDate;

  public String getAction() {
    return Action;
  }

  public void setAction(String action) {
    Action = action;
  }

  public String getExternalReference() {
    return ExternalReference;
  }

  public void setExternalReference(String externalReference) {
    ExternalReference = externalReference;
  }

  public String getInternalReference() {
    return InternalReference;
  }

  public void setInternalReference(String internalReference) {
    InternalReference = internalReference;
  }

  public String getTradeId() {
    return TradeId;
  }

  public void setTradeId(String tradeId) {
    TradeId = tradeId;
  }

  public String getCounterPartyRole() {
    return CounterPartyRole;
  }

  public void setCounterPartyRole(String counterPartyRole) {
    CounterPartyRole = counterPartyRole;
  }

  public String getBook() {
    return Book;
  }

  public void setBook(String book) {
    Book = book;
  }

  public String getCounterparty() {
    return Counterparty;
  }

  public void setCounterparty(String counterparty) {
    Counterparty = counterparty;
  }

  public String getBundleType() {
    return BundleType;
  }

  public void setBundleType(String bundleType) {
    BundleType = bundleType;
  }

  public String getBundleName() {
    return BundleName;
  }

  public void setBundleName(String bundleName) {
    BundleName = bundleName;
  }

  public String getBuySell() {
    return BuySell;
  }

  public void setBuySell(String buySell) {
    BuySell = buySell;
  }

  public String getNotional() {
    return Notional;
  }

  public void setNotional(String notional) {
    Notional = notional;
  }

  public String getTradeDatetime() {
    return TradeDatetime;
  }

  public void setTradeDatetime(String tradeDatetime) {
    TradeDatetime = tradeDatetime;
  }

  public String getTradeSettleDate() {
    return TradeSettleDate;
  }

  public void setTradeSettleDate(String tradeSettleDate) {
    TradeSettleDate = tradeSettleDate;
  }

  public String getStartDate() {
    return StartDate;
  }

  public void setStartDate(String startDate) {
    StartDate = startDate;
  }

  public String getMaturityDate() {
    return MaturityDate;
  }

  public void setMaturityDate(String maturityDate) {
    MaturityDate = maturityDate;
  }

  public String getTrader() {
    return Trader;
  }

  public void setTrader(String trader) {
    Trader = trader;
  }

  public String getSalesPerson() {
    return SalesPerson;
  }

  public void setSalesPerson(String salesPerson) {
    SalesPerson = salesPerson;
  }

  public String getTemplateName() {
    return TemplateName;
  }

  public void setTemplateName(String templateName) {
    TemplateName = templateName;
  }

  public String getComments() {
    return Comments;
  }

  public void setComments(String comments) {
    Comments = comments;
  }

  public String getHolidayCode() {
    return HolidayCode;
  }

  public void setHolidayCode(String holidayCode) {
    HolidayCode = holidayCode;
  }

  public String getOptionStyle() {
    return OptionStyle;
  }

  public void setOptionStyle(String optionStyle) {
    OptionStyle = optionStyle;
  }

  public String getExerciseType() {
    return ExerciseType;
  }

  public void setExerciseType(String exerciseType) {
    ExerciseType = exerciseType;
  }

  public String getOptionType() {
    return OptionType;
  }

  public void setOptionType(String optionType) {
    OptionType = optionType;
  }

  public String getPrimaryCurrency() {
    return PrimaryCurrency;
  }

  public void setPrimaryCurrency(String primaryCurrency) {
    PrimaryCurrency = primaryCurrency;
  }

  public String getSecondaryCurrency() {
    return SecondaryCurrency;
  }

  public void setSecondaryCurrency(String secondaryCurrency) {
    SecondaryCurrency = secondaryCurrency;
  }

  public String getPrimaryAmount() {
    return PrimaryAmount;
  }

  public void setPrimaryAmount(String primaryAmount) {
    PrimaryAmount = primaryAmount;
  }

  public String getQuotingAmount() {
    return QuotingAmount;
  }

  public void setQuotingAmount(String quotingAmount) {
    QuotingAmount = quotingAmount;
  }

  public String getStrike() {
    return Strike;
  }

  public void setStrike(String strike) {
    Strike = strike;
  }

  public String getFirstExerciseDate() {
    return FirstExerciseDate;
  }

  public void setFirstExerciseDate(String firstExerciseDate) {
    FirstExerciseDate = firstExerciseDate;
  }

  public String getDeliveryHolidays() {
    return DeliveryHolidays;
  }

  public void setDeliveryHolidays(String deliveryHolidays) {
    DeliveryHolidays = deliveryHolidays;
  }

  public String getFXReset() {
    return FXReset;
  }

  public void setFXReset(String FXReset) {
    this.FXReset = FXReset;
  }

  public String getAutoExercise() {
    return AutoExercise;
  }

  public void setAutoExercise(String autoExercise) {
    AutoExercise = autoExercise;
  }

  public String getExpiryTime() {
    return ExpiryTime;
  }

  public void setExpiryTime(String expiryTime) {
    ExpiryTime = expiryTime;
  }

  public String getExpiryTimeZone() {
    return ExpiryTimeZone;
  }

  public void setExpiryTimeZone(String expiryTimeZone) {
    ExpiryTimeZone = expiryTimeZone;
  }

  public String getSettlementType() {
    return SettlementType;
  }

  public void setSettlementType(String settlementType) {
    SettlementType = settlementType;
  }

  public String getSettlementCurrency() {
    return SettlementCurrency;
  }

  public void setSettlementCurrency(String settlementCurrency) {
    SettlementCurrency = settlementCurrency;
  }

  public String getPsStrategyKeyword() {
    return psStrategyKeyword;
  }

  public void setPsStrategyKeyword(String psStrategyKeyword) {
    this.psStrategyKeyword = psStrategyKeyword;
  }

  public String getFeeType() {
    return feeType;
  }

  public void setFeeType(String feeType) {
    this.feeType = feeType;
  }

  public String getFeeAmount() {
    return feeAmount;
  }

  public void setFeeAmount(String feeAmount) {
    this.feeAmount = feeAmount;
  }

  public String getFeeDirection() {
    return feeDirection;
  }

  public void setFeeDirection(String feeDirection) {
    this.feeDirection = feeDirection;
  }

  public String getFeeCurrency() {
    return feeCurrency;
  }

  public void setFeeCurrency(String feeCurrency) {
    this.feeCurrency = feeCurrency;
  }

  public String getFeeLegalEntity() {
    return feeLegalEntity;
  }

  public void setFeeLegalEntity(String feeLegalEntity) {
    this.feeLegalEntity = feeLegalEntity;
  }

  public String getFeeStartDate() {
    return feeStartDate;
  }

  public void setFeeStartDate(String feeStartDate) {
    this.feeStartDate = feeStartDate;
  }

  public String getFeeEndDate() {
    return feeEndDate;
  }

  public void setFeeEndDate(String feeEndDate) {
    this.feeEndDate = feeEndDate;
  }

  public String getFeeKnownDate() {
    return feeKnownDate;
  }

  public void setFeeKnownDate(String feeKnownDate) {
    this.feeKnownDate = feeKnownDate;
  }

  public String getFeeDate() {
    return feeDate;
  }

  public void setFeeDate(String feeDate) {
    this.feeDate = feeDate;
  }

  @Override
  public String getCsvFileName() {
    return "FXOPTION_VANILLA_Sample.csv";
  }
}