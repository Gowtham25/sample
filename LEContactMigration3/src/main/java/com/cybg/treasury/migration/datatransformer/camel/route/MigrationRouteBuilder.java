package com.cybg.treasury.migration.datatransformer.camel.route;

interface MigrationRouteBuilder {
  String getSupportedType();
}
