package com.cybg.treasury.migration.datatransformer.transform.mxml;

import java.util.ArrayList;
import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.trade.FXNDF;
import com.cybg.treasury.migration.murex.jaxb.model.Trade;

/**
 * @author theo on 14/12/2016.
 */
public class FXNDFTradeTranslator extends AbstractTradeTranslator<FXNDF> {

  @Override
  public FXNDF translate(String bundleName, Trade trade) {
    FXNDF csvTrade = new FXNDF();
    csvTrade.setBundleName(bundleName);
    //TODO implement
    return csvTrade;
  }

  @Override
  public List<String> getSupportedTypologies() {
    List<String> typologies = new ArrayList<>();
    typologies.add("FX_NDF");
    return typologies;
  }
}
