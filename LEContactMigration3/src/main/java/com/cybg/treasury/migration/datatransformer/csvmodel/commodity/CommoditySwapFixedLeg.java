package com.cybg.treasury.migration.datatransformer.csvmodel.commodity;

import org.apache.camel.dataformat.bindy.annotation.DataField;

public class CommoditySwapFixedLeg {
  @DataField(pos = 26, columnName = "CommoditySwapLeg.SwapDirection") private String swapDirection;
  @DataField(pos = 27, columnName = "CommoditySwapLeg.CommodityType") private String commodityType;
  @DataField(pos = 28, columnName = "CommoditySwapLeg.CommodityResetName") private String commodityResetName;
  @DataField(pos = 29, columnName = "CommoditySwapLeg.Strike") private String strike;
  @DataField(pos = 30, columnName = "CommoditySwapLeg.BuySellUnits") private String buySellUnits;
  @DataField(pos = 31, columnName = "CommoditySwapLeg.ParStrike") private String parStrike;

  public void setSwapDirection(String swapDirection) {
    this.swapDirection = swapDirection;
  }

  public void setCommodityType(String commodityType) {
    this.commodityType = commodityType;
  }

  public void setCommodityResetName(String commodityResetName) {
    this.commodityResetName = commodityResetName;
  }

  public void setStrike(String strike) {
    this.strike = strike;
  }

  public void setBuySellUnits(String buySellUnits) {
    this.buySellUnits = buySellUnits;
  }

  public void setParStrike(String parStrike) {
    this.parStrike = parStrike;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    CommoditySwapFixedLeg that = (CommoditySwapFixedLeg)o;

    if(swapDirection != null ? !swapDirection.equals(that.swapDirection) : that.swapDirection != null) return false;
    if(commodityType != null ? !commodityType.equals(that.commodityType) : that.commodityType != null) return false;
    if(commodityResetName != null ? !commodityResetName.equals(that.commodityResetName) : that.commodityResetName != null)
      return false;
    if(strike != null ? !strike.equals(that.strike) : that.strike != null) return false;
    if(buySellUnits != null ? !buySellUnits.equals(that.buySellUnits) : that.buySellUnits != null) return false;
    return parStrike != null ? parStrike.equals(that.parStrike) : that.parStrike == null;

  }

  @Override
  public int hashCode() {
    int result = swapDirection != null ? swapDirection.hashCode() : 0;
    result = 31 * result + (commodityType != null ? commodityType.hashCode() : 0);
    result = 31 * result + (commodityResetName != null ? commodityResetName.hashCode() : 0);
    result = 31 * result + (strike != null ? strike.hashCode() : 0);
    result = 31 * result + (buySellUnits != null ? buySellUnits.hashCode() : 0);
    result = 31 * result + (parStrike != null ? parStrike.hashCode() : 0);
    return result;
  }
}
