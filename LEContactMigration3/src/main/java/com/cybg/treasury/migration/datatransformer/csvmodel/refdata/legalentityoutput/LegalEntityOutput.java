package com.cybg.treasury.migration.datatransformer.csvmodel.refdata.legalentityoutput;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVOutput;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/**
 * @author theo on 05/01/2017.
 */
@CsvRecord(separator = "#", generateHeaderColumns = true)
public class LegalEntityOutput implements CSVOutput {

  @DataField(pos = 0) private String Action = "NEW";
  @DataField(pos = 1) private String LongName;
  @DataField(pos = 2) private String ShortName;
  @DataField(pos = 3) private String Parent;
  @DataField(pos = 4) private String Country;
  @DataField(pos = 5) private String InactiveDate;
  @DataField(pos = 6) private String User = "calypso_user";
  @DataField(pos = 7) private String ExternalReference;
  @DataField(pos = 8) private String HolidayCode;
  @DataField(pos = 9) private String Financial;
  @DataField(pos = 10) private String Status;
  @DataField(pos = 11) private String Roles = "CounterParty";
  @DataField(pos = 12) private String Comment;

  public LegalEntityOutput() {
  }

  public LegalEntityOutput(String action, String longName, String shortName, String parent, String country, String inactiveDate, String user, String externalReference, String holidayCode, String financial, String status, String roles, String comment) {
    Action = action;
    LongName = longName;
    ShortName = shortName;
    Parent = parent;
    Country = country;
    InactiveDate = inactiveDate;
    User = user;
    ExternalReference = externalReference;
    HolidayCode = holidayCode;
    Financial = financial;
    Status = status;
    Roles = roles;
    Comment = comment;
  }

  public void setAction(String action) {
    Action = action;
  }

  public void setLongName(String longName) {
    LongName = longName;
  }

  public void setShortName(String shortName) {
    ShortName = shortName;
  }

  public void setParent(String parent) {
    Parent = parent;
  }

  public void setCountry(String country) {
    Country = country;
  }

  public void setInactiveDate(String inactiveDate) {
    InactiveDate = inactiveDate;
  }

  public void setUser(String user) {
    User = user;
  }

  public void setExternalReference(String externalReference) {
    ExternalReference = externalReference;
  }

  public void setHolidayCode(String holidayCode) {
    HolidayCode = holidayCode;
  }

  public void setFinancial(String financial) {
    Financial = financial;
  }

  public void setStatus(String status) {
    Status = status;
  }

  public void setRoles(String roles) {
    Roles = roles;
  }

  public void setComment(String comment) {
    Comment = comment;
  }

  @Override
  public String getFileName() {
    return "LegalEntity_NEW.csv";
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    LegalEntityOutput that = (LegalEntityOutput)o;

    if(Action != null ? !Action.equals(that.Action) : that.Action != null) return false;
    if(LongName != null ? !LongName.equals(that.LongName) : that.LongName != null) return false;
    if(ShortName != null ? !ShortName.equals(that.ShortName) : that.ShortName != null) return false;
    if(Parent != null ? !Parent.equals(that.Parent) : that.Parent != null) return false;
    if(Country != null ? !Country.equals(that.Country) : that.Country != null) return false;
    if(InactiveDate != null ? !InactiveDate.equals(that.InactiveDate) : that.InactiveDate != null) return false;
    if(User != null ? !User.equals(that.User) : that.User != null) return false;
    if(ExternalReference != null ? !ExternalReference.equals(that.ExternalReference) : that.ExternalReference != null)
      return false;
    if(HolidayCode != null ? !HolidayCode.equals(that.HolidayCode) : that.HolidayCode != null) return false;
    if(Financial != null ? !Financial.equals(that.Financial) : that.Financial != null) return false;
    if(Status != null ? !Status.equals(that.Status) : that.Status != null) return false;
    if(Roles != null ? !Roles.equals(that.Roles) : that.Roles != null) return false;
    return Comment != null ? Comment.equals(that.Comment) : that.Comment == null;

  }

  @Override
  public int hashCode() {
    int result = Action != null ? Action.hashCode() : 0;
    result = 31 * result + (LongName != null ? LongName.hashCode() : 0);
    result = 31 * result + (ShortName != null ? ShortName.hashCode() : 0);
    result = 31 * result + (Parent != null ? Parent.hashCode() : 0);
    result = 31 * result + (Country != null ? Country.hashCode() : 0);
    result = 31 * result + (InactiveDate != null ? InactiveDate.hashCode() : 0);
    result = 31 * result + (User != null ? User.hashCode() : 0);
    result = 31 * result + (ExternalReference != null ? ExternalReference.hashCode() : 0);
    result = 31 * result + (HolidayCode != null ? HolidayCode.hashCode() : 0);
    result = 31 * result + (Financial != null ? Financial.hashCode() : 0);
    result = 31 * result + (Status != null ? Status.hashCode() : 0);
    result = 31 * result + (Roles != null ? Roles.hashCode() : 0);
    result = 31 * result + (Comment != null ? Comment.hashCode() : 0);
    return result;
  }
}
