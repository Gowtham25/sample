package com.cybg.treasury.migration.datatransformer.camel.route;

import java.util.concurrent.CountDownLatch;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Refdata2CDUFRouteBuilder extends RouteBuilder {
  private final String refdataInputDirectory;
  private final String refdataInputfilename;
  private final String refdataOutputDirectory;
  private final Processor csv2CDUFProcessor;
  private final Processor bindyUnmarshalProcessor;
  private final Processor bindyMarshalProcessor;
  private final Processor errorHandler;
  private final CountDownLatch latch;

  @Autowired
  public Refdata2CDUFRouteBuilder(@Value("#{environment['migrationdata.input.dir']}") String refdataInputDirectory,
                                   @Value("#{environment['migrationdata.output.dir']}") String refdataOutputDirectory,
                                   @Value("#{environment['migrationdata.input.filename']}") String refdataInputfilename,
                                   Processor csv2CDUFProcessor,
                                   Processor bindyMarshalProcessor,
                                   Processor bindyUnmarshalProcessor,
                                   Processor errorLoggingProcessor,
                                   CountDownLatch latch) {
    this.refdataInputDirectory = refdataInputDirectory;
    this.refdataOutputDirectory = refdataOutputDirectory;
    this.refdataInputfilename = refdataInputfilename;
    this.csv2CDUFProcessor = csv2CDUFProcessor;
    this.bindyUnmarshalProcessor = bindyUnmarshalProcessor;
    this.bindyMarshalProcessor = bindyMarshalProcessor;
    this.errorHandler = errorLoggingProcessor;
    this.latch = latch;
  }

  @Override
  @SuppressWarnings("unchecked")
  public void configure() throws Exception {

    String inputEndpoint = refdataInputDirectory + "?move=.done&moveFailed=.error&fileName=" + refdataInputfilename;

    onException(Exception.class)
      .logStackTrace(true)
      .process(errorHandler)
      .stop();

    from(inputEndpoint)
      .process(bindyUnmarshalProcessor)
      .process(csv2CDUFProcessor)
      .process(bindyMarshalProcessor)
      .to(refdataOutputDirectory)
      .process(exchange -> latch.countDown());
  }
}
