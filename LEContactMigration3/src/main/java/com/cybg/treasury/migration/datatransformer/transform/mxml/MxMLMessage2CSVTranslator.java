package com.cybg.treasury.migration.datatransformer.transform.mxml;

import java.util.ArrayList;
import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.trade.CSVTrade;
import com.cybg.treasury.migration.murex.jaxb.model.MxML;
import com.cybg.treasury.migration.murex.jaxb.model.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author theo on 13/12/2016.
 */
@Component
public class MxMLMessage2CSVTranslator {

  private final CSVTradeTranslatorFinder tradeTranslatorFinder;

  @Autowired
  public MxMLMessage2CSVTranslator(CSVTradeTranslatorFinder tradeTranslatorFinder) {
    this.tradeTranslatorFinder = tradeTranslatorFinder;
  }

  public List<CSVTrade> translate(MxML mxML) {
    List<CSVTrade> csvTrades = new ArrayList<>();
    List<Trade> trades = mxML.getTrades().getTrade();
    String packageId = mxML.getPackages().getPackage().getId().replace("package", "MX");
    for(Trade trade : trades) {
      String typology = trade.getTradeHeader().getTradeCategory().getTypology();
      MxML2CSVTradeTranslator cdufTranslator = tradeTranslatorFinder.find(typology);
      csvTrades.add(cdufTranslator.translate(packageId, trade));
    }
    return csvTrades;
  }
}
