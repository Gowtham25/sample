package com.cybg.treasury.migration.datatransformer.transform.csv.refdata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cybg.treasury.migration.datatransformer.transform.csv.CsvToCsvDataTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author theo on 13/12/2016.
 */
@Component
public class RefdataTransformerFinder {

  private final List<CsvToCsvDataTransformer> CsvToCsvDataTransformers;
  private Map<String, CsvToCsvDataTransformer> refdata2CDUFTransformerMap = new HashMap<>(5);

  @Autowired
  public RefdataTransformerFinder(List<CsvToCsvDataTransformer> CsvToCsvDataTransformers) {
    this.CsvToCsvDataTransformers = CsvToCsvDataTransformers;
    populateTranslatorMap();
  }

  public CsvToCsvDataTransformer find(String filename) {
    return refdata2CDUFTransformerMap.get(filename);
  }

  @SuppressWarnings("unchecked")
  private void populateTranslatorMap() {
    for(CsvToCsvDataTransformer refdataTransformer : CsvToCsvDataTransformers) {
      String supportedType = refdataTransformer.getSupportedType();
      refdata2CDUFTransformerMap.put(supportedType, refdataTransformer);
    }
  }
}
