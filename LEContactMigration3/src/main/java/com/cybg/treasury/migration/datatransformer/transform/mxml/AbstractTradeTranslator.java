package com.cybg.treasury.migration.datatransformer.transform.mxml;

import java.util.List;
import java.util.Optional;

import com.cybg.treasury.migration.datatransformer.csvmodel.trade.CSVTrade;
import com.cybg.treasury.migration.murex.jaxb.model.*;

/**
 * @author theo on 15/12/2016.
 */
public abstract class AbstractTradeTranslator<T extends CSVTrade> implements MxML2CSVTradeTranslator<T> {

  public static final String NAB = "NAB";
  public static final String PUT = "Put";
  public static final String CALL = "Call";
  public static final String BUY = "Buy";
  public static final String SELL = "Sell";
  public static final String CURRENCY_1_PER_CURRENCY_2 = "currency1PerCurrency2";

  protected String getExternalReference(Trade trade) {
    return trade.getId();
  }

  protected String getBook(Trade trade) {
    //TODO we need mapping logic depending on how we model this in Calypso
    return trade.getPortfolios().getPortfolio().getId();
  }

  protected String getCounterparty(Trade trade) {
    List<Party> parties = trade.getParties().getParty();
    Optional<Party> nabParty = parties.stream().filter(party -> !party.getId().equals(NAB)).findFirst();
    //TODO we need mapping logic depending on how we model this in Calypso
    return nabParty.orElse(new Party()).getId();
  }

  protected String getTradeDateTime(Trade trade) {
    return trade.getTradeHeader().getTradeDate().toString();
  }

  protected String getTrader(Trade trade) {
    TradeView nabTradeView = getNABTradeView(trade);
    return nabTradeView.getUserName();
  }

  protected String getComments(Trade trade) {
    String comnt = "";
    Comments comments = getNABTradeView(trade).getComments();
    if (comments != null) {
      List<Comment> commentList = comments.getComment();
      comnt = commentList.stream().map(Comment::getContent).reduce("", (s, s2) -> s.concat(s2).concat(";"));
    }
    return comnt;
  }

  protected String getSettlementCurrency(Trade trade) {
    return getSettlementInstructionKey(trade).getFlowCurrency();
  }

  protected String getSettlementType(Trade trade) {
    return getSettlementInstructionKey(trade).getSettlementType();
  }

  protected SettlementInstructionKey getSettlementInstructionKey(Trade trade) {
    return trade.getSettlementInstructions().getSettlementInstruction().get(0).getSettlementInstructionKey();
  }

  private TradeView getNABTradeView(Trade trade) {
    List<TradeView> tradeView = trade.getTradeHeader().getTradeViews().getTradeView();
    return tradeView.parallelStream().filter(tradeVue -> !tradeVue.getPartyReference().getHref().equals(NAB)).findFirst().orElse(new TradeView());
  }


}
