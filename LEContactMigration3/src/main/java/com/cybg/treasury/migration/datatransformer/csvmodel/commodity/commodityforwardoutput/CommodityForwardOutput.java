package com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commodityforwardoutput;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVOutput;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

// TODO complete implementation
@CsvRecord(separator = "#", generateHeaderColumns = true)
public class CommodityForwardOutput implements CSVOutput {
  @DataField(pos = 1, columnName = "Action") private String action = "NEW";
  @DataField(pos = 2, columnName = "Counterparty") private String counterparty;
  @DataField(pos = 3, columnName = "External Reference") private String externalReference;

  public void setCounterparty(String counterparty) {
    this.counterparty = counterparty;
  }

  public void setExternalReference(String externalReference) {
    this.externalReference = externalReference;
  }

  @Override
  public String getFileName() {
    return "COMMODITYFORWARD_NEW.csv";
  }
}
