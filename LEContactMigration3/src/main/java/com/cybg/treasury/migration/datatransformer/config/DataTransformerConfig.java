package com.cybg.treasury.migration.datatransformer.config;

import java.util.concurrent.CountDownLatch;

import org.apache.camel.Main;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(basePackages = {"com.cybg.treasury.migration.datatransformer.*"})
@PropertySource("classpath:application.properties")
public class DataTransformerConfig {

  @Bean
  public Main main() {
    return new Main();
  }

  @Bean
  public CountDownLatch countDownLatch() {
    //for more route builders we will have to change the latch size
    return new CountDownLatch(1);
  }

}