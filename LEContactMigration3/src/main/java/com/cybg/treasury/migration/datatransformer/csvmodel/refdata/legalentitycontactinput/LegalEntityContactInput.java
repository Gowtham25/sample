package com.cybg.treasury.migration.datatransformer.csvmodel.refdata.legalentitycontactinput;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/*
 * Simple Input POJO class LE Contacts Input
 * 
 * @DataField
 * This annotation comes with camel-bindy, which is used to map the values read directly with the
 * members declared in the class. This rules out the process of using constructor or setter
 * methods in the class to set the value to the data-members 
 * 
 */

@CsvRecord(separator = ",")
public class LegalEntityContactInput {

	@DataField(pos = 1) String wid_id;
	@DataField(pos = 2) String short_name;
	@DataField(pos = 3) String long_name;
	@DataField(pos = 4) String domicile_country;
	@DataField(pos = 5) String client_trading_address;
	@DataField(pos = 6) String client_registered_address;
	@DataField(pos = 7) String client_postal_address;
	@DataField(pos = 8) String contact_job_title;
	@DataField(pos = 9) String contact_job_function;
	@DataField(pos = 10) String contact_first_name;
	@DataField(pos = 11) String contact_middle_name;
	@DataField(pos = 12) String contact_last_name;
	@DataField(pos = 13) String product;
	@DataField(pos = 14) String contact_address;
	@DataField(pos = 15) String contact_phone;
	@DataField(pos = 16) String contact_fax;
	@DataField(pos = 17) String secondary_fax;
	@DataField(pos = 18) String contact_lotus_notes;
	@DataField(pos = 19) String contact_email;
	
	public LegalEntityContactInput() {
		// TODO Auto-generated constructor stub
		
		/*
		 * This constructor is added only coz of parameterised constructor is
		 * present in the class and also camel-bindy requires a default constructor
		 * to create an instance of this POJO
		 * 
		 */
	}
	
	//Parameterised Constructor needed for testing purposes
	public LegalEntityContactInput(
			String wid_id, 
			String short_name,
			String long_name, 
			String domicile_country,
			String client_trading_address, 
			String client_registered_address,
			String client_postal_address, 
			String contact_job_title,
			String contact_job_function, 
			String contact_first_name,
			String contact_middle_name, 
			String contact_last_name,
			String product, 
			String contact_address, 
			String contact_phone,
			String contact_fax, 
			String secondary_fax,
			String contact_lotus_notes, 
			String contact_email) {
		
		this.wid_id = wid_id;
		this.short_name = short_name;
		this.long_name = long_name;
		this.domicile_country = domicile_country;
		this.client_trading_address = client_trading_address;
		this.client_registered_address = client_registered_address;
		this.client_postal_address = client_postal_address;
		this.contact_job_title = contact_job_title;
		this.contact_job_function = contact_job_function;
		this.contact_first_name = contact_first_name;
		this.contact_middle_name = contact_middle_name;
		this.contact_last_name = contact_last_name;
		this.product = product;
		this.contact_address = contact_address;
		this.contact_phone = contact_phone;
		this.contact_fax = contact_fax;
		this.secondary_fax = secondary_fax;
		this.contact_lotus_notes = contact_lotus_notes;
		this.contact_email = contact_email;
	}
	//*/
	
	public String getWid_id() {
		return wid_id;
	}
	public String getShort_name() {
		return short_name;
	}
	public String getLong_name() {
		return long_name;
	}
	public String getDomicile_country() {
		return domicile_country;
	}
	public String getClient_trading_address() {
		return client_trading_address;
	}
	public String getClient_registered_address() {
		return client_registered_address;
	}
	public String getClient_postal_address() {
		return client_postal_address;
	}
	public String getContact_job_title() {
		return contact_job_title;
	}
	public String getContact_job_function() {
		return contact_job_function;
	}
	public String getContact_first_name() {
		return contact_first_name;
	}
	public String getContact_middle_name() {
		return contact_middle_name;
	}
	public String getContact_last_name() {
		return contact_last_name;
	}
	public String getProduct() {
		return product;
	}
	public String getContact_address() {
		return contact_address;
	}
	public String getContact_phone() {
		return contact_phone;
	}
	public String getContact_fax() {
		return contact_fax;
	}
	public String getSecondary_fax() {
		return secondary_fax;
	}
	public String getContact_lotus_notes() {
		return contact_lotus_notes;
	}
	public String getContact_email() {
		return contact_email;
	}
	
	
}
