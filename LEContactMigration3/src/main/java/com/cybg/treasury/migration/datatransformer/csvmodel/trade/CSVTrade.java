package com.cybg.treasury.migration.datatransformer.csvmodel.trade;

/**
 * @author theo on 13/12/2016.
            */
public interface CSVTrade {

  String getCsvFileName();
}
