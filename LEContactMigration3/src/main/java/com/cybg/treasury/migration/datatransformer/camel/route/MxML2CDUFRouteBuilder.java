package com.cybg.treasury.migration.datatransformer.camel.route;

import javax.inject.Named;

import com.cybg.treasury.migration.datatransformer.csvmodel.trade.CSVTrade;
import com.cybg.treasury.migration.datatransformer.csvmodel.trade.FXVanillaOption;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.model.dataformat.JaxbDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author theo on 01/12/2016.
 */
@Component
public class MxML2CDUFRouteBuilder extends RouteBuilder {

  private final Processor mxml2CDUFProcessor;
  private final Processor tradeFilterProcessor;
  private final Processor csvHeaderProcessor;
  private final Processor errorHandler;
  private final String mxmlInputDirectory;
  private final String csvOutputDirectory;
  private final int threads;

  @Autowired
  public MxML2CDUFRouteBuilder(@Value("#{environment['mxml.input.dir']}") String mxmlInputDirectory,
                                  @Value("#{environment['csv.output.dir']}") String csvOutputDirectory,
                                @Value("#{environment['threads']?:10}") int threads,
                                @Named("mxML2CDUFProcessor") Processor mxml2CDUFProcessor,
                                @Named("errorLoggingProcessor") Processor errorLoggingProcessor,
                                @Named("tradeFilterProcessor") Processor tradeFilterProcessor,
                                  @Named("csvHeaderProcessor")  Processor csvHeaderProcessor) {
    this.mxmlInputDirectory = mxmlInputDirectory;
    this.csvOutputDirectory = csvOutputDirectory;
    this.threads = threads;
    this.mxml2CDUFProcessor = mxml2CDUFProcessor;
    this.errorHandler = errorLoggingProcessor;
    this.tradeFilterProcessor = tradeFilterProcessor;
    this.csvHeaderProcessor = csvHeaderProcessor;
  }

  @Override
  public void configure() throws Exception {
    JaxbDataFormat jaxb = new JaxbDataFormat(true);
    jaxb.setContextPath("com.accenture.murex.jaxb.model");

    BindyCsvDataFormat bindyDF = new BindyCsvDataFormat(FXVanillaOption.class.getPackage().getName());

    String inputDirectory = mxmlInputDirectory + "?move=.done&moveFailed=.error";

    String outputDirectory = csvOutputDirectory + "?autoCreate=true&fileExist=Append";

    onException(Exception.class)
      .logStackTrace(true)
      .process(errorHandler)
      .stop();

    from(inputDirectory)
      .threads(threads)
      .unmarshal(jaxb)
      .process(tradeFilterProcessor)
      .process(mxml2CDUFProcessor)
      .split(body())
      .setHeader(Exchange.FILE_NAME, simple("${body.csvFileName}"))
      .process(exchange -> {
        //reset the data format and make it dynamic for each model
        CSVTrade csvTrade = exchange.getIn().getBody(CSVTrade.class);
        bindyDF.setModelFactory(null);
//        bindyDF.setClassType(csvTrade.getClass());//TODO
      }).marshal(bindyDF)
      .process(csvHeaderProcessor)
      .to(outputDirectory);

  }
}
