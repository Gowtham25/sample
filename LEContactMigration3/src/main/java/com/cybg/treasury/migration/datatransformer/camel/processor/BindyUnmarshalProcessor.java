package com.cybg.treasury.migration.datatransformer.camel.processor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVInput;
import com.cybg.treasury.migration.datatransformer.csvmodel.CSVInputTypeFinder;
import com.cybg.treasury.migration.datatransformer.exception.DataTransformerException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BindyUnmarshalProcessor implements Processor {
  private final CSVInputTypeFinder csvInputTypeFinder;

  @Autowired
  public BindyUnmarshalProcessor(CSVInputTypeFinder csvInputTypeFinder) {
    this.csvInputTypeFinder = csvInputTypeFinder;
  }

  @Override
  @SuppressWarnings("unchecked")
  public void process(Exchange exchange) throws Exception {
    String filename = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
    CSVInput csvInput = csvInputTypeFinder.find(filename);
    if(csvInput == null) {
      throw new DataTransformerException("Could not find appropriate CSVInput type for filename " + filename);
    }
    BindyCsvDataFormat bindyDF = new BindyCsvDataFormat(csvInput.getClass().getPackage().getName());
    Object unmarshal = flatten((List<HashMap<String, Object>>)bindyDF.unmarshal(exchange, exchange.getIn().getBody(InputStream.class)));
    exchange.getIn().setBody(unmarshal);
  }

  private Object flatten(List<HashMap<String, Object>> unflattenedList) {
    return unflattenedList.stream().map(hashMap -> new ArrayList<>(hashMap.values()).get(0)).collect(Collectors.toList());
  }
}
