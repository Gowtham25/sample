package com.cybg.treasury.migration.datatransformer.camel.processor;

import java.util.List;
import java.util.stream.Collectors;

import com.cybg.treasury.migration.murex.jaxb.model.MxML;
import com.cybg.treasury.migration.murex.jaxb.model.Trade;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author theo on 13/12/2016.
 */
@Component
public class TradeFilterProcessor implements Processor {

  public boolean filterInternalTrades;
  public static final String NABAU = "NABAU";
  public static final String CLYDESDALE = "CLYDESDALE";
  public static final String YORKSHIRE = "YORKSHIRE";

  @Autowired
  public TradeFilterProcessor(@Value("#{environment['filterInternalTrades']}") boolean filterInternalTrades) {
    this.filterInternalTrades = filterInternalTrades;
  }

  @Override
  public void process(Exchange exchange) throws Exception {
    MxML mxML = exchange.getIn().getBody(MxML.class);
    List<Trade> cybgTrades = mxML.getTrades()
                           .getTrade()
                           .stream()
                           .filter(trade -> trade.getTradeHeader().getTradeViews().getTradeView().get(0).getLegalEntity().contains(CLYDESDALE)
                           || trade.getTradeHeader().getTradeViews().getTradeView().get(0).getLegalEntity().contains(YORKSHIRE))
                           .collect(Collectors.toList());

    if (filterInternalTrades) {
      cybgTrades = cybgTrades
                     .stream()
                     .filter(trade -> trade.getParties().getParty().get(1).getPartyName().contains(NABAU))
                     .collect(Collectors.toList());
    }
    mxML.getTrades().getTrade().retainAll(cybgTrades);
  }
}
