//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7-b41 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.02.09 at 04:35:52 PM GMT 
//


package com.cybg.treasury.migration.murex.jaxb.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}settlementInstructionKey"/>
 *         &lt;element ref="{}settlementInstructionDescription"/>
 *         &lt;element ref="{}nostroInstructions"/>
 *         &lt;element ref="{}vostroInstructions"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "settlementInstructionKey",
    "settlementInstructionDescription",
    "nostroInstructions",
    "vostroInstructions"
})
@XmlRootElement(name = "settlementInstruction")
public class SettlementInstruction {

    @XmlElement(required = true)
    protected SettlementInstructionKey settlementInstructionKey;
    @XmlElement(required = true)
    protected SettlementInstructionDescription settlementInstructionDescription;
    @XmlElement(required = true)
    protected NostroInstructions nostroInstructions;
    @XmlElement(required = true)
    protected VostroInstructions vostroInstructions;

    /**
     * Gets the value of the settlementInstructionKey property.
     * 
     * @return
     *     possible object is
     *     {@link SettlementInstructionKey }
     *     
     */
    public SettlementInstructionKey getSettlementInstructionKey() {
        return settlementInstructionKey;
    }

    /**
     * Sets the value of the settlementInstructionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link SettlementInstructionKey }
     *     
     */
    public void setSettlementInstructionKey(SettlementInstructionKey value) {
        this.settlementInstructionKey = value;
    }

    /**
     * Gets the value of the settlementInstructionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link SettlementInstructionDescription }
     *     
     */
    public SettlementInstructionDescription getSettlementInstructionDescription() {
        return settlementInstructionDescription;
    }

    /**
     * Sets the value of the settlementInstructionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link SettlementInstructionDescription }
     *     
     */
    public void setSettlementInstructionDescription(SettlementInstructionDescription value) {
        this.settlementInstructionDescription = value;
    }

    /**
     * Gets the value of the nostroInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link NostroInstructions }
     *     
     */
    public NostroInstructions getNostroInstructions() {
        return nostroInstructions;
    }

    /**
     * Sets the value of the nostroInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link NostroInstructions }
     *     
     */
    public void setNostroInstructions(NostroInstructions value) {
        this.nostroInstructions = value;
    }

    /**
     * Gets the value of the vostroInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link VostroInstructions }
     *     
     */
    public VostroInstructions getVostroInstructions() {
        return vostroInstructions;
    }

    /**
     * Sets the value of the vostroInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link VostroInstructions }
     *     
     */
    public void setVostroInstructions(VostroInstructions value) {
        this.vostroInstructions = value;
    }

}
