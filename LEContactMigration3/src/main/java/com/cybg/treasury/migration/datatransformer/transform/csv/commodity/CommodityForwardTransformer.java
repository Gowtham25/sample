package com.cybg.treasury.migration.datatransformer.transform.csv.commodity;

import com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commodityforwardoutput.CommodityForwardOutput;
import com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commoditytradeinput.CommodityTradeInput;
import com.cybg.treasury.migration.datatransformer.transform.csv.CsvToCsvDataTransformer;
import org.springframework.stereotype.Component;

@Component
public class CommodityForwardTransformer implements CsvToCsvDataTransformer<CommodityTradeInput, CommodityForwardOutput> {
  @Override
  public CommodityForwardOutput transform(CommodityTradeInput csvInput) {
    CommodityForwardOutput commodityForwardOutput = new CommodityForwardOutput();
    commodityForwardOutput.setExternalReference(csvInput.getTradeNo());
    commodityForwardOutput.setCounterparty(csvInput.getCounterparty());
    return commodityForwardOutput;
  }

  @Override
  public String getSupportedType() {
    return "Commodity Forward";
  }
}
