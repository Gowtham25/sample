package com.cybg.treasury.migration.datatransformer.camel.route;

import java.util.concurrent.CountDownLatch;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LegalEntityRouteBuilder extends RouteBuilder implements MigrationRouteBuilder {
  private final String inputDirectory;
  private final String outputDirectory;
  private final Processor csv2CDUFProcessor;
  private final Processor bindyUnmarshalProcessor;
  private final Processor bindyMarshalProcessor;
  private final Processor errorHandler;
  private final CountDownLatch latch;

  @Autowired
  public LegalEntityRouteBuilder(@Value("#{environment['legalentity.input.dir']}") String inputDirectory,
                                  @Value("#{environment['migrationdata.output.dir']}") String outputDirectory,
                                  Processor csv2CDUFProcessor,
                                  Processor bindyMarshalProcessor,
                                  Processor bindyUnmarshalProcessor,
                                  Processor errorLoggingProcessor,
                                  CountDownLatch latch) {
    this.inputDirectory = inputDirectory;
    this.outputDirectory = outputDirectory;
    this.csv2CDUFProcessor = csv2CDUFProcessor;
    this.bindyUnmarshalProcessor = bindyUnmarshalProcessor;
    this.bindyMarshalProcessor = bindyMarshalProcessor;
    this.errorHandler = errorLoggingProcessor;
    this.latch = latch;
  }

  @Override
  @SuppressWarnings("unchecked")
  public void configure() throws Exception {
    String inputEndpoint = inputDirectory + "?move=.done&moveFailed=.error";

    onException(Exception.class)
      .logStackTrace(true)
      .process(errorHandler)
      .stop();

    from(inputEndpoint)
      .process(bindyUnmarshalProcessor)
      .process(csv2CDUFProcessor)
      .process(bindyMarshalProcessor)
      .to(outputDirectory)
      .process(exchange -> latch.countDown());
  }

  @Override
  public String getSupportedType() {
    return "LegalEntity";
  }
}
