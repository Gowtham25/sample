package com.cybg.treasury.migration.datatransformer.csvmodel.commodity;

import org.apache.camel.dataformat.bindy.annotation.DataField;

public class CommodityDateSchedule {
  @DataField(pos = 32, columnName = "DateSchedule.PayFrequency") private String payFrequency;
  @DataField(pos = 33, columnName = "DateSchedule.FixingPolicy") private String fixingPolicy;
  @DataField(pos = 34, columnName = "DateSchedule.StartDate") private String startDate;
  @DataField(pos = 35, columnName = "DateSchedule.EndDate") private String endDate;
  @DataField(pos = 36, columnName = "DateSchedule.Frequency") private String frequency;
  @DataField(pos = 37, columnName = "DateSchedule.DateRoll") private String dateRoll;

  public void setPayFrequency(String payFrequency) {
    this.payFrequency = payFrequency;
  }

  public void setFixingPolicy(String fixingPolicy) {
    this.fixingPolicy = fixingPolicy;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public void setFrequency(String frequency) {
    this.frequency = frequency;
  }

  public void setDateRoll(String dateRoll) {
    this.dateRoll = dateRoll;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    CommodityDateSchedule that = (CommodityDateSchedule)o;

    if(payFrequency != null ? !payFrequency.equals(that.payFrequency) : that.payFrequency != null) return false;
    if(fixingPolicy != null ? !fixingPolicy.equals(that.fixingPolicy) : that.fixingPolicy != null) return false;
    if(startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
    if(endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
    if(frequency != null ? !frequency.equals(that.frequency) : that.frequency != null) return false;
    return dateRoll != null ? dateRoll.equals(that.dateRoll) : that.dateRoll == null;

  }

  @Override
  public int hashCode() {
    int result = payFrequency != null ? payFrequency.hashCode() : 0;
    result = 31 * result + (fixingPolicy != null ? fixingPolicy.hashCode() : 0);
    result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
    result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
    result = 31 * result + (frequency != null ? frequency.hashCode() : 0);
    result = 31 * result + (dateRoll != null ? dateRoll.hashCode() : 0);
    return result;
  }
}
