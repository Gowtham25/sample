package com.cybg.treasury.migration.datatransformer.transform.csv.refdata;

import com.cybg.treasury.migration.datatransformer.csvmodel.refdata.legalentityinput.LegalEntityInput;
import com.cybg.treasury.migration.datatransformer.csvmodel.refdata.legalentityoutput.LegalEntityOutput;
import com.cybg.treasury.migration.datatransformer.transform.csv.CsvToCsvDataTransformer;
import org.springframework.stereotype.Component;

/**
 * @author theo on 05/01/2017.
 */
@Component
public class LegalEntityTransformer implements CsvToCsvDataTransformer<LegalEntityInput, LegalEntityOutput> {

  public static final String ENABLED = "Enabled";
  public static final String DISABLED = "Disabled";
  public static final String ACTIVE = "ACTIVE";

  @Override
  public LegalEntityOutput transform(LegalEntityInput leInput) {
    LegalEntityOutput leOutput = new LegalEntityOutput();
    leOutput.setLongName(leInput.getLongName());
    leOutput.setShortName(leInput.getShortName());
    leOutput.setStatus(getStatus(leInput));
    leOutput.setCountry(getCountry(leInput.getCountryCode()));
    leOutput.setFinancial(isFinancial(leInput));
    return leOutput;
  }

  private String getCountry(String countryCode) {
    return "UNITED KINGDOM";
    //TODO implement this somehow (Calypso lookup, hardcoded, mapping properties file )
  }

  private String isFinancial(LegalEntityInput leInput) {
    Boolean result = leInput.getIsFinancialForSettlement().equals("1") ? Boolean.TRUE : Boolean.FALSE;
    return result.toString();
  }

  private String getStatus(LegalEntityInput leInput) {
    String result = DISABLED;
    if (ACTIVE.equals(leInput.getStatusCode())) {
      result = ENABLED;
    }
    return result;
  }

  @Override
  public String getSupportedType() {
    return "LegalEntity";
  }
}
