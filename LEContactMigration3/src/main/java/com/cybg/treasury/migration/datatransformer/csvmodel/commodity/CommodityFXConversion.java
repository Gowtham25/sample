package com.cybg.treasury.migration.datatransformer.csvmodel.commodity;

import org.apache.camel.dataformat.bindy.annotation.DataField;

public class CommodityFXConversion {
  @DataField(pos = 42, columnName = "FXConversion.FXResetRateFixedB") private String fxResetRateFixedB;
  @DataField(pos = 43, columnName = "FXConversion.FXResetRate") private String fxResetRate;
  @DataField(pos = 44, columnName = "FXConversion.AverageMethod") private String averageMethod;
  @DataField(pos = 45, columnName = "FXConversion.RoundingAfter") private String roundingAfter;
  @DataField(pos = 46, columnName = "FXConversion.HolidayCode") private String holidayCode;
  @DataField(pos = 47, columnName = "FXConversion.FXEndLagB") private String fxEndLagB;
  @DataField(pos = 48, columnName = "FXConversion.CustomRoundingB") private String customRoundingB;
  @DataField(pos = 49, columnName = "FXConversion.CustomRoundingValue") private String customRoundingValue;

  public void setFxResetRate(String fxResetRate) {
    this.fxResetRate = fxResetRate;
  }

  public void setAverageMethod(String averageMethod) {
    this.averageMethod = averageMethod;
  }

  public void setRoundingAfter(String roundingAfter) {
    this.roundingAfter = roundingAfter;
  }

  public void setFxResetRateFixedB(String fxResetRateFixedB) {
    this.fxResetRateFixedB = fxResetRateFixedB;
  }

  public void setHolidayCode(String holidayCode) {
    this.holidayCode = holidayCode;
  }

  public void setFxEndLagB(String fxEndLagB) {
    this.fxEndLagB = fxEndLagB;
  }

  public void setCustomRoundingB(String customRoundingB) {
    this.customRoundingB = customRoundingB;
  }

  public void setCustomRoundingValue(String customRoundingValue) {
    this.customRoundingValue = customRoundingValue;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    CommodityFXConversion that = (CommodityFXConversion)o;

    if(fxResetRateFixedB != null ? !fxResetRateFixedB.equals(that.fxResetRateFixedB) : that.fxResetRateFixedB != null)
      return false;
    if(fxResetRate != null ? !fxResetRate.equals(that.fxResetRate) : that.fxResetRate != null) return false;
    if(averageMethod != null ? !averageMethod.equals(that.averageMethod) : that.averageMethod != null) return false;
    if(roundingAfter != null ? !roundingAfter.equals(that.roundingAfter) : that.roundingAfter != null) return false;
    if(holidayCode != null ? !holidayCode.equals(that.holidayCode) : that.holidayCode != null) return false;
    if(fxEndLagB != null ? !fxEndLagB.equals(that.fxEndLagB) : that.fxEndLagB != null) return false;
    if(customRoundingB != null ? !customRoundingB.equals(that.customRoundingB) : that.customRoundingB != null)
      return false;
    return customRoundingValue != null ? customRoundingValue.equals(that.customRoundingValue) : that.customRoundingValue == null;

  }

  @Override
  public int hashCode() {
    int result = fxResetRateFixedB != null ? fxResetRateFixedB.hashCode() : 0;
    result = 31 * result + (fxResetRate != null ? fxResetRate.hashCode() : 0);
    result = 31 * result + (averageMethod != null ? averageMethod.hashCode() : 0);
    result = 31 * result + (roundingAfter != null ? roundingAfter.hashCode() : 0);
    result = 31 * result + (holidayCode != null ? holidayCode.hashCode() : 0);
    result = 31 * result + (fxEndLagB != null ? fxEndLagB.hashCode() : 0);
    result = 31 * result + (customRoundingB != null ? customRoundingB.hashCode() : 0);
    result = 31 * result + (customRoundingValue != null ? customRoundingValue.hashCode() : 0);
    return result;
  }
}
