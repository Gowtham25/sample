package com.cybg.treasury.migration.datatransformer.transform.csv.commodity;

import com.cybg.treasury.migration.datatransformer.csvmodel.commodity.*;
import com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commodityforwardaverageoutput.CommodityForwardAverageOutput;
import com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commoditytradeinput.CommodityTradeInput;
import com.cybg.treasury.migration.datatransformer.transform.DateFormatConvertor;
import com.cybg.treasury.migration.datatransformer.transform.csv.CsvToCsvDataTransformer;
import org.springframework.stereotype.Component;

@Component
public class CommodityForwardAverageTransformer implements CsvToCsvDataTransformer<CommodityTradeInput, CommodityForwardAverageOutput> {
  private final String commodityFileDateInputFormat = "dd-MMM-yyyy";
  private final String commodityCDUFDateInputFormat = "yyyyMMdd";

  @Override
  public CommodityForwardAverageOutput transform(CommodityTradeInput csvInput) {
    CommodityForwardAverageOutput commodityForwardAverageOutput = new CommodityForwardAverageOutput();
    commodityForwardAverageOutput.setExternalReference(csvInput.getTradeNo());
    commodityForwardAverageOutput.setCounterparty(csvInput.getCounterparty()); // TODO need to map to Calypso Counterparty
    commodityForwardAverageOutput.setBook(csvInput.getPortfolio()); // TODO need to map to Calypso book
    String currency = csvInput.getSettlementAsset();
    commodityForwardAverageOutput.setSettlementCurrency(currency);
    commodityForwardAverageOutput.setTradeCurrency(currency);

    DateFormatConvertor dateFormatConvertor = new DateFormatConvertor();
    String tradeDate = dateFormatConvertor.convertDateFormat(
      csvInput.getTradeDate(), commodityFileDateInputFormat, commodityCDUFDateInputFormat
    );
    commodityForwardAverageOutput.setTradeDate(tradeDate);
    commodityForwardAverageOutput.setSettleDate(
      dateFormatConvertor.convertDateFormat(
        csvInput.getSettlementDate(), commodityFileDateInputFormat, commodityCDUFDateInputFormat
      )
    );
    commodityForwardAverageOutput.setStartDate(tradeDate);

    commodityForwardAverageOutput.setTraderName(csvInput.getTRADER());
    commodityForwardAverageOutput.setSalesPerson("NONE");
    commodityForwardAverageOutput.setComment(csvInput.getCOMMENT());
    commodityForwardAverageOutput.setProductType("CommoditySwap2");
    commodityForwardAverageOutput.setProductSubType("Standard");
    commodityForwardAverageOutput.setSwapType(csvInput.getBuySell().equals("B") ? "Buy (Pay Fixed)" : "Sell (Rec Fixed)");
    commodityForwardAverageOutput.setPaymentCurrency(currency);

    commodityForwardAverageOutput.setFloatingLeg(getFloatingLegDetails(csvInput));
    commodityForwardAverageOutput.setFixedLeg(getFixedLegDetails(csvInput));
    commodityForwardAverageOutput.setDateSchedule(getDateScheduleDetails(csvInput));
    commodityForwardAverageOutput.setQuantitySchedule(getQuantityScheduleDetails(csvInput));
    commodityForwardAverageOutput.setCommodityFXConversion(getFXConversionDetails(csvInput));
    commodityForwardAverageOutput.setCommodityPaymentSchedule(getPaymentScheduleDetails(csvInput));
    commodityForwardAverageOutput.setPortfolioPath(csvInput.getPortfolioPath());
    return commodityForwardAverageOutput;
  }

  private CommodityPaymentSchedule getPaymentScheduleDetails(CommodityTradeInput csvInput) {
    CommodityPaymentSchedule commodityPaymentSchedule = new CommodityPaymentSchedule();
    commodityPaymentSchedule.setDateRoll("FOLLOWING"); // TODO check date roll on payment schedule
    return commodityPaymentSchedule;
  }

  private CommodityFXConversion getFXConversionDetails(CommodityTradeInput csvInput) {
    CommodityFXConversion commodityFXConversion = new CommodityFXConversion();
    commodityFXConversion.setFxResetRate(
      !csvInput.getPEG_CURRENCY().equals(csvInput.getSettlementAsset())
        ? "TRUE"
        : "FALSE"
    );
    commodityFXConversion.setAverageMethod("CTA");
    commodityFXConversion.setRoundingAfter("Average");
    return commodityFXConversion;
  }

  private CommodityQuantitySchedule getQuantityScheduleDetails(CommodityTradeInput csvInput) {
    CommodityQuantitySchedule commodityQuantitySchedule = new CommodityQuantitySchedule();
    commodityQuantitySchedule.setQuantity(csvInput.getDealtQuantity().replaceAll(",", ""));
    commodityQuantitySchedule.setQuoteUnit(csvInput.getDealtMeasure()); // TODO map to valid Calypso quote unit
    commodityQuantitySchedule.setQuantityPerPeriod("MTH"); // TODO check logic for this
    commodityQuantitySchedule.setQuantityAfterConversion("Dont Round");
    return commodityQuantitySchedule;
  }

  private CommodityDateSchedule getDateScheduleDetails(CommodityTradeInput csvInput) {
    CommodityDateSchedule commodityDateSchedule = new CommodityDateSchedule();
    commodityDateSchedule.setPayFrequency("Periodic");
    commodityDateSchedule.setFixingPolicy("Whole Period");

    DateFormatConvertor dateFormatConvertor = new DateFormatConvertor();
    commodityDateSchedule.setStartDate(
      dateFormatConvertor.convertDateFormat(
        csvInput.getAVERAGE_START_DATE(), commodityFileDateInputFormat, commodityCDUFDateInputFormat
      )
    );
    commodityDateSchedule.setEndDate(
      dateFormatConvertor.convertDateFormat(
        csvInput.getAVEREGE_END_DATE(), commodityFileDateInputFormat, commodityCDUFDateInputFormat
      )
    );
    commodityDateSchedule.setFrequency("MTH"); // TODO map to valid calypso averaging frequency
    return commodityDateSchedule;
  }

  private CommoditySwapFixedLeg getFixedLegDetails(CommodityTradeInput csvInput) {
    CommoditySwapFixedLeg commoditySwapFixedLeg = new CommoditySwapFixedLeg();
    commoditySwapFixedLeg.setSwapDirection("Fixed");
    commoditySwapFixedLeg.setCommodityType("Reset");
    commoditySwapFixedLeg.setCommodityResetName(csvInput.getAsset()); // TODO map to Calypso Commodity Reset
    commoditySwapFixedLeg.setStrike(csvInput.getSTRIKE_RATE().replaceAll(",", ""));
    commoditySwapFixedLeg.setBuySellUnits(csvInput.getDealtMeasure()); // TODO map to valid Calypso commodity unit
    return commoditySwapFixedLeg;
  }

  private CommoditySwapFloatingLeg getFloatingLegDetails(CommodityTradeInput csvInput) {
    CommoditySwapFloatingLeg commoditySwapFloatingLeg = new CommoditySwapFloatingLeg();
    commoditySwapFloatingLeg.setSwapDirection("Float");
    commoditySwapFloatingLeg.setCommodityType("Reset");
    commoditySwapFloatingLeg.setCommodityResetName(csvInput.getAsset()); // TODO map to Calypso commodity resets
    return commoditySwapFloatingLeg;
  }

  @Override
  public String getSupportedType() {
    return "Commodity Forward Average";
  }
}
