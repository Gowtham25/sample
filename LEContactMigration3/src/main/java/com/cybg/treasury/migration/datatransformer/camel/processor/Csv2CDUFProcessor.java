package com.cybg.treasury.migration.datatransformer.camel.processor;

import java.util.ArrayList;
import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVInput;
import com.cybg.treasury.migration.datatransformer.csvmodel.CSVOutput;
import com.cybg.treasury.migration.datatransformer.exception.DataTransformerException;
import com.cybg.treasury.migration.datatransformer.transform.csv.CsvToCsvDataTransformer;
import com.cybg.treasury.migration.datatransformer.transform.csv.refdata.RefdataTransformerFinder;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Csv2CDUFProcessor implements Processor {

  private final RefdataTransformerFinder refdataTransformerFinder;

  @Autowired
  public Csv2CDUFProcessor(RefdataTransformerFinder refdataTransformerFinder) {
    this.refdataTransformerFinder = refdataTransformerFinder;
  }

  @Override
  @SuppressWarnings("unchecked")
  public void process(Exchange exchange) throws Exception {
    List<CSVInput> csvInputs = exchange.getIn().getBody(List.class);
    List<CSVOutput> csvOutputs = new ArrayList<>();
    if(!csvInputs.isEmpty()) {
      String transformerType = csvInputs.get(0).getObjectType();
      CsvToCsvDataTransformer refdataTransformer = refdataTransformerFinder.find(transformerType);
      if(refdataTransformer == null) {
        throw new DataTransformerException("Could not find appropriate CsvToCsvDataTransformer for type " + transformerType);
      }
      csvInputs.forEach(csvInput -> csvOutputs.add(refdataTransformer.transform(csvInput)));
    }
    exchange.getIn().setBody(csvOutputs);
  }
}
