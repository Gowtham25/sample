package com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commoditytradeinput;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVInput;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.springframework.stereotype.Component;

// TODO complete implementation
@CsvRecord(separator = ",", skipFirstLine = true)
@Component
public class CommodityTradeInput implements CSVInput {
  @DataField(pos = 1, name = "Trade No") private String tradeNo;
  @DataField(pos = 2, name = "Counterparty") private String counterparty;
  @DataField(pos = 3, name = "Portfolio Path") private String portfolioPath;
  @DataField(pos = 4, name = "Status") private String status;
  @DataField(pos = 5, name = "Previous Status") private String prevStatus;
  @DataField(pos = 6, name = "Status Update Date") private String statusUpdateDate;
  @DataField(pos = 7, name = "Asset") private String asset;
  @DataField(pos = 8, name = "Associated Asset") private String associatedAsset;
  @DataField(pos = 9, name = "Product") private String product;
  @DataField(pos = 10, name = "Product Type") private String productType;
  @DataField(pos = 11, name = "Package") private String packageType;
  @DataField(pos = 12, name = "Parent Product Type") private String parentProductType;
  @DataField(pos = 13, name = "OrderType") private String ORDER_TYPE;
  @DataField(pos = 14, name = "B/S") private String buySell;
  @DataField(pos = 15, name = "C/P") private String callPut;
  @DataField(pos = 16, name = "Trade Date") private String tradeDate;
  @DataField(pos = 17, name = "Expiry/Fixing Date") private String expiryOrFixingDate;
  @DataField(pos = 18) private String FX_FIXING_DATE;
  @DataField(pos = 19) private String LAST_ALLOWABLE_DATE;
  @DataField(pos = 20, name = "Settlement Date") private String settlementDate;
  @DataField(pos = 21, name = "Value Asset") private String valueAsset;
  @DataField(pos = 22, name = "Settlement Asset") private String settlementAsset;
  @DataField(pos = 23, name = "Dealt Quantity") private String dealtQuantity;
  @DataField(pos = 24, name = "Dealt Measure") private String dealtMeasure;
  @DataField(pos = 25, name = "Peg Quantity") private String pegQuantity;
  @DataField(pos = 26) private String PEG_MEASURE;
  @DataField(pos = 27) private String REFERENCE_QUOTE_UNIT;
  @DataField(pos = 28) private String DEALT_QUOTE_UNIT;
  @DataField(pos = 29) private String CONVERSION;
  @DataField(pos = 30) private String STRIKE_RATE;
  @DataField(pos = 31) private String DEALT_VALUE;
  @DataField(pos = 32) private String DEALT_PREMIUM_VALUE;
  @DataField(pos = 33) private String PEG_VALUE;
  @DataField(pos = 34) private String PEG_RATE;
  @DataField(pos = 35) private String PEG_CURRENCY;
  @DataField(pos = 36) private String REFERENCE;
  @DataField(pos = 37) private String CLASSIFICATION;
  @DataField(pos = 38) private String CONTRACT_CODE;
  @DataField(pos = 39) private String EXPIRY_FIXING_TIME;
  @DataField(pos = 40) private String COM_FIXING_PRICE;
  @DataField(pos = 41) private String FX_REFERENCE;
  @DataField(pos = 42) private String FX_FIXING;
  @DataField(pos = 43) private String FX_FIXING_PRICE;
  @DataField(pos = 44) private String DEAL_FIXING_PRICE;
  @DataField(pos = 45) private String PAY_RECEIVE;
  @DataField(pos = 46) private String PAYMENT;
  @DataField(pos = 47) private String PAYMENT_SIGNED;
  @DataField(pos = 48) private String FIXED_POSITION;
  @DataField(pos = 49) private String FIXED_CURRENCY;
  @DataField(pos = 50) private String FIXED_FACE_VALUE;
  @DataField(pos = 51) private String PRICING_METHOD;
  @DataField(pos = 52) private String EXTRAPOLATE;
  @DataField(pos = 53) private String QUOTE_CONVENTION;
  @DataField(pos = 54) private String EXERCISE_STYLE;
  @DataField(pos = 55) private String APRA_ID;
  @DataField(pos = 56) private String INFINITY_ORG_ID;
  @DataField(pos = 57) private String TRADER;
  @DataField(pos = 58) private String DISTRIBUTOR;
  @DataField(pos = 59) private String BROKER;
  @DataField(pos = 60) private String MARKET_SEGMENT;
  @DataField(pos = 61) private String BUID;
  @DataField(pos = 62) private String TAG;
  @DataField(pos = 63) private String COMMENT;
  @DataField(pos = 64) private String VALUE_ADD;
  @DataField(pos = 65) private String VALUE_ADD_PER_QUOTE_UNIT;
  @DataField(pos = 66) private String CEDAR_ID;
  @DataField(pos = 67) private String INDEPENDENT_AMOUNT;
  @DataField(pos = 68) private String NEGOTIATED_OPTION_CP;
  @DataField(pos = 69) private String MARGIN;
  @DataField(pos = 70) private String PROTEIN;
  @DataField(pos = 71) private String SCREENINGS;
  @DataField(pos = 72) private String MOISTURE;
  @DataField(pos = 73) private String AVERAGE_TYPE;
  @DataField(pos = 74) private String AVERAGE_START_DATE;
  @DataField(pos = 75) private String AVEREGE_END_DATE;
  @DataField(pos = 76) private String SPLIT_ASSET;
  @DataField(pos = 77) private String LEG_ID;
  @DataField(pos = 78) private String SPREAD;
  @DataField(pos = 79) private String CLOSE_OUT_RATE;
  @DataField(pos = 80) private String CLOSE_OUT_QUNATITY;
  @DataField(pos = 81) private String PARENT_TRADE;
  @DataField(pos = 82) private String SPLIT_DATE;

  public String getTradeNo() {
    return tradeNo;
  }

  public void setTradeNo(String tradeNo) {
    this.tradeNo = tradeNo;
  }

  public String getCounterparty() {
    return counterparty;
  }

  public void setCounterparty(String counterparty) {
    this.counterparty = counterparty;
  }

  public String getPortfolioPath() {
    return portfolioPath;
  }

  public void setPortfolioPath(String portfolioPath) {
    this.portfolioPath = portfolioPath;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getPrevStatus() {
    return prevStatus;
  }

  public void setPrevStatus(String prevStatus) {
    this.prevStatus = prevStatus;
  }

  public String getStatusUpdateDate() {
    return statusUpdateDate;
  }

  public void setStatusUpdateDate(String statusUpdateDate) {
    this.statusUpdateDate = statusUpdateDate;
  }

  public String getAsset() {
    return asset;
  }

  public void setAsset(String asset) {
    this.asset = asset;
  }

  public String getAssociatedAsset() {
    return associatedAsset;
  }

  public void setAssociatedAsset(String associatedAsset) {
    this.associatedAsset = associatedAsset;
  }

  public String getProduct() {
    return product;
  }

  public void setProduct(String product) {
    this.product = product;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public String getPackageType() {
    return packageType;
  }

  public void setPackageType(String packageType) {
    this.packageType = packageType;
  }

  public String getParentProductType() {
    return parentProductType;
  }

  public void setParentProductType(String parentProductType) {
    this.parentProductType = parentProductType;
  }

  public String getORDER_TYPE() {
    return ORDER_TYPE;
  }

  public void setORDER_TYPE(String ORDER_TYPE) {
    this.ORDER_TYPE = ORDER_TYPE;
  }

  public String getBuySell() {
    return buySell;
  }

  public void setBuySell(String buySell) {
    this.buySell = buySell;
  }

  public String getCallPut() {
    return callPut;
  }

  public void setCallPut(String callPut) {
    this.callPut = callPut;
  }

  public String getTradeDate() {
    return tradeDate;
  }

  public void setTradeDate(String tradeDate) {
    this.tradeDate = tradeDate;
  }

  public String getExpiryOrFixingDate() {
    return expiryOrFixingDate;
  }

  public void setExpiryOrFixingDate(String expiryOrFixingDate) {
    this.expiryOrFixingDate = expiryOrFixingDate;
  }

  public String getFX_FIXING_DATE() {
    return FX_FIXING_DATE;
  }

  public void setFX_FIXING_DATE(String FX_FIXING_DATE) {
    this.FX_FIXING_DATE = FX_FIXING_DATE;
  }

  public String getLAST_ALLOWABLE_DATE() {
    return LAST_ALLOWABLE_DATE;
  }

  public void setLAST_ALLOWABLE_DATE(String LAST_ALLOWABLE_DATE) {
    this.LAST_ALLOWABLE_DATE = LAST_ALLOWABLE_DATE;
  }

  public String getSettlementDate() {
    return settlementDate;
  }

  public void setSettlementDate(String settlementDate) {
    this.settlementDate = settlementDate;
  }

  public String getValueAsset() {
    return valueAsset;
  }

  public void setValueAsset(String valueAsset) {
    this.valueAsset = valueAsset;
  }

  public String getSettlementAsset() {
    return settlementAsset;
  }

  public void setSettlementAsset(String settlementAsset) {
    this.settlementAsset = settlementAsset;
  }

  public String getDealtQuantity() {
    return dealtQuantity;
  }

  public void setDealtQuantity(String dealtQuantity) {
    this.dealtQuantity = dealtQuantity;
  }

  public String getDealtMeasure() {
    return dealtMeasure;
  }

  public void setDealtMeasure(String dealtMeasure) {
    this.dealtMeasure = dealtMeasure;
  }

  public String getPegQuantity() {
    return pegQuantity;
  }

  public void setPegQuantity(String pegQuantity) {
    this.pegQuantity = pegQuantity;
  }

  public String getPEG_MEASURE() {
    return PEG_MEASURE;
  }

  public void setPEG_MEASURE(String PEG_MEASURE) {
    this.PEG_MEASURE = PEG_MEASURE;
  }

  public String getREFERENCE_QUOTE_UNIT() {
    return REFERENCE_QUOTE_UNIT;
  }

  public void setREFERENCE_QUOTE_UNIT(String REFERENCE_QUOTE_UNIT) {
    this.REFERENCE_QUOTE_UNIT = REFERENCE_QUOTE_UNIT;
  }

  public String getDEALT_QUOTE_UNIT() {
    return DEALT_QUOTE_UNIT;
  }

  public void setDEALT_QUOTE_UNIT(String DEALT_QUOTE_UNIT) {
    this.DEALT_QUOTE_UNIT = DEALT_QUOTE_UNIT;
  }

  public String getCONVERSION() {
    return CONVERSION;
  }

  public void setCONVERSION(String CONVERSION) {
    this.CONVERSION = CONVERSION;
  }

  public String getSTRIKE_RATE() {
    return STRIKE_RATE;
  }

  public void setSTRIKE_RATE(String STRIKE_RATE) {
    this.STRIKE_RATE = STRIKE_RATE;
  }

  public String getDEALT_VALUE() {
    return DEALT_VALUE;
  }

  public void setDEALT_VALUE(String DEALT_VALUE) {
    this.DEALT_VALUE = DEALT_VALUE;
  }

  public String getDEALT_PREMIUM_VALUE() {
    return DEALT_PREMIUM_VALUE;
  }

  public void setDEALT_PREMIUM_VALUE(String DEALT_PREMIUM_VALUE) {
    this.DEALT_PREMIUM_VALUE = DEALT_PREMIUM_VALUE;
  }

  public String getPEG_VALUE() {
    return PEG_VALUE;
  }

  public void setPEG_VALUE(String PEG_VALUE) {
    this.PEG_VALUE = PEG_VALUE;
  }

  public String getPEG_RATE() {
    return PEG_RATE;
  }

  public void setPEG_RATE(String PEG_RATE) {
    this.PEG_RATE = PEG_RATE;
  }

  public String getPEG_CURRENCY() {
    return PEG_CURRENCY;
  }

  public void setPEG_CURRENCY(String PEG_CURRENCY) {
    this.PEG_CURRENCY = PEG_CURRENCY;
  }

  public String getREFERENCE() {
    return REFERENCE;
  }

  public void setREFERENCE(String REFERENCE) {
    this.REFERENCE = REFERENCE;
  }

  public String getCLASSIFICATION() {
    return CLASSIFICATION;
  }

  public void setCLASSIFICATION(String CLASSIFICATION) {
    this.CLASSIFICATION = CLASSIFICATION;
  }

  public String getCONTRACT_CODE() {
    return CONTRACT_CODE;
  }

  public void setCONTRACT_CODE(String CONTRACT_CODE) {
    this.CONTRACT_CODE = CONTRACT_CODE;
  }

  public String getEXPIRY_FIXING_TIME() {
    return EXPIRY_FIXING_TIME;
  }

  public void setEXPIRY_FIXING_TIME(String EXPIRY_FIXING_TIME) {
    this.EXPIRY_FIXING_TIME = EXPIRY_FIXING_TIME;
  }

  public String getCOM_FIXING_PRICE() {
    return COM_FIXING_PRICE;
  }

  public void setCOM_FIXING_PRICE(String COM_FIXING_PRICE) {
    this.COM_FIXING_PRICE = COM_FIXING_PRICE;
  }

  public String getFX_REFERENCE() {
    return FX_REFERENCE;
  }

  public void setFX_REFERENCE(String FX_REFERENCE) {
    this.FX_REFERENCE = FX_REFERENCE;
  }

  public String getFX_FIXING() {
    return FX_FIXING;
  }

  public void setFX_FIXING(String FX_FIXING) {
    this.FX_FIXING = FX_FIXING;
  }

  public String getFX_FIXING_PRICE() {
    return FX_FIXING_PRICE;
  }

  public void setFX_FIXING_PRICE(String FX_FIXING_PRICE) {
    this.FX_FIXING_PRICE = FX_FIXING_PRICE;
  }

  public String getDEAL_FIXING_PRICE() {
    return DEAL_FIXING_PRICE;
  }

  public void setDEAL_FIXING_PRICE(String DEAL_FIXING_PRICE) {
    this.DEAL_FIXING_PRICE = DEAL_FIXING_PRICE;
  }

  public String getPAY_RECEIVE() {
    return PAY_RECEIVE;
  }

  public void setPAY_RECEIVE(String PAY_RECEIVE) {
    this.PAY_RECEIVE = PAY_RECEIVE;
  }

  public String getPAYMENT() {
    return PAYMENT;
  }

  public void setPAYMENT(String PAYMENT) {
    this.PAYMENT = PAYMENT;
  }

  public String getPAYMENT_SIGNED() {
    return PAYMENT_SIGNED;
  }

  public void setPAYMENT_SIGNED(String PAYMENT_SIGNED) {
    this.PAYMENT_SIGNED = PAYMENT_SIGNED;
  }

  public String getFIXED_POSITION() {
    return FIXED_POSITION;
  }

  public void setFIXED_POSITION(String FIXED_POSITION) {
    this.FIXED_POSITION = FIXED_POSITION;
  }

  public String getFIXED_CURRENCY() {
    return FIXED_CURRENCY;
  }

  public void setFIXED_CURRENCY(String FIXED_CURRENCY) {
    this.FIXED_CURRENCY = FIXED_CURRENCY;
  }

  public String getFIXED_FACE_VALUE() {
    return FIXED_FACE_VALUE;
  }

  public void setFIXED_FACE_VALUE(String FIXED_FACE_VALUE) {
    this.FIXED_FACE_VALUE = FIXED_FACE_VALUE;
  }

  public String getPRICING_METHOD() {
    return PRICING_METHOD;
  }

  public void setPRICING_METHOD(String PRICING_METHOD) {
    this.PRICING_METHOD = PRICING_METHOD;
  }

  public String getEXTRAPOLATE() {
    return EXTRAPOLATE;
  }

  public void setEXTRAPOLATE(String EXTRAPOLATE) {
    this.EXTRAPOLATE = EXTRAPOLATE;
  }

  public String getQUOTE_CONVENTION() {
    return QUOTE_CONVENTION;
  }

  public void setQUOTE_CONVENTION(String QUOTE_CONVENTION) {
    this.QUOTE_CONVENTION = QUOTE_CONVENTION;
  }

  public String getEXERCISE_STYLE() {
    return EXERCISE_STYLE;
  }

  public void setEXERCISE_STYLE(String EXERCISE_STYLE) {
    this.EXERCISE_STYLE = EXERCISE_STYLE;
  }

  public String getAPRA_ID() {
    return APRA_ID;
  }

  public void setAPRA_ID(String APRA_ID) {
    this.APRA_ID = APRA_ID;
  }

  public String getINFINITY_ORG_ID() {
    return INFINITY_ORG_ID;
  }

  public void setINFINITY_ORG_ID(String INFINITY_ORG_ID) {
    this.INFINITY_ORG_ID = INFINITY_ORG_ID;
  }

  public String getTRADER() {
    return TRADER;
  }

  public void setTRADER(String TRADER) {
    this.TRADER = TRADER;
  }

  public String getDISTRIBUTOR() {
    return DISTRIBUTOR;
  }

  public void setDISTRIBUTOR(String DISTRIBUTOR) {
    this.DISTRIBUTOR = DISTRIBUTOR;
  }

  public String getBROKER() {
    return BROKER;
  }

  public void setBROKER(String BROKER) {
    this.BROKER = BROKER;
  }

  public String getMARKET_SEGMENT() {
    return MARKET_SEGMENT;
  }

  public void setMARKET_SEGMENT(String MARKET_SEGMENT) {
    this.MARKET_SEGMENT = MARKET_SEGMENT;
  }

  public String getBUID() {
    return BUID;
  }

  public void setBUID(String BUID) {
    this.BUID = BUID;
  }

  public String getTAG() {
    return TAG;
  }

  public void setTAG(String TAG) {
    this.TAG = TAG;
  }

  public String getCOMMENT() {
    return COMMENT;
  }

  public void setCOMMENT(String COMMENT) {
    this.COMMENT = COMMENT;
  }

  public String getVALUE_ADD() {
    return VALUE_ADD;
  }

  public void setVALUE_ADD(String VALUE_ADD) {
    this.VALUE_ADD = VALUE_ADD;
  }

  public String getVALUE_ADD_PER_QUOTE_UNIT() {
    return VALUE_ADD_PER_QUOTE_UNIT;
  }

  public void setVALUE_ADD_PER_QUOTE_UNIT(String VALUE_ADD_PER_QUOTE_UNIT) {
    this.VALUE_ADD_PER_QUOTE_UNIT = VALUE_ADD_PER_QUOTE_UNIT;
  }

  public String getCEDAR_ID() {
    return CEDAR_ID;
  }

  public void setCEDAR_ID(String CEDAR_ID) {
    this.CEDAR_ID = CEDAR_ID;
  }

  public String getINDEPENDENT_AMOUNT() {
    return INDEPENDENT_AMOUNT;
  }

  public void setINDEPENDENT_AMOUNT(String INDEPENDENT_AMOUNT) {
    this.INDEPENDENT_AMOUNT = INDEPENDENT_AMOUNT;
  }

  public String getNEGOTIATED_OPTION_CP() {
    return NEGOTIATED_OPTION_CP;
  }

  public void setNEGOTIATED_OPTION_CP(String NEGOTIATED_OPTION_CP) {
    this.NEGOTIATED_OPTION_CP = NEGOTIATED_OPTION_CP;
  }

  public String getMARGIN() {
    return MARGIN;
  }

  public void setMARGIN(String MARGIN) {
    this.MARGIN = MARGIN;
  }

  public String getPROTEIN() {
    return PROTEIN;
  }

  public void setPROTEIN(String PROTEIN) {
    this.PROTEIN = PROTEIN;
  }

  public String getSCREENINGS() {
    return SCREENINGS;
  }

  public void setSCREENINGS(String SCREENINGS) {
    this.SCREENINGS = SCREENINGS;
  }

  public String getMOISTURE() {
    return MOISTURE;
  }

  public void setMOISTURE(String MOISTURE) {
    this.MOISTURE = MOISTURE;
  }

  public String getAVERAGE_TYPE() {
    return AVERAGE_TYPE;
  }

  public void setAVERAGE_TYPE(String AVERAGE_TYPE) {
    this.AVERAGE_TYPE = AVERAGE_TYPE;
  }

  public String getAVERAGE_START_DATE() {
    return AVERAGE_START_DATE;
  }

  public void setAVERAGE_START_DATE(String AVERAGE_START_DATE) {
    this.AVERAGE_START_DATE = AVERAGE_START_DATE;
  }

  public String getAVEREGE_END_DATE() {
    return AVEREGE_END_DATE;
  }

  public void setAVEREGE_END_DATE(String AVEREGE_END_DATE) {
    this.AVEREGE_END_DATE = AVEREGE_END_DATE;
  }

  public String getSPLIT_ASSET() {
    return SPLIT_ASSET;
  }

  public void setSPLIT_ASSET(String SPLIT_ASSET) {
    this.SPLIT_ASSET = SPLIT_ASSET;
  }

  public String getLEG_ID() {
    return LEG_ID;
  }

  public void setLEG_ID(String LEG_ID) {
    this.LEG_ID = LEG_ID;
  }

  public String getSPREAD() {
    return SPREAD;
  }

  public void setSPREAD(String SPREAD) {
    this.SPREAD = SPREAD;
  }

  public String getCLOSE_OUT_RATE() {
    return CLOSE_OUT_RATE;
  }

  public void setCLOSE_OUT_RATE(String CLOSE_OUT_RATE) {
    this.CLOSE_OUT_RATE = CLOSE_OUT_RATE;
  }

  public String getCLOSE_OUT_QUNATITY() {
    return CLOSE_OUT_QUNATITY;
  }

  public void setCLOSE_OUT_QUNATITY(String CLOSE_OUT_QUNATITY) {
    this.CLOSE_OUT_QUNATITY = CLOSE_OUT_QUNATITY;
  }

  public String getPARENT_TRADE() {
    return PARENT_TRADE;
  }

  public void setPARENT_TRADE(String PARENT_TRADE) {
    this.PARENT_TRADE = PARENT_TRADE;
  }

  public String getSPLIT_DATE() {
    return SPLIT_DATE;
  }

  public void setSPLIT_DATE(String SPLIT_DATE) {
    this.SPLIT_DATE = SPLIT_DATE;
  }

  @Override
  public String getFileName() {
    return "Commodity.csv";
  }

  @Override
  public String getObjectType() {
    return productType;
  }

  private boolean hasValidProductType() {
    return productType != null;
  }

  public boolean isCommodityForwardAverageTrade() {
    return hasValidProductType() && productType.equals("Commodity Forward Average");
  }

  public boolean isCommodityForwardTrade() {
    return hasValidProductType() && productType.equals("Commodity Forward");
  }

  public String getPortfolio() {
    return portfolioPath.startsWith("\\NAG\\Clydesdale") ? "Clydesdale" : "Yorkshire";
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    CommodityTradeInput that = (CommodityTradeInput)o;

    if(tradeNo != null ? !tradeNo.equals(that.tradeNo) : that.tradeNo != null) return false;
    if(counterparty != null ? !counterparty.equals(that.counterparty) : that.counterparty != null) return false;
    if(portfolioPath != null ? !portfolioPath.equals(that.portfolioPath) : that.portfolioPath != null) return false;
    if(status != null ? !status.equals(that.status) : that.status != null) return false;
    if(prevStatus != null ? !prevStatus.equals(that.prevStatus) : that.prevStatus != null) return false;
    if(statusUpdateDate != null ? !statusUpdateDate.equals(that.statusUpdateDate) : that.statusUpdateDate != null)
      return false;
    if(asset != null ? !asset.equals(that.asset) : that.asset != null) return false;
    if(associatedAsset != null ? !associatedAsset.equals(that.associatedAsset) : that.associatedAsset != null)
      return false;
    if(product != null ? !product.equals(that.product) : that.product != null) return false;
    if(productType != null ? !productType.equals(that.productType) : that.productType != null) return false;
    if(packageType != null ? !packageType.equals(that.packageType) : that.packageType != null) return false;
    if(parentProductType != null ? !parentProductType.equals(that.parentProductType) : that.parentProductType != null)
      return false;
    if(ORDER_TYPE != null ? !ORDER_TYPE.equals(that.ORDER_TYPE) : that.ORDER_TYPE != null) return false;
    if(buySell != null ? !buySell.equals(that.buySell) : that.buySell != null) return false;
    if(callPut != null ? !callPut.equals(that.callPut) : that.callPut != null) return false;
    if(tradeDate != null ? !tradeDate.equals(that.tradeDate) : that.tradeDate != null) return false;
    if(expiryOrFixingDate != null ? !expiryOrFixingDate.equals(that.expiryOrFixingDate) : that.expiryOrFixingDate != null)
      return false;
    if(FX_FIXING_DATE != null ? !FX_FIXING_DATE.equals(that.FX_FIXING_DATE) : that.FX_FIXING_DATE != null) return false;
    if(LAST_ALLOWABLE_DATE != null ? !LAST_ALLOWABLE_DATE.equals(that.LAST_ALLOWABLE_DATE) : that.LAST_ALLOWABLE_DATE != null)
      return false;
    if(settlementDate != null ? !settlementDate.equals(that.settlementDate) : that.settlementDate != null) return false;
    if(valueAsset != null ? !valueAsset.equals(that.valueAsset) : that.valueAsset != null) return false;
    if(settlementAsset != null ? !settlementAsset.equals(that.settlementAsset) : that.settlementAsset != null)
      return false;
    if(dealtQuantity != null ? !dealtQuantity.equals(that.dealtQuantity) : that.dealtQuantity != null) return false;
    if(dealtMeasure != null ? !dealtMeasure.equals(that.dealtMeasure) : that.dealtMeasure != null) return false;
    if(pegQuantity != null ? !pegQuantity.equals(that.pegQuantity) : that.pegQuantity != null) return false;
    if(PEG_MEASURE != null ? !PEG_MEASURE.equals(that.PEG_MEASURE) : that.PEG_MEASURE != null) return false;
    if(REFERENCE_QUOTE_UNIT != null ? !REFERENCE_QUOTE_UNIT.equals(that.REFERENCE_QUOTE_UNIT) : that.REFERENCE_QUOTE_UNIT != null)
      return false;
    if(DEALT_QUOTE_UNIT != null ? !DEALT_QUOTE_UNIT.equals(that.DEALT_QUOTE_UNIT) : that.DEALT_QUOTE_UNIT != null)
      return false;
    if(CONVERSION != null ? !CONVERSION.equals(that.CONVERSION) : that.CONVERSION != null) return false;
    if(STRIKE_RATE != null ? !STRIKE_RATE.equals(that.STRIKE_RATE) : that.STRIKE_RATE != null) return false;
    if(DEALT_VALUE != null ? !DEALT_VALUE.equals(that.DEALT_VALUE) : that.DEALT_VALUE != null) return false;
    if(DEALT_PREMIUM_VALUE != null ? !DEALT_PREMIUM_VALUE.equals(that.DEALT_PREMIUM_VALUE) : that.DEALT_PREMIUM_VALUE != null)
      return false;
    if(PEG_VALUE != null ? !PEG_VALUE.equals(that.PEG_VALUE) : that.PEG_VALUE != null) return false;
    if(PEG_RATE != null ? !PEG_RATE.equals(that.PEG_RATE) : that.PEG_RATE != null) return false;
    if(PEG_CURRENCY != null ? !PEG_CURRENCY.equals(that.PEG_CURRENCY) : that.PEG_CURRENCY != null) return false;
    if(REFERENCE != null ? !REFERENCE.equals(that.REFERENCE) : that.REFERENCE != null) return false;
    if(CLASSIFICATION != null ? !CLASSIFICATION.equals(that.CLASSIFICATION) : that.CLASSIFICATION != null) return false;
    if(CONTRACT_CODE != null ? !CONTRACT_CODE.equals(that.CONTRACT_CODE) : that.CONTRACT_CODE != null) return false;
    if(EXPIRY_FIXING_TIME != null ? !EXPIRY_FIXING_TIME.equals(that.EXPIRY_FIXING_TIME) : that.EXPIRY_FIXING_TIME != null)
      return false;
    if(COM_FIXING_PRICE != null ? !COM_FIXING_PRICE.equals(that.COM_FIXING_PRICE) : that.COM_FIXING_PRICE != null)
      return false;
    if(FX_REFERENCE != null ? !FX_REFERENCE.equals(that.FX_REFERENCE) : that.FX_REFERENCE != null) return false;
    if(FX_FIXING != null ? !FX_FIXING.equals(that.FX_FIXING) : that.FX_FIXING != null) return false;
    if(FX_FIXING_PRICE != null ? !FX_FIXING_PRICE.equals(that.FX_FIXING_PRICE) : that.FX_FIXING_PRICE != null)
      return false;
    if(DEAL_FIXING_PRICE != null ? !DEAL_FIXING_PRICE.equals(that.DEAL_FIXING_PRICE) : that.DEAL_FIXING_PRICE != null)
      return false;
    if(PAY_RECEIVE != null ? !PAY_RECEIVE.equals(that.PAY_RECEIVE) : that.PAY_RECEIVE != null) return false;
    if(PAYMENT != null ? !PAYMENT.equals(that.PAYMENT) : that.PAYMENT != null) return false;
    if(PAYMENT_SIGNED != null ? !PAYMENT_SIGNED.equals(that.PAYMENT_SIGNED) : that.PAYMENT_SIGNED != null) return false;
    if(FIXED_POSITION != null ? !FIXED_POSITION.equals(that.FIXED_POSITION) : that.FIXED_POSITION != null) return false;
    if(FIXED_CURRENCY != null ? !FIXED_CURRENCY.equals(that.FIXED_CURRENCY) : that.FIXED_CURRENCY != null) return false;
    if(FIXED_FACE_VALUE != null ? !FIXED_FACE_VALUE.equals(that.FIXED_FACE_VALUE) : that.FIXED_FACE_VALUE != null)
      return false;
    if(PRICING_METHOD != null ? !PRICING_METHOD.equals(that.PRICING_METHOD) : that.PRICING_METHOD != null) return false;
    if(EXTRAPOLATE != null ? !EXTRAPOLATE.equals(that.EXTRAPOLATE) : that.EXTRAPOLATE != null) return false;
    if(QUOTE_CONVENTION != null ? !QUOTE_CONVENTION.equals(that.QUOTE_CONVENTION) : that.QUOTE_CONVENTION != null)
      return false;
    if(EXERCISE_STYLE != null ? !EXERCISE_STYLE.equals(that.EXERCISE_STYLE) : that.EXERCISE_STYLE != null) return false;
    if(APRA_ID != null ? !APRA_ID.equals(that.APRA_ID) : that.APRA_ID != null) return false;
    if(INFINITY_ORG_ID != null ? !INFINITY_ORG_ID.equals(that.INFINITY_ORG_ID) : that.INFINITY_ORG_ID != null)
      return false;
    if(TRADER != null ? !TRADER.equals(that.TRADER) : that.TRADER != null) return false;
    if(DISTRIBUTOR != null ? !DISTRIBUTOR.equals(that.DISTRIBUTOR) : that.DISTRIBUTOR != null) return false;
    if(BROKER != null ? !BROKER.equals(that.BROKER) : that.BROKER != null) return false;
    if(MARKET_SEGMENT != null ? !MARKET_SEGMENT.equals(that.MARKET_SEGMENT) : that.MARKET_SEGMENT != null) return false;
    if(BUID != null ? !BUID.equals(that.BUID) : that.BUID != null) return false;
    if(TAG != null ? !TAG.equals(that.TAG) : that.TAG != null) return false;
    if(COMMENT != null ? !COMMENT.equals(that.COMMENT) : that.COMMENT != null) return false;
    if(VALUE_ADD != null ? !VALUE_ADD.equals(that.VALUE_ADD) : that.VALUE_ADD != null) return false;
    if(VALUE_ADD_PER_QUOTE_UNIT != null ? !VALUE_ADD_PER_QUOTE_UNIT.equals(that.VALUE_ADD_PER_QUOTE_UNIT) : that.VALUE_ADD_PER_QUOTE_UNIT != null)
      return false;
    if(CEDAR_ID != null ? !CEDAR_ID.equals(that.CEDAR_ID) : that.CEDAR_ID != null) return false;
    if(INDEPENDENT_AMOUNT != null ? !INDEPENDENT_AMOUNT.equals(that.INDEPENDENT_AMOUNT) : that.INDEPENDENT_AMOUNT != null)
      return false;
    if(NEGOTIATED_OPTION_CP != null ? !NEGOTIATED_OPTION_CP.equals(that.NEGOTIATED_OPTION_CP) : that.NEGOTIATED_OPTION_CP != null)
      return false;
    if(MARGIN != null ? !MARGIN.equals(that.MARGIN) : that.MARGIN != null) return false;
    if(PROTEIN != null ? !PROTEIN.equals(that.PROTEIN) : that.PROTEIN != null) return false;
    if(SCREENINGS != null ? !SCREENINGS.equals(that.SCREENINGS) : that.SCREENINGS != null) return false;
    if(MOISTURE != null ? !MOISTURE.equals(that.MOISTURE) : that.MOISTURE != null) return false;
    if(AVERAGE_TYPE != null ? !AVERAGE_TYPE.equals(that.AVERAGE_TYPE) : that.AVERAGE_TYPE != null) return false;
    if(AVERAGE_START_DATE != null ? !AVERAGE_START_DATE.equals(that.AVERAGE_START_DATE) : that.AVERAGE_START_DATE != null)
      return false;
    if(AVEREGE_END_DATE != null ? !AVEREGE_END_DATE.equals(that.AVEREGE_END_DATE) : that.AVEREGE_END_DATE != null)
      return false;
    if(SPLIT_ASSET != null ? !SPLIT_ASSET.equals(that.SPLIT_ASSET) : that.SPLIT_ASSET != null) return false;
    if(LEG_ID != null ? !LEG_ID.equals(that.LEG_ID) : that.LEG_ID != null) return false;
    if(SPREAD != null ? !SPREAD.equals(that.SPREAD) : that.SPREAD != null) return false;
    if(CLOSE_OUT_RATE != null ? !CLOSE_OUT_RATE.equals(that.CLOSE_OUT_RATE) : that.CLOSE_OUT_RATE != null) return false;
    if(CLOSE_OUT_QUNATITY != null ? !CLOSE_OUT_QUNATITY.equals(that.CLOSE_OUT_QUNATITY) : that.CLOSE_OUT_QUNATITY != null)
      return false;
    if(PARENT_TRADE != null ? !PARENT_TRADE.equals(that.PARENT_TRADE) : that.PARENT_TRADE != null) return false;
    return SPLIT_DATE != null ? SPLIT_DATE.equals(that.SPLIT_DATE) : that.SPLIT_DATE == null;

  }

  @Override
  public int hashCode() {
    int result = tradeNo != null ? tradeNo.hashCode() : 0;
    result = 31 * result + (counterparty != null ? counterparty.hashCode() : 0);
    result = 31 * result + (portfolioPath != null ? portfolioPath.hashCode() : 0);
    result = 31 * result + (status != null ? status.hashCode() : 0);
    result = 31 * result + (prevStatus != null ? prevStatus.hashCode() : 0);
    result = 31 * result + (statusUpdateDate != null ? statusUpdateDate.hashCode() : 0);
    result = 31 * result + (asset != null ? asset.hashCode() : 0);
    result = 31 * result + (associatedAsset != null ? associatedAsset.hashCode() : 0);
    result = 31 * result + (product != null ? product.hashCode() : 0);
    result = 31 * result + (productType != null ? productType.hashCode() : 0);
    result = 31 * result + (packageType != null ? packageType.hashCode() : 0);
    result = 31 * result + (parentProductType != null ? parentProductType.hashCode() : 0);
    result = 31 * result + (ORDER_TYPE != null ? ORDER_TYPE.hashCode() : 0);
    result = 31 * result + (buySell != null ? buySell.hashCode() : 0);
    result = 31 * result + (callPut != null ? callPut.hashCode() : 0);
    result = 31 * result + (tradeDate != null ? tradeDate.hashCode() : 0);
    result = 31 * result + (expiryOrFixingDate != null ? expiryOrFixingDate.hashCode() : 0);
    result = 31 * result + (FX_FIXING_DATE != null ? FX_FIXING_DATE.hashCode() : 0);
    result = 31 * result + (LAST_ALLOWABLE_DATE != null ? LAST_ALLOWABLE_DATE.hashCode() : 0);
    result = 31 * result + (settlementDate != null ? settlementDate.hashCode() : 0);
    result = 31 * result + (valueAsset != null ? valueAsset.hashCode() : 0);
    result = 31 * result + (settlementAsset != null ? settlementAsset.hashCode() : 0);
    result = 31 * result + (dealtQuantity != null ? dealtQuantity.hashCode() : 0);
    result = 31 * result + (dealtMeasure != null ? dealtMeasure.hashCode() : 0);
    result = 31 * result + (pegQuantity != null ? pegQuantity.hashCode() : 0);
    result = 31 * result + (PEG_MEASURE != null ? PEG_MEASURE.hashCode() : 0);
    result = 31 * result + (REFERENCE_QUOTE_UNIT != null ? REFERENCE_QUOTE_UNIT.hashCode() : 0);
    result = 31 * result + (DEALT_QUOTE_UNIT != null ? DEALT_QUOTE_UNIT.hashCode() : 0);
    result = 31 * result + (CONVERSION != null ? CONVERSION.hashCode() : 0);
    result = 31 * result + (STRIKE_RATE != null ? STRIKE_RATE.hashCode() : 0);
    result = 31 * result + (DEALT_VALUE != null ? DEALT_VALUE.hashCode() : 0);
    result = 31 * result + (DEALT_PREMIUM_VALUE != null ? DEALT_PREMIUM_VALUE.hashCode() : 0);
    result = 31 * result + (PEG_VALUE != null ? PEG_VALUE.hashCode() : 0);
    result = 31 * result + (PEG_RATE != null ? PEG_RATE.hashCode() : 0);
    result = 31 * result + (PEG_CURRENCY != null ? PEG_CURRENCY.hashCode() : 0);
    result = 31 * result + (REFERENCE != null ? REFERENCE.hashCode() : 0);
    result = 31 * result + (CLASSIFICATION != null ? CLASSIFICATION.hashCode() : 0);
    result = 31 * result + (CONTRACT_CODE != null ? CONTRACT_CODE.hashCode() : 0);
    result = 31 * result + (EXPIRY_FIXING_TIME != null ? EXPIRY_FIXING_TIME.hashCode() : 0);
    result = 31 * result + (COM_FIXING_PRICE != null ? COM_FIXING_PRICE.hashCode() : 0);
    result = 31 * result + (FX_REFERENCE != null ? FX_REFERENCE.hashCode() : 0);
    result = 31 * result + (FX_FIXING != null ? FX_FIXING.hashCode() : 0);
    result = 31 * result + (FX_FIXING_PRICE != null ? FX_FIXING_PRICE.hashCode() : 0);
    result = 31 * result + (DEAL_FIXING_PRICE != null ? DEAL_FIXING_PRICE.hashCode() : 0);
    result = 31 * result + (PAY_RECEIVE != null ? PAY_RECEIVE.hashCode() : 0);
    result = 31 * result + (PAYMENT != null ? PAYMENT.hashCode() : 0);
    result = 31 * result + (PAYMENT_SIGNED != null ? PAYMENT_SIGNED.hashCode() : 0);
    result = 31 * result + (FIXED_POSITION != null ? FIXED_POSITION.hashCode() : 0);
    result = 31 * result + (FIXED_CURRENCY != null ? FIXED_CURRENCY.hashCode() : 0);
    result = 31 * result + (FIXED_FACE_VALUE != null ? FIXED_FACE_VALUE.hashCode() : 0);
    result = 31 * result + (PRICING_METHOD != null ? PRICING_METHOD.hashCode() : 0);
    result = 31 * result + (EXTRAPOLATE != null ? EXTRAPOLATE.hashCode() : 0);
    result = 31 * result + (QUOTE_CONVENTION != null ? QUOTE_CONVENTION.hashCode() : 0);
    result = 31 * result + (EXERCISE_STYLE != null ? EXERCISE_STYLE.hashCode() : 0);
    result = 31 * result + (APRA_ID != null ? APRA_ID.hashCode() : 0);
    result = 31 * result + (INFINITY_ORG_ID != null ? INFINITY_ORG_ID.hashCode() : 0);
    result = 31 * result + (TRADER != null ? TRADER.hashCode() : 0);
    result = 31 * result + (DISTRIBUTOR != null ? DISTRIBUTOR.hashCode() : 0);
    result = 31 * result + (BROKER != null ? BROKER.hashCode() : 0);
    result = 31 * result + (MARKET_SEGMENT != null ? MARKET_SEGMENT.hashCode() : 0);
    result = 31 * result + (BUID != null ? BUID.hashCode() : 0);
    result = 31 * result + (TAG != null ? TAG.hashCode() : 0);
    result = 31 * result + (COMMENT != null ? COMMENT.hashCode() : 0);
    result = 31 * result + (VALUE_ADD != null ? VALUE_ADD.hashCode() : 0);
    result = 31 * result + (VALUE_ADD_PER_QUOTE_UNIT != null ? VALUE_ADD_PER_QUOTE_UNIT.hashCode() : 0);
    result = 31 * result + (CEDAR_ID != null ? CEDAR_ID.hashCode() : 0);
    result = 31 * result + (INDEPENDENT_AMOUNT != null ? INDEPENDENT_AMOUNT.hashCode() : 0);
    result = 31 * result + (NEGOTIATED_OPTION_CP != null ? NEGOTIATED_OPTION_CP.hashCode() : 0);
    result = 31 * result + (MARGIN != null ? MARGIN.hashCode() : 0);
    result = 31 * result + (PROTEIN != null ? PROTEIN.hashCode() : 0);
    result = 31 * result + (SCREENINGS != null ? SCREENINGS.hashCode() : 0);
    result = 31 * result + (MOISTURE != null ? MOISTURE.hashCode() : 0);
    result = 31 * result + (AVERAGE_TYPE != null ? AVERAGE_TYPE.hashCode() : 0);
    result = 31 * result + (AVERAGE_START_DATE != null ? AVERAGE_START_DATE.hashCode() : 0);
    result = 31 * result + (AVEREGE_END_DATE != null ? AVEREGE_END_DATE.hashCode() : 0);
    result = 31 * result + (SPLIT_ASSET != null ? SPLIT_ASSET.hashCode() : 0);
    result = 31 * result + (LEG_ID != null ? LEG_ID.hashCode() : 0);
    result = 31 * result + (SPREAD != null ? SPREAD.hashCode() : 0);
    result = 31 * result + (CLOSE_OUT_RATE != null ? CLOSE_OUT_RATE.hashCode() : 0);
    result = 31 * result + (CLOSE_OUT_QUNATITY != null ? CLOSE_OUT_QUNATITY.hashCode() : 0);
    result = 31 * result + (PARENT_TRADE != null ? PARENT_TRADE.hashCode() : 0);
    result = 31 * result + (SPLIT_DATE != null ? SPLIT_DATE.hashCode() : 0);
    return result;
  }
}
