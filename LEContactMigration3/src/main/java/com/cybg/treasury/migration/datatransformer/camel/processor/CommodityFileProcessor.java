package com.cybg.treasury.migration.datatransformer.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class CommodityFileProcessor implements Processor {
  @Override
  public void process(Exchange exchange) throws Exception {
    String body = exchange.getIn().getBody(String.class);
    body = body.substring(StringUtils.ordinalIndexOf(body, "\n", 8) + 1);
    exchange.getIn().setBody(body);
  }
}
