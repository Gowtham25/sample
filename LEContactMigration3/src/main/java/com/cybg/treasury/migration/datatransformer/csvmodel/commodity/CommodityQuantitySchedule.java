package com.cybg.treasury.migration.datatransformer.csvmodel.commodity;

import org.apache.camel.dataformat.bindy.annotation.DataField;

public class CommodityQuantitySchedule {
  @DataField(pos = 38, columnName = "QuantitySchedule.Quantity") private String quantity;
  @DataField(pos = 39, columnName = "QuantitySchedule.QuoteUnit") private String quoteUnit;
  @DataField(pos = 40, columnName = "QuantitySchedule.PerPeriod") private String quantityPerPeriod;
  @DataField(pos = 41, columnName = "QuantitySchedule.AfterConversion") private String quantityAfterConversion;

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  public void setQuoteUnit(String quoteUnit) {
    this.quoteUnit = quoteUnit;
  }

  public void setQuantityPerPeriod(String quantityPerPeriod) {
    this.quantityPerPeriod = quantityPerPeriod;
  }

  public void setQuantityAfterConversion(String quantityAfterConversion) {
    this.quantityAfterConversion = quantityAfterConversion;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    CommodityQuantitySchedule that = (CommodityQuantitySchedule)o;

    if(quantity != null ? !quantity.equals(that.quantity) : that.quantity != null) return false;
    if(quoteUnit != null ? !quoteUnit.equals(that.quoteUnit) : that.quoteUnit != null) return false;
    if(quantityPerPeriod != null ? !quantityPerPeriod.equals(that.quantityPerPeriod) : that.quantityPerPeriod != null)
      return false;
    return quantityAfterConversion != null ? quantityAfterConversion.equals(that.quantityAfterConversion) : that.quantityAfterConversion == null;

  }

  @Override
  public int hashCode() {
    int result = quantity != null ? quantity.hashCode() : 0;
    result = 31 * result + (quoteUnit != null ? quoteUnit.hashCode() : 0);
    result = 31 * result + (quantityPerPeriod != null ? quantityPerPeriod.hashCode() : 0);
    result = 31 * result + (quantityAfterConversion != null ? quantityAfterConversion.hashCode() : 0);
    return result;
  }
}
