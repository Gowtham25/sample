package com.cybg.treasury.migration.datatransformer;

import java.util.concurrent.CountDownLatch;

import com.cybg.treasury.migration.datatransformer.camel.route.MigrationRouteFinder;
import com.cybg.treasury.migration.datatransformer.config.DataTransformerConfig;
import org.apache.camel.Main;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DataTransformer {
  private static final Logger log = LoggerFactory.getLogger(DataTransformer.class);

  public static void main(String... args) throws Exception {
	  
	  
	  //This line is updated only in master branch
	  
    if(args.length > 0) {
      ApplicationContext context = new AnnotationConfigApplicationContext(DataTransformerConfig.class);

      //register routes
      Main main = context.getBean(Main.class);
      MigrationRouteFinder migrationRouteFinder = context.getBean(MigrationRouteFinder.class);
      RouteBuilder routeBuilder = (RouteBuilder)migrationRouteFinder.find(args[0]);

      if(routeBuilder != null) {
        main.addRouteBuilder(routeBuilder);

        //register completion listener
        CountDownLatch downLatch = context.getBean(CountDownLatch.class);
        new Thread(() -> {
          try {
            downLatch.await();
            log.info("Data transformation has completed. Gracefully Shutting down");
            main.stop();
          } catch(Exception e) {
            log.error("Errors while executing ShutdownListener", e);
          }
        }).start();

        main.run();
      }
    }
  }
}
