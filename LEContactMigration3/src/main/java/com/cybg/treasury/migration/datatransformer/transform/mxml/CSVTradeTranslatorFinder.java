package com.cybg.treasury.migration.datatransformer.transform.mxml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author theo on 13/12/2016.
 */
@Component
public class CSVTradeTranslatorFinder {

  private final List<MxML2CSVTradeTranslator> MxML2CSVTradeTranslators;
  private Map<String, MxML2CSVTradeTranslator> mxMLTrade2CDUFTranslatorMap = new HashMap<>(5);

  @Autowired
  public CSVTradeTranslatorFinder(List<MxML2CSVTradeTranslator> MxML2CSVTradeTranslators) {
    this.MxML2CSVTradeTranslators = MxML2CSVTradeTranslators;
    populateTranslatorMap();
  }

  public MxML2CSVTradeTranslator find(String typology) {
    return mxMLTrade2CDUFTranslatorMap.get(typology);
  }

  @SuppressWarnings("unchecked")
  private void populateTranslatorMap() {
    for(MxML2CSVTradeTranslator mxML2CsvTradeTranslator : MxML2CSVTradeTranslators) {
      List<String> supportedTypologies = mxML2CsvTradeTranslator.getSupportedTypologies();
      supportedTypologies.forEach(typology ->  mxMLTrade2CDUFTranslatorMap.put(typology, mxML2CsvTradeTranslator));
    }
  }
}
