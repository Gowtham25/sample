package com.cybg.treasury.migration.datatransformer.camel.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commoditytradeinput.CommodityTradeInput;
import org.springframework.stereotype.Component;

@Component
class CommodityForwardAverageTradeAggregator {
  private Map<String, CommodityTradeInput> commodityForwardAverageTradeInputMap = new HashMap<>();

  Map<String, CommodityTradeInput> getCommodityForwardAverageTradeInputMap() {
    return commodityForwardAverageTradeInputMap;
  }

  void aggregate(CommodityTradeInput commodityTradeInput) {
    String tradeNo = commodityTradeInput.getTradeNo();
    commodityForwardAverageTradeInputMap.merge(tradeNo, commodityTradeInput, (existingValue, newValue) -> {
      existingValue.setAVERAGE_START_DATE(newValue.getAVERAGE_START_DATE());
      return existingValue;
    });
  }

  List<CommodityTradeInput> getTradeList() {
    return commodityForwardAverageTradeInputMap.values().stream()
             .collect(Collectors.toList());
  }
}
