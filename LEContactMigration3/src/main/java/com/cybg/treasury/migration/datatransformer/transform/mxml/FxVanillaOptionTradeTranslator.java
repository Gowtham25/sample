package com.cybg.treasury.migration.datatransformer.transform.mxml;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.trade.FXVanillaOption;
import com.cybg.treasury.migration.murex.jaxb.model.*;
import org.springframework.stereotype.Component;

/**
 * @author theo on 13/12/2016.
 */
@Component
public class FxVanillaOptionTradeTranslator extends AbstractTradeTranslator<FXVanillaOption> {

  public static final String FXO_VANILLA = "FXO_VANILLA";

  @Override
  public FXVanillaOption translate(String bundleName, Trade trade) {
    FXVanillaOption csvTrade = new FXVanillaOption();
    csvTrade.setExternalReference(getExternalReference(trade));
    csvTrade.setBook(getBook(trade));
    csvTrade.setCounterparty(getCounterparty(trade));
    csvTrade.setBundleName(bundleName);
    csvTrade.setBuySell(getBuySell(trade));
    csvTrade.setNotional(getNotional(trade));
    csvTrade.setTradeDatetime(getTradeDateTime(trade));
    csvTrade.setTradeSettleDate(getTradeSettleDate(trade));
    csvTrade.setStartDate(getStartDate(trade));
    csvTrade.setMaturityDate(getStartDate(trade));
    csvTrade.setTrader(getTrader(trade));
    csvTrade.setSalesPerson(getTrader(trade));
    csvTrade.setComments(getComments(trade));
    //csvTrade.setHolidayCode(getHolidayCode(trade)); //TODO

    //productSpecific
    csvTrade.setExerciseType(getExerciseType(trade));
    csvTrade.setOptionType(getOptionType(trade));

    List<String> currencyPair = getCurrencyPair(trade);
    csvTrade.setPrimaryCurrency(currencyPair.get(0));
    csvTrade.setSecondaryCurrency(currencyPair.get(1));
    csvTrade.setPrimaryAmount(getCurrencyAmount(trade, currencyPair.get(0)));
    csvTrade.setQuotingAmount(getCurrencyAmount(trade, currencyPair.get(1)));
    csvTrade.setStrike(getStrike(trade));
//    csvTrade.setExpiryTimeZone(getExpiryTimeZone(trade)); // TODO
    csvTrade.setSettlementType(getSettlementType(trade));
    csvTrade.setSettlementCurrency(getSettlementCurrency(trade));
    return csvTrade;
  }

  private String getStrike(Trade trade) {
    return getFxVanillaOption(trade).getFxStrike().getExchangeRate().getRate().toString();
  }

  private String getCurrencyAmount(Trade trade, String currency) {
    String currencyAmount;
    if (getFxVanillaOption(trade).getPutCurrencyAmount().getCurrency().equals(currency)){
      currencyAmount = getFxVanillaOption(trade).getPutCurrencyAmount().getAmount().toString();
    } else {
      currencyAmount = getFxVanillaOption(trade).getCallCurrencyAmount().getAmount().toString();
    }
    return currencyAmount;
  }

  private List<String> getCurrencyPair(Trade trade) {
    List<String> currencyPair = new ArrayList<>(2);
    FxQuotation fxQuotation = getFxQuotation(trade);
    String fxQuoteBasis = fxQuotation.getFxQuoteBasis();
    if (fxQuoteBasis.equals(CURRENCY_1_PER_CURRENCY_2)){
      currencyPair.add(fxQuotation.getCurrency1());
      currencyPair.add(fxQuotation.getCurrency2());
    } else {
      currencyPair.add(fxQuotation.getCurrency2());
      currencyPair.add(fxQuotation.getCurrency1());
    }
    return currencyPair;
  }

  private FxQuotation getFxQuotation(Trade trade) {
    return getFxVanillaOption(trade).getFxStrike().getExchangeRate().getFxQuotation();
  }

  private String getExerciseType(Trade trade) {
    return getFxVanillaOption(trade).getOption().getOptionStyle().toUpperCase();
  }

  private String getStartDate(Trade trade) {
    return getFxVanillaOption(trade).getOption().getOptionMaturity().getDate().toString();
  }

  private String getTradeSettleDate(Trade trade) {
    return trade.getTradeBody().getFxOption().getSettlement().getSettlementFlow().getFlow().getDate().toString();
  }

  private String getNotional(Trade trade) {
    String optionType = getOptionType(trade);
    BigDecimal notional;
    if (optionType.equals(PUT)) {
      notional = getFxVanillaOption(trade).getPutCurrencyAmount().getAmount();
    } else {
      notional = getFxVanillaOption(trade).getCallCurrencyAmount().getAmount();
    }
    return notional.toString();
  }

  private String getOptionType(Trade trade) {
    String tradedCurrency = getFxVanillaOption(trade).getTradedCurrency();
    return tradedCurrency.equals(getFxVanillaOption(trade).getPutCurrencyAmount().getCurrency())? PUT : CALL;
  }

  private String getBuySell(Trade trade) {
    OptionHolderReference optionHolderReference = getFxVanillaOption(trade).getOption().getOptionHolderReference();
    return optionHolderReference.getHref().equals(NAB) ? BUY : SELL;
  }

  private FxVanillaOption getFxVanillaOption(Trade trade) {
    return trade.getTradeBody().getFxOption().getFxVanillaOption();
  }

  @Override
  public List<String> getSupportedTypologies() {
    List<String> typologies = new ArrayList<>();
    typologies.add(FXO_VANILLA);
    return typologies;
  }

}
