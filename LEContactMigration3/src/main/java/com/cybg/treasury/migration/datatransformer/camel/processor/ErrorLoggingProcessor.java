package com.cybg.treasury.migration.datatransformer.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author theo on 13/12/2016.
 */
@Component
public class ErrorLoggingProcessor implements Processor {
  private static final Logger logger = LoggerFactory.getLogger(ErrorLoggingProcessor.class);
  private static final String CAMEL_FILE_NAME = "CamelFileName";

  @Override
  public void process(Exchange exchange)  {
    Message in = exchange.getIn();
    String fileName = (String)in.getHeader(CAMEL_FILE_NAME);
    Throwable cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
    logger.error("Exception thrown while processing camel route for file: " + fileName, cause);
  }
}
