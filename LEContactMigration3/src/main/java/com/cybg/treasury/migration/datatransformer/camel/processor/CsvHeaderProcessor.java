package com.cybg.treasury.migration.datatransformer.camel.processor;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author theo on 16/12/2016.
 */
@Component
public class CsvHeaderProcessor implements Processor {

  private final String csvOutputDirectory;

  @Autowired
  public CsvHeaderProcessor(@Value("#{environment['csv.output.dir']}") String csvOutputDirectory) {
    this.csvOutputDirectory = csvOutputDirectory;
  }

  @Override
  public void process(Exchange exchange) throws Exception {
    String fileName = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
    byte[] body = (byte[])exchange.getIn().getBody();
    Path filePath = Paths.get(URI.create(csvOutputDirectory)).resolve(fileName);
    if(Files.exists(filePath)) {
      byte[] stripOutHeaders = stripOutHeaders(body);
      exchange.getIn().setBody(stripOutHeaders);
    }
  }

  private byte[] stripOutHeaders(byte[] body) {
    String bodyString = new String(body);
    int eol = bodyString.indexOf("\n");
    String resultString = bodyString.substring(eol + 1, bodyString.length() - 1);
    return resultString.getBytes();
  }
}
