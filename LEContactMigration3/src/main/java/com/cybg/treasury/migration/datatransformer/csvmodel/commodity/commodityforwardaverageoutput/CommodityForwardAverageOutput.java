package com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commodityforwardaverageoutput;

import com.cybg.treasury.migration.datatransformer.csvmodel.CSVOutput;
import com.cybg.treasury.migration.datatransformer.csvmodel.commodity.*;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.Link;

@CsvRecord(separator = "#", generateHeaderColumns = true)
public class CommodityForwardAverageOutput implements CSVOutput {
  @DataField(pos = 1, columnName = "Action") private String action = "NEW";
  @DataField(pos = 2, columnName = "ExternalReference") private String externalReference;
  @DataField(pos = 3, columnName = "TradeCounterParty") private String counterparty;
  @DataField(pos = 4, columnName = "TradeBook") private String book;
  @DataField(pos = 5, columnName = "TradeSettlementCurrency") private String settlementCurrency;
  @DataField(pos = 6, columnName = "TradeCurrency") private String tradeCurrency;
  @DataField(pos = 7, columnName = "TradeDateTime") private String tradeDate;
  @DataField(pos = 8, columnName = "SettleDate") private String settleDate;
  @DataField(pos = 9, columnName = "StartDate") private String startDate;
  @DataField(pos = 10, columnName = "TraderName") private String traderName;
  @DataField(pos = 11, columnName = "SalesPerson") private String salesPerson;
  @DataField(pos = 12, columnName = "Comment") private String comment;
  @DataField(pos = 13, columnName = "ProductType") private String productType;
  @DataField(pos = 14, columnName = "ProductSubType") private String productSubType;
  @DataField(pos = 15, columnName = "SwapType") private String swapType;
  @DataField(pos = 16, columnName = "PaymentCurrency") private String paymentCurrency;
  @Link private CommoditySwapFloatingLeg floatingLeg;
  @Link private CommoditySwapFixedLeg fixedLeg;
  @Link private CommodityDateSchedule dateSchedule;
  @Link private CommodityQuantitySchedule quantitySchedule;
  @Link private CommodityFXConversion commodityFXConversion;
  @Link private CommodityPaymentSchedule commodityPaymentSchedule;
  @DataField(pos = 57, columnName = "keyword.Portfolio Path") private String portfolioPath;

  public void setAction(String action) {
    this.action = action;
  }

  public void setExternalReference(String externalReference) {
    this.externalReference = externalReference;
  }

  public void setCounterparty(String counterparty) {
    this.counterparty = counterparty;
  }

  public void setBook(String book) {
    this.book = book;
  }

  public void setSettlementCurrency(String settlementCurrency) {
    this.settlementCurrency = settlementCurrency;
  }

  public void setTradeCurrency(String tradeCurrency) {
    this.tradeCurrency = tradeCurrency;
  }

  public void setTradeDate(String tradeDate) {
    this.tradeDate = tradeDate;
  }

  public void setSettleDate(String settleDate) {
    this.settleDate = settleDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public void setTraderName(String traderName) {
    this.traderName = traderName;
  }

  public void setSalesPerson(String salesPerson) {
    this.salesPerson = salesPerson;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public void setProductSubType(String productSubType) {
    this.productSubType = productSubType;
  }

  public void setSwapType(String swapType) {
    this.swapType = swapType;
  }

  public void setPaymentCurrency(String paymentCurrency) {
    this.paymentCurrency = paymentCurrency;
  }

  public void setFloatingLeg(CommoditySwapFloatingLeg floatingLeg) {
    this.floatingLeg = floatingLeg;
  }

  public void setFixedLeg(CommoditySwapFixedLeg fixedLeg) {
    this.fixedLeg = fixedLeg;
  }

  public void setDateSchedule(CommodityDateSchedule dateSchedule) {
    this.dateSchedule = dateSchedule;
  }

  public void setQuantitySchedule(CommodityQuantitySchedule quantitySchedule) {
    this.quantitySchedule = quantitySchedule;
  }

  public void setCommodityFXConversion(CommodityFXConversion commodityFXConversion) {
    this.commodityFXConversion = commodityFXConversion;
  }

  public void setCommodityPaymentSchedule(CommodityPaymentSchedule commodityPaymentSchedule) {
    this.commodityPaymentSchedule = commodityPaymentSchedule;
  }

  public void setPortfolioPath(String portfolioPath) {
    this.portfolioPath = portfolioPath;
  }

  @Override
  public String getFileName() {
    return "COMMODITYSWAP_NEW.csv";
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    CommodityForwardAverageOutput that = (CommodityForwardAverageOutput)o;

    if(action != null ? !action.equals(that.action) : that.action != null) return false;
    if(externalReference != null ? !externalReference.equals(that.externalReference) : that.externalReference != null)
      return false;
    if(counterparty != null ? !counterparty.equals(that.counterparty) : that.counterparty != null) return false;
    if(book != null ? !book.equals(that.book) : that.book != null) return false;
    if(settlementCurrency != null ? !settlementCurrency.equals(that.settlementCurrency) : that.settlementCurrency != null)
      return false;
    if(tradeCurrency != null ? !tradeCurrency.equals(that.tradeCurrency) : that.tradeCurrency != null) return false;
    if(tradeDate != null ? !tradeDate.equals(that.tradeDate) : that.tradeDate != null) return false;
    if(settleDate != null ? !settleDate.equals(that.settleDate) : that.settleDate != null) return false;
    if(startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
    if(traderName != null ? !traderName.equals(that.traderName) : that.traderName != null) return false;
    if(salesPerson != null ? !salesPerson.equals(that.salesPerson) : that.salesPerson != null) return false;
    if(comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
    if(productType != null ? !productType.equals(that.productType) : that.productType != null) return false;
    if(productSubType != null ? !productSubType.equals(that.productSubType) : that.productSubType != null) return false;
    if(swapType != null ? !swapType.equals(that.swapType) : that.swapType != null) return false;
    if(paymentCurrency != null ? !paymentCurrency.equals(that.paymentCurrency) : that.paymentCurrency != null)
      return false;
    if(floatingLeg != null ? !floatingLeg.equals(that.floatingLeg) : that.floatingLeg != null) return false;
    if(fixedLeg != null ? !fixedLeg.equals(that.fixedLeg) : that.fixedLeg != null) return false;
    if(dateSchedule != null ? !dateSchedule.equals(that.dateSchedule) : that.dateSchedule != null) return false;
    if(quantitySchedule != null ? !quantitySchedule.equals(that.quantitySchedule) : that.quantitySchedule != null)
      return false;
    if(commodityFXConversion != null ? !commodityFXConversion.equals(that.commodityFXConversion) : that.commodityFXConversion != null)
      return false;
    if(commodityPaymentSchedule != null ? !commodityPaymentSchedule.equals(that.commodityPaymentSchedule) : that.commodityPaymentSchedule != null)
      return false;
    return portfolioPath != null ? portfolioPath.equals(that.portfolioPath) : that.portfolioPath == null;

  }

  @Override
  public int hashCode() {
    int result = action != null ? action.hashCode() : 0;
    result = 31 * result + (externalReference != null ? externalReference.hashCode() : 0);
    result = 31 * result + (counterparty != null ? counterparty.hashCode() : 0);
    result = 31 * result + (book != null ? book.hashCode() : 0);
    result = 31 * result + (settlementCurrency != null ? settlementCurrency.hashCode() : 0);
    result = 31 * result + (tradeCurrency != null ? tradeCurrency.hashCode() : 0);
    result = 31 * result + (tradeDate != null ? tradeDate.hashCode() : 0);
    result = 31 * result + (settleDate != null ? settleDate.hashCode() : 0);
    result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
    result = 31 * result + (traderName != null ? traderName.hashCode() : 0);
    result = 31 * result + (salesPerson != null ? salesPerson.hashCode() : 0);
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    result = 31 * result + (productType != null ? productType.hashCode() : 0);
    result = 31 * result + (productSubType != null ? productSubType.hashCode() : 0);
    result = 31 * result + (swapType != null ? swapType.hashCode() : 0);
    result = 31 * result + (paymentCurrency != null ? paymentCurrency.hashCode() : 0);
    result = 31 * result + (floatingLeg != null ? floatingLeg.hashCode() : 0);
    result = 31 * result + (fixedLeg != null ? fixedLeg.hashCode() : 0);
    result = 31 * result + (dateSchedule != null ? dateSchedule.hashCode() : 0);
    result = 31 * result + (quantitySchedule != null ? quantitySchedule.hashCode() : 0);
    result = 31 * result + (commodityFXConversion != null ? commodityFXConversion.hashCode() : 0);
    result = 31 * result + (commodityPaymentSchedule != null ? commodityPaymentSchedule.hashCode() : 0);
    result = 31 * result + (portfolioPath != null ? portfolioPath.hashCode() : 0);
    return result;
  }
}
