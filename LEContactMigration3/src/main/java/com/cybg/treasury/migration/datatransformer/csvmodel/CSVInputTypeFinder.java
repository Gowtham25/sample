package com.cybg.treasury.migration.datatransformer.csvmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author theo on 13/12/2016.
 */
@Component
public class CSVInputTypeFinder {

  private final List<CSVInput> csvInputs;
  private Map<String, CSVInput> csvInputMap = new HashMap<>(5);

  @Autowired
  public CSVInputTypeFinder(List<CSVInput> csvInputs) {
    this.csvInputs = csvInputs;
    populateTranslatorMap();
  }

  public CSVInput find(String filename) {
    return csvInputMap.get(filename);
  }

  @SuppressWarnings("unchecked")
  private void populateTranslatorMap() {
    for(CSVInput csvInput : csvInputs) {
      String supportedType = csvInput.getFileName();
      csvInputMap.put(supportedType, csvInput);
    }
  }
}
