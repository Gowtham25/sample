package com.cybg.treasury.migration.datatransformer.csvmodel.commodity;

import org.apache.camel.dataformat.bindy.annotation.DataField;

public class CommodityPaymentSchedule {
  @DataField(pos = 50, columnName = "PaymentSchedule.PaymentDayOrRule") private String paymentDayOrRule;
  @DataField(pos = 51, columnName = "PaymentSchedule.Calendars") private String calendars;
  @DataField(pos = 52, columnName = "PaymentSchedule.PaymentLag") private String paymentLag;
  @DataField(pos = 53, columnName = "PaymentSchedule.LagType") private String lagType;
  @DataField(pos = 54, columnName = "PaymentSchedule.PaymentDayB") private String paymentDayB;
  @DataField(pos = 55, columnName = "PaymentSchedule.PaymentRule") private String paymentRule;
  @DataField(pos = 56, columnName = "PaymentSchedule.DateRoll") private String dateRoll;

  public void setPaymentDayOrRule(String paymentDayOrRule) {
    this.paymentDayOrRule = paymentDayOrRule;
  }

  public void setCalendars(String calendars) {
    this.calendars = calendars;
  }

  public void setPaymentLag(String paymentLag) {
    this.paymentLag = paymentLag;
  }

  public void setLagType(String lagType) {
    this.lagType = lagType;
  }

  public void setPaymentDayB(String paymentDayB) {
    this.paymentDayB = paymentDayB;
  }

  public void setPaymentRule(String paymentRule) {
    this.paymentRule = paymentRule;
  }

  public void setDateRoll(String dateRoll) {
    this.dateRoll = dateRoll;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    CommodityPaymentSchedule that = (CommodityPaymentSchedule)o;

    if(paymentDayOrRule != null ? !paymentDayOrRule.equals(that.paymentDayOrRule) : that.paymentDayOrRule != null)
      return false;
    if(calendars != null ? !calendars.equals(that.calendars) : that.calendars != null) return false;
    if(paymentLag != null ? !paymentLag.equals(that.paymentLag) : that.paymentLag != null) return false;
    if(lagType != null ? !lagType.equals(that.lagType) : that.lagType != null) return false;
    if(paymentDayB != null ? !paymentDayB.equals(that.paymentDayB) : that.paymentDayB != null) return false;
    if(paymentRule != null ? !paymentRule.equals(that.paymentRule) : that.paymentRule != null) return false;
    return dateRoll != null ? dateRoll.equals(that.dateRoll) : that.dateRoll == null;

  }

  @Override
  public int hashCode() {
    int result = paymentDayOrRule != null ? paymentDayOrRule.hashCode() : 0;
    result = 31 * result + (calendars != null ? calendars.hashCode() : 0);
    result = 31 * result + (paymentLag != null ? paymentLag.hashCode() : 0);
    result = 31 * result + (lagType != null ? lagType.hashCode() : 0);
    result = 31 * result + (paymentDayB != null ? paymentDayB.hashCode() : 0);
    result = 31 * result + (paymentRule != null ? paymentRule.hashCode() : 0);
    result = 31 * result + (dateRoll != null ? dateRoll.hashCode() : 0);
    return result;
  }
}
