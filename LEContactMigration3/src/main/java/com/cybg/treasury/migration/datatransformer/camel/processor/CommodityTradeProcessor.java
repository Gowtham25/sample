package com.cybg.treasury.migration.datatransformer.camel.processor;

import java.util.ArrayList;
import java.util.List;

import com.cybg.treasury.migration.datatransformer.csvmodel.commodity.commoditytradeinput.CommodityTradeInput;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class CommodityTradeProcessor implements Processor {
  private CommodityForwardAverageTradeAggregator commodityForwardAverageTradeAggregator;

  @Autowired
  public CommodityTradeProcessor(CommodityForwardAverageTradeAggregator commodityForwardAverageTradeAggregator) {
    this.commodityForwardAverageTradeAggregator = commodityForwardAverageTradeAggregator;
  }

  @Override
  @SuppressWarnings("unchecked")
  public void process(Exchange exchange) throws Exception {
    List<List<CommodityTradeInput>> tradeList = new ArrayList<>();
    List<CommodityTradeInput> commodityForwardTradeList = new ArrayList<>();

    List<CommodityTradeInput> commodityTradeInputs = exchange.getIn().getBody(List.class);
    if(!CollectionUtils.isEmpty(commodityTradeInputs)) {
      for(CommodityTradeInput commodityTradeInput : commodityTradeInputs) {
        if(commodityTradeInput.isCommodityForwardAverageTrade()) {
          commodityForwardAverageTradeAggregator.aggregate(commodityTradeInput);
        } else if(commodityTradeInput.isCommodityForwardTrade()) {
          commodityForwardTradeList.add(commodityTradeInput);
        }
      }
    }

    tradeList.add(commodityForwardAverageTradeAggregator.getTradeList());
    tradeList.add(commodityForwardTradeList);
    exchange.getIn().setBody(tradeList);
  }
}
