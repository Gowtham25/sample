package com.cybg.treasury.migration.datatransformer.csvmodel.commodity;

import org.apache.camel.dataformat.bindy.annotation.DataField;

public class CommoditySwapFloatingLeg {
  @DataField(pos = 17, columnName = "CommoditySwapLeg.SwapDirection") private String swapDirection;
  @DataField(pos = 18, columnName = "CommoditySwapLeg.CommodityType") private String commodityType;
  @DataField(pos = 19, columnName = "CommoditySwapLeg.CommodityResetName") private String commodityResetName;
  @DataField(pos = 20, columnName = "CommoditySwapLeg.FutureContractName") private String futureContractName;
  @DataField(pos = 21, columnName = "CommoditySwapLeg.FixingCalendar") private String fixingCalendar;
  @DataField(pos = 22, columnName = "CommoditySwapLeg.Spread") private String spread;
  @DataField(pos = 23, columnName = "CommoditySwapLeg.Strike") private String strike;
  @DataField(pos = 24, columnName = "CommoditySwapLeg.BuySellUnits") private String buySellUnits;
  @DataField(pos = 25, columnName = "CommoditySwapLeg.ParStrike") private String parStrike;

  public void setSwapDirection(String swapDirection) {
    this.swapDirection = swapDirection;
  }

  public void setCommodityType(String commodityType) {
    this.commodityType = commodityType;
  }

  public void setCommodityResetName(String commodityResetName) {
    this.commodityResetName = commodityResetName;
  }

  public void setFutureContractName(String futureContractName) {
    this.futureContractName = futureContractName;
  }

  public void setFixingCalendar(String fixingCalendar) {
    this.fixingCalendar = fixingCalendar;
  }

  public void setSpread(String spread) {
    this.spread = spread;
  }

  public void setStrike(String strike) {
    this.strike = strike;
  }

  public void setBuySellUnits(String buySellUnits) {
    this.buySellUnits = buySellUnits;
  }

  public void setParStrike(String parStrike) {
    this.parStrike = parStrike;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;

    CommoditySwapFloatingLeg that = (CommoditySwapFloatingLeg)o;

    if(swapDirection != null ? !swapDirection.equals(that.swapDirection) : that.swapDirection != null) return false;
    if(commodityType != null ? !commodityType.equals(that.commodityType) : that.commodityType != null) return false;
    if(commodityResetName != null ? !commodityResetName.equals(that.commodityResetName) : that.commodityResetName != null)
      return false;
    if(futureContractName != null ? !futureContractName.equals(that.futureContractName) : that.futureContractName != null)
      return false;
    if(fixingCalendar != null ? !fixingCalendar.equals(that.fixingCalendar) : that.fixingCalendar != null) return false;
    if(spread != null ? !spread.equals(that.spread) : that.spread != null) return false;
    if(strike != null ? !strike.equals(that.strike) : that.strike != null) return false;
    if(buySellUnits != null ? !buySellUnits.equals(that.buySellUnits) : that.buySellUnits != null) return false;
    return parStrike != null ? parStrike.equals(that.parStrike) : that.parStrike == null;

  }

  @Override
  public int hashCode() {
    int result = swapDirection != null ? swapDirection.hashCode() : 0;
    result = 31 * result + (commodityType != null ? commodityType.hashCode() : 0);
    result = 31 * result + (commodityResetName != null ? commodityResetName.hashCode() : 0);
    result = 31 * result + (futureContractName != null ? futureContractName.hashCode() : 0);
    result = 31 * result + (fixingCalendar != null ? fixingCalendar.hashCode() : 0);
    result = 31 * result + (spread != null ? spread.hashCode() : 0);
    result = 31 * result + (strike != null ? strike.hashCode() : 0);
    result = 31 * result + (buySellUnits != null ? buySellUnits.hashCode() : 0);
    result = 31 * result + (parStrike != null ? parStrike.hashCode() : 0);
    return result;
  }
}
