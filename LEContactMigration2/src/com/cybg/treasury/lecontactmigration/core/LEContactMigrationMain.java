package com.cybg.treasury.lecontactmigration.core;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.cybg.treasury.lecontactmigration.config.LEContactMigrationConfiguration;
import com.cybg.treasury.lecontactmigration.routes.LEContactRoutes;

public class LEContactMigrationMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Getting the context through AnnotationConfigApplicationContext
		//This context helps to retrieve the bean of the desired class
		ApplicationContext applicationContext=new AnnotationConfigApplicationContext(LEContactMigrationConfiguration.class);
		
		//Creating a default camel context
		CamelContext camelContext=new DefaultCamelContext();
		
		try {
			
			//Getting the route LEContactRoutes class instance through the bean
			//Then adding this route to the camelcontext
			camelContext.addRoutes(applicationContext.getBean(LEContactRoutes.class));
			
			//initiating the camelcontext
			camelContext.start();
			
			//waiting for the file to be processed in the backend by the camel
			Thread.sleep(3 * 60 * 1000); // waiting for 3 minutes
			
			//terminating the camelcontext after the desired waiting time
			camelContext.stop();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}finally{
			
			//Closing the application context
			( (AnnotationConfigApplicationContext) applicationContext).close();
			
		}
		
	}

}
