package com.cybg.treasury.lecontactmigration.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

/*
 * This class is created initially for reading CSV Files.
 * But as we use camel-bindy, this class is no longer required
 * 
 */

public class CSVReader {

	private File leContactInput;
	private String[] headers;
	private FileReader fileReader;
	private BufferedReader bufferedReader;
	private int headerCount;
	private int recordsCount;

	public CSVReader(String fileName) {
		// TODO Auto-generated constructor stub
		
		try(Stream<String> lines = Files.lines(Paths.get(fileName))){
			
			headers = lines
			.findFirst()
			.map((firstLine) -> firstLine.split("\\,"))
			.get();
			headerCount = headers.length;
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try(Stream<String> fileLines = Files.lines(Paths.get(fileName))){
			
			recordsCount = (int) fileLines.count();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public int getHeaderCount(){
		
		return headerCount;
	}
	
	
	public int getRecordsCount() {
		
		return recordsCount;
		
	}
	
	public String[] getHeaders() {
		return headers;
	}

	public String[] getValues(String row) {
		// TODO Auto-generated method stub
		
		String tmpRow = row + ",X";
		String[] tmpArray = tmpRow.split(","); 
		
		String[] values = Arrays.copyOf(tmpArray, tmpArray.length-1);
		
		return values;
	}
	
}
