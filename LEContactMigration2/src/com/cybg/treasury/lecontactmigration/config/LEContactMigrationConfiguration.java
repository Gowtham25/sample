package com.cybg.treasury.lecontactmigration.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.cybg.treasury.lecontactmigration.processor.InputFileValidator;
import com.cybg.treasury.lecontactmigration.processor.LEContactProcessor;
import com.cybg.treasury.lecontactmigration.routes.LEContactRoutes;


/*
 * This file acts as configuration file for the LEContact Migration application.
 * The properties file is present in the root directory of the project named as app.properties
 * 
 * 
 * @Configuration
 * This annotation is used to denote that the class annotated with this annotation acts as a 
 * configuration file
 * 
 * 
 * @ComponentScan
 * This annotation is used to provide the places to scan to look for the bean class.
 * Here, basePackageClasses is used which specifies the classes to include in the scan
 * Similarly we can use basePackages and specify the packages in the string format
 * 
 * @PropertySource
 * This annotation is used to specify the properties file to be used by the project
 * 
 */

@Configuration
@ComponentScan(basePackageClasses={LEContactRoutes.class, LEContactProcessor.class, InputFileValidator.class})
@PropertySource("file:///G:/myrepo/sample/LEContactMigration2/app.properties")
public class LEContactMigrationConfiguration {

}
