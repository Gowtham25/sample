package com.cybg.treasury.lecontactmigration.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.cybg.treasury.lecontactmigration.csvmodel.LEContactInput;
import com.cybg.treasury.lecontactmigration.csvmodel.LEContactOutput;
import com.cybg.treasury.lecontactmigration.transformer.LEContactTransformer;

/*
 * 
 * Test Class to test testTransformValues() method of LEContactTransformer Class
 * 
 */

public class LEContactTransformerTest {

	
	private LEContactInput leContactInput;
	private LEContactOutput expectedLEContactOutput;
	private LEContactOutput actualLEContactOutput;
	
	@Before
	public void setUp() {
		
		leContactInput = new LEContactInput(
				"5244811",
				"R&K DRYSDALE-BEW",
				"R&K Drysdale Ltd",
				"United Kingdom",
				"Old Cambus Quarry Cockburnspath BEW   TD 135Ys GB",
				"",
				"",
				"",
				"",
				"Gavin",
				"",
				"Simpson",
				"",
				"Old Cambus Quarry Cockburnspath BEW   TD 135Ys GB",
				"+44 1 368830448",
				"+44 1 368830561",
				"",
				"",
				"gavin@rkdrysdale.co.uk"
			);
		
		expectedLEContactOutput = new LEContactOutput(
				"NEW",
				"R&K DRYSDALE-BEW",
				"ALL",
				"ALL",
				"Mail",
				"ALL",
				"",
				"",
				"",
				"Simpson",
				"Gavin",
				"",
				"Old Cambus Quarry Cockburnspath BEW   TD 135Ys GB",
				"",
				"",
				"",
				"",
				"+44 1 368830448",
				"",
				"+44 1 368830561",
				"",
				"gavin@rkdrysdale.co.uk",
				"",
				"",
				""
			);
		
	}
	
	@Test
	public void testTransformValues() {
		
		actualLEContactOutput = LEContactTransformer.transformValues(leContactInput);
		
		/*
		System.out.println("__"+expectedLEContactOutput.getAction()+"__");
		System.out.println("__"+expectedLEContactOutput.getName()+"__");
		System.out.println("__"+expectedLEContactOutput.getRole()+"__");
		System.out.println("__"+expectedLEContactOutput.getSupportedProduct()+"__");
		System.out.println("__"+expectedLEContactOutput.getContactType()+"__");
		System.out.println("__"+expectedLEContactOutput.getProcessingOrg()+"__");
		System.out.println("__"+expectedLEContactOutput.getEffectiveFrom()+"__");
		System.out.println("__"+expectedLEContactOutput.getEffectiveTo()+"__");
		System.out.println("__"+expectedLEContactOutput.getStaticDataFilter()+"__");
		System.out.println("__"+expectedLEContactOutput.getLastName()+"__");
		System.out.println("__"+expectedLEContactOutput.getFirstName()+"__");
		System.out.println("__"+expectedLEContactOutput.getTitle()+"__");
		System.out.println("__"+expectedLEContactOutput.getAddress()+"__");
		System.out.println("__"+expectedLEContactOutput.getCity()+"__");
		System.out.println("__"+expectedLEContactOutput.getState()+"__");
		System.out.println("__"+expectedLEContactOutput.getZipCode()+"__");
		System.out.println("__"+expectedLEContactOutput.getCountry()+"__");
		System.out.println("__"+expectedLEContactOutput.getPhoneNumber()+"__");
		System.out.println("__"+expectedLEContactOutput.getTelex()+"__");
		System.out.println("__"+expectedLEContactOutput.getFax()+"__");
		System.out.println("__"+expectedLEContactOutput.getSwift()+"__");
		System.out.println("__"+expectedLEContactOutput.getEmail()+"__");
		System.out.println("__"+expectedLEContactOutput.getExternalRef()+"__");
		System.out.println("__"+expectedLEContactOutput.getComments()+"__");
		System.out.println("__"+expectedLEContactOutput.getCodeAddress()+"__");
		
		System.out.println();
		System.out.println();
		
		System.out.println("__"+actualLEContactOutput.getAction()+"__");
		System.out.println("__"+actualLEContactOutput.getName()+"__");
		System.out.println("__"+actualLEContactOutput.getRole()+"__");
		System.out.println("__"+actualLEContactOutput.getSupportedProduct()+"__");
		System.out.println("__"+actualLEContactOutput.getContactType()+"__");
		System.out.println("__"+actualLEContactOutput.getProcessingOrg()+"__");
		System.out.println("__"+actualLEContactOutput.getEffectiveFrom()+"__");
		System.out.println("__"+actualLEContactOutput.getEffectiveTo()+"__");
		System.out.println("__"+actualLEContactOutput.getStaticDataFilter()+"__");
		System.out.println("__"+actualLEContactOutput.getLastName()+"__");
		System.out.println("__"+actualLEContactOutput.getFirstName()+"__");
		System.out.println("__"+actualLEContactOutput.getTitle()+"__");
		System.out.println("__"+actualLEContactOutput.getAddress()+"__");
		System.out.println("__"+actualLEContactOutput.getCity()+"__");
		System.out.println("__"+actualLEContactOutput.getState()+"__");
		System.out.println("__"+actualLEContactOutput.getZipCode()+"__");
		System.out.println("__"+actualLEContactOutput.getCountry()+"__");
		System.out.println("__"+actualLEContactOutput.getPhoneNumber()+"__");
		System.out.println("__"+actualLEContactOutput.getTelex()+"__");
		System.out.println("__"+actualLEContactOutput.getFax()+"__");
		System.out.println("__"+actualLEContactOutput.getSwift()+"__");
		System.out.println("__"+actualLEContactOutput.getEmail()+"__");
		System.out.println("__"+actualLEContactOutput.getExternalRef()+"__");
		System.out.println("__"+actualLEContactOutput.getComments()+"__");
		System.out.println("__"+actualLEContactOutput.getCodeAddress()+"__");
		*/
		
		
		assertEquals(expectedLEContactOutput, actualLEContactOutput);
		
	}
	
	@After
	public void tearDown(){
		
		this.leContactInput = null;
		this.expectedLEContactOutput = null;
		this.actualLEContactOutput = null;
		
	}
	
}
