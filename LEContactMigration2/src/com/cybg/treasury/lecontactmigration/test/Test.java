package com.cybg.treasury.lecontactmigration.test;

import com.cybg.treasury.lecontactmigration.csvmodel.LEContactInput;
import com.cybg.treasury.lecontactmigration.csvmodel.LEContactOutput;
import com.cybg.treasury.lecontactmigration.transformer.LEContactTransformer;

public class Test {

	private static LEContactInput leContactInput;
	private static LEContactOutput expectedLEContactOutput;
	private static LEContactOutput actualLEContactOutput;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		leContactInput = new LEContactInput(
				"122389",
				"CARRS MILLING-CRL",
				"Carr's Group PLC",
				"United Kingdom",
				"Old Croft Stanwix CRL   CA3 9BA GB",
				"",
				"",
				"",
				"",
				"G20_PORT_REC_DISCREPANCIES",
				"",
				"-",
				"",
				"Old Croft Stanwix CRL  CA3 9BA GB",
				"",
				"",
				"",
				"",
				"Ellen.Smith@cmiplc.co.uk"
			);
		
		expectedLEContactOutput = new LEContactOutput(
				"NEW",
				"CARRS MILLING-CRL",
				"ALL",
				"ALL",
				"Mail",
				"ALL",
				"",
				"",
				"",
				"-",
				"G20_PORT_REC_DISCREPANCIES",
				"",
				"Old Croft Stanwix CRL  CA3 9BA GB",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"",
				"Ellen.Smith@cmiplc.co.uk",
				"",
				"",
				""
				
			);
		
		actualLEContactOutput = LEContactTransformer.transformValues(leContactInput);
		
		/*
		System.out.println("_:_ "+actualLEContactOutput.getAction()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getName()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getRole()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getSupportedProduct()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getContactType()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getProcessingOrg()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getEffectiveFrom()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getEffectiveTo()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getStaticDataFilter()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getLastName()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getFirstName()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getTitle()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getAddress()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getCity()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getState()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getZipCode()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getCountry()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getPhoneNumber()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getTelex()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getFax()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getSwift()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getEmail()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getExternalRef()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getComments()+"_");
		System.out.println("_:_ "+actualLEContactOutput.getCodeAddress()+"_");
		System.out.println();
		System.out.println();
		System.out.println("_:_ "+expectedLEContactOutput.getAction()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getName()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getRole()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getSupportedProduct()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getContactType()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getProcessingOrg()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getEffectiveFrom()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getEffectiveTo()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getStaticDataFilter()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getLastName()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getFirstName()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getTitle()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getAddress()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getCity()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getState()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getZipCode()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getCountry()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getPhoneNumber()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getTelex()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getFax()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getSwift()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getEmail()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getExternalRef()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getComments()+"_");
		System.out.println("_:_ "+expectedLEContactOutput.getCodeAddress()+"_");
		*/
		
		System.out.println("Obj eq check : "+expectedLEContactOutput.equals(actualLEContactOutput));
		
	}

}
