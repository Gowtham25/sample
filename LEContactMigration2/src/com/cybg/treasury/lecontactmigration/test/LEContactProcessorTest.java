package com.cybg.treasury.lecontactmigration.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;

import com.cybg.treasury.lecontactmigration.csvmodel.LEContactInput;
import com.cybg.treasury.lecontactmigration.csvmodel.LEContactOutput;
import com.cybg.treasury.lecontactmigration.processor.LEContactProcessor;

/*
 * 
 * Test Class to test process() method of LEContactProcessor Class
 * 
 */

public class LEContactProcessorTest {

	private LEContactProcessor leContactProcessor;
	private List<LEContactInput> employeeList;
	private Exchange exchange;
	private List<LEContactOutput> actualLEContactOutputs;
	private List<LEContactOutput> expectedLEContactOutputs;

	@Before
	public void setUp() {
		leContactProcessor = new LEContactProcessor();
		employeeList = getRecordsList();
		exchange = setupExchange(employeeList);
		setExpectedLEContactOutput();
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testProcess()throws Exception{
		
		
			leContactProcessor.process(exchange);
			
			//System.out.println("inside");
			
			String output = exchange.getIn().getBody(String.class);
			
			actualLEContactOutputs = (ArrayList<LEContactOutput>) convertStringToList(output);
			
			if(actualLEContactOutputs == null){
				System.out.println("actualLEContactOutputs is null");
			}
			
			assertEquals(expectedLEContactOutputs.size(), actualLEContactOutputs.size());
			
			assertEquals(expectedLEContactOutputs, actualLEContactOutputs);
			
	
	}
	
	private ArrayList<LEContactOutput> convertStringToList(String output) {
		// TODO Auto-generated method stub
		
		String[] lines = output.split("\\n");
		List<LEContactOutput> tmpLeContactOutputs = new ArrayList<>();
		//System.out.println("Length : "+lines.length);
		
		for(int i=0; i<lines.length; ++i){
			
			String[] tmpcolValues = (lines[i]+",X").split(",");
			
			String[] colValues = Arrays.copyOf(tmpcolValues, tmpcolValues.length-1);
			
			tmpLeContactOutputs.add(new LEContactOutput(colValues[0], colValues[1], colValues[2], colValues[3], colValues[4], colValues[5], colValues[6], colValues[7], colValues[8], colValues[9], colValues[10], colValues[11], colValues[12], colValues[13], colValues[14], colValues[15], colValues[16], colValues[17], colValues[18], colValues[19], colValues[20], colValues[21], colValues[22], colValues[23], colValues[24]));
			
		}
		
		return (ArrayList<LEContactOutput>) tmpLeContactOutputs;
	}
	private List<LEContactInput> getRecordsList() {
		// TODO Auto-generated method stub
		
		List<LEContactInput> leContactInputs = new ArrayList<>();
		
		leContactInputs.add(new LEContactInput("WID","SHORT_NAME","LONG_NAME","DOMICILE_COUNTRY","CLIENT_TRADING_ADDRESS","CLIENT_REGISTERED_ADDRESS","CLIENT_POSTAL_ADDRESS","CONTACT_JOB_TITLE","CONTACT_JOB_FUNCTION","CONTACT_FIRST_NAME","CONTACT_MIDDLE_NAME","CONTACT_LAST_NAME","Product","CONTACT_ADDRESS","CONTACT_PHONE","CONTACT_FAX","SECONDARY_FAX","CONTACT_LOTUS_NOTES","CONTACT_EMAIL"));
		leContactInputs.add(new LEContactInput("122389","CARRS MILLING-CRL","Carr's Group PLC","United Kingdom","Old Croft Stanwix CRL   CA3 9BA GB","","","","","G20_PORT_REC_DISCREPANCIES","","-","","Old Croft Stanwix CRL  CA3 9BA GB","","","","","Ellen.Smith@cmiplc.co.uk"));
		leContactInputs.add(new LEContactInput("122389","CARRS MILLING-CRL","Carr's Group PLC","United Kingdom","Old Croft Stanwix CRL   CA3 9BA GB","","","","","Neil","","Austin","FX_OPTION&FX&COMMODITIES","Old Croft Stanwix CRL  CA3 9BA GB","+44 122 8554632","+44 122 8554601","","","Neil.Austin@cmiplc.co.uk"));
		leContactInputs.add(new LEContactInput("23226","TIPTON COS BS-DUD","TIPTON & COSELEY BUILDING SOCIETY","United Kingdom","70 Owen Street Tipton DUD   DY4 8HE GB","","","","","Documentation","","Team","","70 Owen Street Tipton DUD   DY4 8HE GB","+44 121 5214061","","","","confirmation@thetipton.co.uk"));
		leContactInputs.add(new LEContactInput("5244811","R&K DRYSDALE-BEW","R&K Drysdale Ltd","United Kingdom","Old Cambus Quarry Cockburnspath BEW   TD 135Ys GB","NULL","NULL"," "," ","Gavin"," ","Simpson","NULL","Old Cambus Quarry Cockburnspath BEW   TD 135Ys GB","+44 1 368830448","+44 1 368830561","NULL","NULL","gavin@rkdrysdale.co.uk"));
		
		return leContactInputs;
	}
	
	public void tearDown() {
		leContactProcessor = null;
		employeeList = null;
		exchange = null;
		actualLEContactOutputs = null;
		expectedLEContactOutputs = null;
	}
	
	private void setExpectedLEContactOutput() {
		// TODO Auto-generated method stub
		
		expectedLEContactOutputs = new ArrayList<>();
		expectedLEContactOutputs.add(new LEContactOutput("Action","Name","Role","SupportedProduct","ContactType","ProcessingOrg","EffectiveFrom","EffectiveTo","StaticDataFilter","LastName","FirstName","Title","Address","City","State","ZipCode","Country","PhoneNumber","Telex","Fax","Swift","Email","ExternalRef","Comments","CodeAddress"));
		expectedLEContactOutputs.add(new LEContactOutput("NEW","CARRS MILLING-CRL","ALL","ALL","Mail","ALL","","","","-","G20_PORT_REC_DISCREPANCIES","","Old Croft Stanwix CRL  CA3 9BA GB","","","","","","","","","Ellen.Smith@cmiplc.co.uk","","",""));
		expectedLEContactOutputs.add(new LEContactOutput("NEW","CARRS MILLING-CRL","ALL","ALL","Mail","ALL","","","","Austin","Neil","","Old Croft Stanwix CRL  CA3 9BA GB","","","","","+44 122 8554632","","+44 122 8554601","","Neil.Austin@cmiplc.co.uk","","",""));
		expectedLEContactOutputs.add(new LEContactOutput("NEW","TIPTON COS BS-DUD","ALL","ALL","Mail","ALL","","","","Team","Documentation","","70 Owen Street Tipton DUD   DY4 8HE GB","","","","","+44 121 5214061","","","","confirmation@thetipton.co.uk","","",""));
		expectedLEContactOutputs.add(new LEContactOutput("NEW","R&K DRYSDALE-BEW","ALL","ALL","Mail","ALL","","","","Simpson","Gavin","","Old Cambus Quarry Cockburnspath BEW   TD 135Ys GB","","","","","+44 1 368830448","","+44 1 368830561","","gavin@rkdrysdale.co.uk","","",""));
		
	}
	
	private Exchange setupExchange(List<LEContactInput> employeeList) {
		// TODO Auto-generated method stub
		
		Exchange _exchange = new DefaultExchange(new DefaultCamelContext());
		
		_exchange.getIn().setBody(employeeList);
		
		return _exchange;
		
	}
	
}
