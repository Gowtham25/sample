package com.cybg.treasury.lecontactmigration.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.cybg.treasury.lecontactmigration.csvmodel.LEContactInput;
import com.cybg.treasury.lecontactmigration.csvmodel.LEContactOutput;
import com.cybg.treasury.lecontactmigration.transformer.LEContactTransformer;

/*
 * 
 * This class acts as a Processor in Camel, which takes the files from the consumer end
 * and processes it inside the process method then, outputs the result to producer end
 * 
 * 
 * @Component
 * This annotation is used to denote that this particular under it acts as a bean.
 * This class instance is got by, getting the bean of this class through Annotation Context
 * 
 * 
 */

@Component
public class LEContactProcessor implements Processor{

	
	private String output;
	private String outputContents;
	private boolean isHeaderRead;
	
	@SuppressWarnings("unchecked")
	/*
	 * (non-Javadoc)
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 * 
	 * The Processor interface has a default method, process() to be overrided in-order
	 * to process the contents in the exchange.
	 * 
	 */
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub

			List<LEContactInput> leContactInputList = (ArrayList<LEContactInput>) exchange.getIn().getBody();
			
			output="";
			outputContents = "";
			isHeaderRead = false;
			
			
			
			leContactInputList.forEach(leContactInput ->{
			//for(LEContactInput leContactInput : leContactInputList){
				
				/*
				System.out.println("wid_id : "+leContactInput.getWid_id());
				System.out.println("short_name : "+leContactInput.getShort_name());
				System.out.println("long_name : "+leContactInput.getLong_name());
				System.out.println("domicile_country : "+leContactInput.getDomicile_country());
				System.out.println("client_trading_address : "+leContactInput.getClient_trading_address());
				System.out.println("client_registered_address : "+leContactInput.getClient_registered_address());
				System.out.println("client_postal_address : "+leContactInput.getClient_postal_address());
				System.out.println("contact_job_title : "+leContactInput.getContact_job_title());
				System.out.println("contact_job_function : "+leContactInput.getContact_job_function());
				System.out.println("contact_first_name : "+leContactInput.getContact_first_name());
				System.out.println("contact_middle_name : "+leContactInput.getContact_middle_name());
				System.out.println("contact_last_name : "+leContactInput.getContact_last_name());
				System.out.println("product : "+leContactInput.getProduct());
				System.out.println("contact_address : "+leContactInput.getContact_address());
				System.out.println("contact_phone : "+leContactInput.getContact_phone());
				System.out.println("contact_fax : "+leContactInput.getContact_fax());
				System.out.println("secondary_fax : "+leContactInput.getSecondary_fax());
				System.out.println("contact_lotus_notes : "+leContactInput.getContact_lotus_notes());
				System.out.println("contact_email : "+leContactInput.getContact_email());
				*/
				
				output = "";
				
				if(!isHeaderRead){
					isHeaderRead = true;
					output = "Action,Name,Role,SupportedProduct,ContactType,ProcessingOrg,EffectiveFrom,EffectiveTo,StaticDataFilter,LastName,FirstName,Title,Address,City,State,ZipCode,Country,PhoneNumber,Telex,Fax,Swift,Email,ExternalRef,Comments,CodeAddress";
					outputContents += output + "\n";
					//continue; //as header is processed, continue with the values(i.e the records)
				}else{
				
				LEContactOutput leContactOutput = LEContactTransformer.transformValues(leContactInput);
				
				output += leContactOutput.getAction()==null ? "" : leContactOutput.getAction()+",";
				output += leContactOutput.getName()==null ? "" : leContactOutput.getName()+",";
				output += leContactOutput.getRole()==null ? "" : leContactOutput.getRole()+",";
				output += leContactOutput.getSupportedProduct()==null ? "" : leContactOutput.getSupportedProduct()+",";
				output += leContactOutput.getContactType()==null ? "" : leContactOutput.getContactType()+",";
				output += leContactOutput.getProcessingOrg()==null ? "" : leContactOutput.getProcessingOrg()+",";
				output += leContactOutput.getEffectiveFrom()==null ? "" : leContactOutput.getEffectiveFrom()+",";
				output += leContactOutput.getEffectiveTo()==null ? "" : leContactOutput.getEffectiveTo()+",";
				output += leContactOutput.getStaticDataFilter()==null ? "" : leContactOutput.getStaticDataFilter()+",";
				output += leContactOutput.getLastName()==null ? "" : leContactOutput.getLastName()+",";
				output += leContactOutput.getFirstName()==null ? "" : leContactOutput.getFirstName()+",";
				output += leContactOutput.getTitle()==null ? "" : leContactOutput.getTitle()+",";
				output += leContactOutput.getAddress()==null ? "" : leContactOutput.getAddress()+",";
				output += leContactOutput.getCity()==null ? "" : leContactOutput.getCity()+",";
				output += leContactOutput.getState()==null ? "" : leContactOutput.getState()+",";
				output += leContactOutput.getZipCode()==null ? "" : leContactOutput.getZipCode()+",";
				output += leContactOutput.getCountry()==null ? "" : leContactOutput.getCountry()+",";
				output += leContactOutput.getPhoneNumber()==null ? "" : leContactOutput.getPhoneNumber()+",";
				output += leContactOutput.getTelex()==null ? "" : leContactOutput.getTelex()+",";
				output += leContactOutput.getFax()==null ? "" : leContactOutput.getFax()+",";
				output += leContactOutput.getSwift()==null ? "" : leContactOutput.getSwift()+",";
				output += leContactOutput.getEmail()==null ? "" : leContactOutput.getEmail()+",";
				output += leContactOutput.getExternalRef()==null ? "" : leContactOutput.getExternalRef()+",";
				output += leContactOutput.getComments()==null ? "" : leContactOutput.getComments()+",";
				output += leContactOutput.getCodeAddress()==null ? "" : leContactOutput.getCodeAddress()+",";
				
				output = output.substring(0, output.length()-1);
				
				outputContents += output + "\n"; 
				}
				
			});
			
			exchange.getIn().setBody(outputContents);
						
		
	}

}
