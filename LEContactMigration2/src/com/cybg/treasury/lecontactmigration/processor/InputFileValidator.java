package com.cybg.treasury.lecontactmigration.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class InputFileValidator implements Processor {

	@SuppressWarnings("unchecked")
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		String output = exchange.getIn().getBody(String.class);
		
		String[] lines = output.split("\\n");
		
		String tmpHeader = lines[0] + ",X";
		String[] tmpHeaderValues = tmpHeader.split(",");
		int tmpHeaderCount = tmpHeaderValues.length;
		
		int headerCount = tmpHeaderCount-1;
		int recordValuesCount = 0;
		int i;
		boolean isValidCSV = true;
		
		//System.out.println("SOF---Header Count : "+headerCount);
		
		for(i=1;i<lines.length;i++){
			
			String tmpRecord = lines[i] + ",X";
			String[] tmpRecordValues = tmpRecord.split(",");
			recordValuesCount = tmpRecordValues.length-1;
			
			//System.out.println("Rec #"+i+" recordValuesCount"+recordValuesCount);
			
			if( headerCount != recordValuesCount ){
				isValidCSV = false;
				break;
			}
			
		}
		
		//System.out.println("EOF---Record Count : "+ recordValuesCount );
		
		if( !isValidCSV ){
			throw new Exception("Invalid CSV File Passed. Record #"+i+" Column count mismatch as "+recordValuesCount
					+" values are only there. But Header Fields are "+headerCount);
		}
		

	}

}
