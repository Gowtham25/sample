package com.cybg.treasury.lecontactmigration.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/*
 * 
 * Error logging class for camel
 */

@Component
public class ErrorLoggingProcessor implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(ErrorLoggingProcessor.class); 
	private static final String CAMEL_FILE_NAME = "CamelFileName";

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		Message in = exchange.getIn();
		String fileName = (String) in.getHeader(CAMEL_FILE_NAME );
		Throwable cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
		Exception exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
		logger.error("Exception thrown while processing camel route for file: "+fileName, cause);
		logger.error("Error Message : "+exception.getMessage());
		
	}

}
