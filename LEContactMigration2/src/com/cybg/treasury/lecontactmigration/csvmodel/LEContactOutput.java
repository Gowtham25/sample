package com.cybg.treasury.lecontactmigration.csvmodel;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/*
 * Simple Input POJO class LE Contacts Output
 * 
 */

@CsvRecord(separator=",")
public class LEContactOutput {

	@DataField(pos = 1) private String Action = "NEW"; //Mandatory
	@DataField(pos = 2) private String Name; //Mandatory
	@DataField(pos = 3) private String Role = "ALL"; //Mandatory
	@DataField(pos = 4) private String SupportedProduct = "ALL"; //Mandatory
	@DataField(pos = 5) private String ContactType = "Mail"; //Mandatory
	@DataField(pos = 6) private String ProcessingOrg = "ALL"; //Mandatory
	@DataField(pos = 7) private String EffectiveFrom = "";
	@DataField(pos = 8) private String EffectiveTo = "";
	@DataField(pos = 9) private String StaticDataFilter = "";
	@DataField(pos = 10) private String LastName = "";
	@DataField(pos = 11) private String FirstName = "";
	@DataField(pos = 12) private String Title = "";
	@DataField(pos = 13) private String Address = "";
	@DataField(pos = 14) private String City = "";
	@DataField(pos = 15) private String State = "";
	@DataField(pos = 16) private String ZipCode = "";
	@DataField(pos = 17) private String Country = "";
	@DataField(pos = 18) private String PhoneNumber = "";
	@DataField(pos = 19) private String Telex = "";
	@DataField(pos = 20) private String Fax = "";
	@DataField(pos = 21) private String Swift = "";
	@DataField(pos = 22) private String Email = "";
	@DataField(pos = 23) private String ExternalRef = "";
	@DataField(pos = 24) private String Comments = "";
	@DataField(pos = 25) private String CodeAddress = "";
	
	
	public LEContactOutput() {
		// TODO Auto-generated constructor stub
	}
	
	//Constructor used for testing purposes
	public LEContactOutput(String action, String name, String role,
			String supportedProduct, String contactType, String processingOrg,
			String effectiveFrom, String effectiveTo, String staticDataFilter,
			String lastName, String firstName, String title, String address,
			String city, String state, String zipCode, String country,
			String phoneNumber, String telex, String fax, String swift,
			String email, String externalRef, String comments,
			String codeAddress) {
		
		Action = action;
		Name = name;
		Role = role;
		SupportedProduct = supportedProduct;
		ContactType = contactType;
		ProcessingOrg = processingOrg;
		EffectiveFrom = effectiveFrom;
		EffectiveTo = effectiveTo;
		StaticDataFilter = staticDataFilter;
		LastName = lastName;
		FirstName = firstName;
		Title = title;
		Address = address;
		City = city;
		State = state;
		ZipCode = zipCode;
		Country = country;
		PhoneNumber = phoneNumber;
		Telex = telex;
		Fax = fax;
		Swift = swift;
		Email = email;
		ExternalRef = externalRef;
		Comments = comments;
		CodeAddress = codeAddress;
	}
	public String getAction() {
		return Action;
	}
	public void setAction(String action) {
		Action = action;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getRole() {
		return Role;
	}
	public void setRole(String role) {
		Role = role;
	}
	public String getSupportedProduct() {
		return SupportedProduct;
	}
	public void setSupportedProduct(String supportedProduct) {
		SupportedProduct = supportedProduct;
	}
	public String getContactType() {
		return ContactType;
	}
	public void setContactType(String contactType) {
		ContactType = contactType;
	}
	public String getProcessingOrg() {
		return ProcessingOrg;
	}
	public void setProcessingOrg(String processingOrg) {
		ProcessingOrg = processingOrg;
	}
	public String getEffectiveFrom() {
		return EffectiveFrom;
	}
	public void setEffectiveFrom(String effectiveFrom) {
		EffectiveFrom = effectiveFrom;
	}
	public String getEffectiveTo() {
		return EffectiveTo;
	}
	public void setEffectiveTo(String effectiveTo) {
		EffectiveTo = effectiveTo;
	}
	public String getStaticDataFilter() {
		return StaticDataFilter;
	}
	public void setStaticDataFilter(String staticDataFilter) {
		StaticDataFilter = staticDataFilter;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	public String getTelex() {
		return Telex;
	}
	public void setTelex(String telex) {
		Telex = telex;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getSwift() {
		return Swift;
	}
	public void setSwift(String swift) {
		Swift = swift;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getExternalRef() {
		return ExternalRef;
	}
	public void setExternalRef(String externalRef) {
		ExternalRef = externalRef;
	}
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		Comments = comments;
	}
	public String getCodeAddress() {
		return CodeAddress;
	}
	public void setCodeAddress(String codeAddress) {
		CodeAddress = codeAddress;
	}
	
	
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		
		int result = this.Action != null ? this.Action.hashCode() : 0;
		result = 13 * result + ( (this.Address != null) ? this.Address.hashCode() : 0);
		result = 13 * result + ( (this.City != null) ? this.City.hashCode() : 0);
		result = 13 * result + ( (this.CodeAddress != null) ? this.CodeAddress.hashCode() : 0);
		result = 13 * result + ( (this.Comments != null) ? this.Comments.hashCode() : 0);
		result = 13 * result + ( (this.ContactType != null) ? this.ContactType.hashCode() : 0);
		result = 13 * result + ( (this.Country != null) ? this.Country.hashCode() : 0);
		result = 13 * result + ( (this.EffectiveFrom != null) ? this.EffectiveFrom.hashCode() : 0);
		result = 13 * result + ( (this.EffectiveTo != null) ? this.EffectiveTo.hashCode() : 0);
		result = 13 * result + ( (this.Email != null) ? this.Email.hashCode() : 0);
		result = 13 * result + ( (this.ExternalRef != null) ? this.ExternalRef.hashCode() : 0);
		result = 13 * result + ( (this.Fax != null) ? this.Fax.hashCode() : 0);
		result = 13 * result + ( (this.FirstName != null) ? this.FirstName.hashCode() : 0);
		result = 13 * result + ( (this.LastName != null) ? this.LastName.hashCode() : 0);
		result = 13 * result + ( (this.Name != null) ? this.Name.hashCode() : 0);
		result = 13 * result + ( (this.PhoneNumber != null) ? this.PhoneNumber.hashCode() : 0);
		result = 13 * result + ( (this.ProcessingOrg != null) ? this.ProcessingOrg.hashCode() : 0);
		result = 13 * result + ( (this.Role != null) ? this.Role.hashCode() : 0);
		result = 13 * result + ( (this.State != null) ? this.State.hashCode() : 0);
		result = 13 * result + ( (this.StaticDataFilter != null) ? this.StaticDataFilter.hashCode() : 0);
		result = 13 * result + ( (this.SupportedProduct != null) ? this.SupportedProduct.hashCode() : 0);
		result = 13 * result + ( (this.Swift != null) ? this.Swift.hashCode() : 0);
		result = 13 * result + ( (this.Telex != null) ? this.Telex.hashCode() : 0);
		result = 13 * result + ( (this.Title != null) ? this.Title.hashCode() : 0);
		result = 13 * result + ( (this.ZipCode != null) ? this.ZipCode.hashCode() : 0);
		
		return super.hashCode();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		
		LEContactOutput leContactOutput = (LEContactOutput) obj;
		
		if(!this.getAction().equals(leContactOutput.getAction())) return false;
		if(!this.getName().equals(leContactOutput.getName())) return false;
		if(!this.getRole().equals(leContactOutput.getRole())) return false;
		if(!this.getSupportedProduct().equals(leContactOutput.getSupportedProduct())) return false;
		if(!this.getContactType().equals(leContactOutput.getContactType())) return false;
		if(!this.getProcessingOrg().equals(leContactOutput.getProcessingOrg())) return false;
		if(!this.getEffectiveFrom().equals(leContactOutput.getEffectiveFrom())) return false;
		if(!this.getEffectiveTo().equals(leContactOutput.getEffectiveTo())) return false;
		if(!this.getStaticDataFilter().equals(leContactOutput.getStaticDataFilter())) return false;
		if(!this.getLastName().equals(leContactOutput.getLastName())) return false;
		if(!this.getFirstName().equals(leContactOutput.getFirstName())) return false;
		if(!this.getTitle().equals(leContactOutput.getTitle())) return false;
		if(!this.getAddress().equals(leContactOutput.getAddress())) return false;
		if(!this.getCity().equals(leContactOutput.getCity())) return false;
		if(!this.getState().equals(leContactOutput.getState())) return false;
		if(!this.getZipCode().equals(leContactOutput.getZipCode())) return false;
		if(!this.getCountry().equals(leContactOutput.getCountry())) return false;
		if(!this.getPhoneNumber().equals(leContactOutput.getPhoneNumber())) return false;
		if(!this.getTelex().equals(leContactOutput.getTelex())) return false;
		if(!this.getFax().equals(leContactOutput.getFax())) return false;
		if(!this.getSwift().equals(leContactOutput.getSwift())) return false;
		if(!this.getEmail().equals(leContactOutput.getEmail())) return false;
		if(!this.getExternalRef().equals(leContactOutput.getExternalRef())) return false;
		if(!this.getComments().equals(leContactOutput.getComments())) return false;
		if(!this.getCodeAddress().equals(leContactOutput.getCodeAddress())) return false;
		
		return true;
	}

	@Override
	public String toString() {
		return "LEContactOutput [Action=" + Action + ", Name=" + Name
				+ ", Role=" + Role + ", SupportedProduct=" + SupportedProduct
				+ ", ContactType=" + ContactType + ", ProcessingOrg="
				+ ProcessingOrg + ", EffectiveFrom=" + EffectiveFrom
				+ ", EffectiveTo=" + EffectiveTo + ", StaticDataFilter="
				+ StaticDataFilter + ", LastName=" + LastName + ", FirstName="
				+ FirstName + ", Title=" + Title + ", Address=" + Address
				+ ", City=" + City + ", State=" + State + ", ZipCode="
				+ ZipCode + ", Country=" + Country + ", PhoneNumber="
				+ PhoneNumber + ", Telex=" + Telex + ", Fax=" + Fax
				+ ", Swift=" + Swift + ", Email=" + Email + ", ExternalRef="
				+ ExternalRef + ", Comments=" + Comments + ", CodeAddress="
				+ CodeAddress + "]";
	}
	
	
	
	
	
}
