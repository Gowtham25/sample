package com.cybg.treasury.lecontactmigration.routes;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.cybg.treasury.lecontactmigration.config.LEContactMigrationConfiguration;
import com.cybg.treasury.lecontactmigration.processor.ErrorLoggingProcessor;
import com.cybg.treasury.lecontactmigration.processor.InputFileValidator;
import com.cybg.treasury.lecontactmigration.processor.LEContactProcessor;

/*
 * 
 * This class acts as a RouteBuilder in Camel, which is used to define where to pick the files,
 * where to place it(i.e the End-points). Also the process applied to it and exceptions to be called, etc.
 * Most of the core logic written here in this RouteBuilder class are of Java DSL language
 * 
 * @Component
 * This annotation is used to denote that this particular under it acts as a bean.
 * This class instance is got by, getting the bean of this class through Annotation Context
 * 
 * 
 */
@Component
public class LEContactRoutes extends RouteBuilder {
	
	private String inputDir;
	private String outputDir;
	private Processor errorHandler;
	private Processor inputFileValidator;
	
	/*
	 * 
	 * The string dependencies while instantiating this class using the bean is resolved by the Values
	 * annotation provided by the Spring Framework.
	 * 
	 * The values provided by the Values annotation are present in the app.properties file defined in
	 * the PropertySource annotation in Spring Configuration Java file  
	 * 
	 */
	public LEContactRoutes(
			@Value("#{environment['inputDir']}")String inputDir, 
			@Value("#{environment['outputDir']}")String outputDir,
			ErrorLoggingProcessor errorLoggingProcessor,
			InputFileValidator inputFileValidator) {
		super();
		this.inputDir = inputDir;
		this.outputDir = outputDir;
		this.errorHandler = errorLoggingProcessor;
		this.inputFileValidator = inputFileValidator;
	}
	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub

		ApplicationContext context=new AnnotationConfigApplicationContext(LEContactMigrationConfiguration.class);
		LEContactProcessor leContactProcessor=context.getBean(LEContactProcessor.class);
		
		/*
		 * BindyCsvDataFormat to map LEContactInput POJO the CSV file
		 */
		BindyCsvDataFormat bindyCsvDataFormat=new BindyCsvDataFormat(com.cybg.treasury.lecontactmigration.csvmodel.LEContactInput.class);
		
		
		/*
		 * Exception class specified to handle the errors that occurs
		 * during the processing of the file...
		 */
		//onException(Exception.class)
			//.maximumRedeliveries(0)
			//.handled(true)
			//.process(errorHandler)
			//.logStackTrace(true);
			//.stop();
		
		from(inputDir+"?move=.processed&moveFailed=.failed&antInclude=**LEContact_*.csv")
		.log("Polling file : ${file:name}")
		.log("File about to sent to Pre-Processor for validation")
		.process(inputFileValidator)
		.onException(Exception.class)
			//.maximumRedeliveries(0)
			.handled(true)
			.process(errorHandler)
			.end()
		.log("File is validated")
		.log("File about to be unmarshalled")
		.unmarshal(bindyCsvDataFormat)
		.log("File is unmarshalled to POJO Class")
		.log("File about to sent to Processor for processing")
		.process(leContactProcessor)
		.log("File is processed and the contents are transformed")
		.log("File is about to be sent to the Endpoint "+outputDir)
		.to(outputDir+"?fileName=${file:name.noext}_NEW.${file:name.ext}")
		.log("File sent to the Endpoint");
		
		( (AnnotationConfigApplicationContext) context).close();
		
	}

}
