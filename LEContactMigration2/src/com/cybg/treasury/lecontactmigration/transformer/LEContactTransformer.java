package com.cybg.treasury.lecontactmigration.transformer;

import com.cybg.treasury.lecontactmigration.csvmodel.LEContactInput;
import com.cybg.treasury.lecontactmigration.csvmodel.LEContactOutput;


/*
 * 
 * This class is the main part of the Migration process.
 * The mapping from the RDM --> Calypso system lies in this transformValues() method
 * 
 */
public class LEContactTransformer {

	public static LEContactOutput transformValues(LEContactInput leContactInput) {
		// TODO Auto-generated method stub
		
		LEContactOutput leContactOutput=new LEContactOutput();
		
		//leContactOutput.setAction(""); //Hardcoded as "NEW" in the POJO File
		//leContactOutput.setRole(""); //Hardcoded as "ALL" in the POJO File
		//leContactOutput.setSupportedProduct(""); //Hardcoded as "ALL" in the POJO File
		//leContactOutput.setContactType(""); //Hardcoded as "Mail" in the POJO File
		leContactOutput.setName(leContactInput.getShort_name());
		leContactOutput.setFirstName(leContactInput.getContact_first_name());
		leContactOutput.setLastName(leContactInput.getContact_last_name());
		leContactOutput.setAddress(leContactInput.getContact_address());
		leContactOutput.setPhoneNumber(leContactInput.getContact_phone());
		leContactOutput.setFax(leContactInput.getContact_fax());
		leContactOutput.setEmail(leContactInput.getContact_email());
		
		return leContactOutput;
	}

}
