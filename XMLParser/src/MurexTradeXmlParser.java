/**
 *  
 * */  
import java.io.File; 
import java.io.FileInputStream;
import java.io.FileReader; 
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.CopyOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths; 
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory; 
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants; 
import javax.xml.stream.XMLStreamException; 
import javax.xml.stream.XMLStreamReader; 
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Transformer; 
import javax.xml.transform.TransformerException; 
import javax.xml.transform.TransformerFactory; 
import javax.xml.transform.stax.StAXSource; 
import javax.xml.transform.stream.StreamResult;

/** * 
 * @author Gowtham Alaguraj 
 *  
 *  */ 
public class MurexTradeXmlParser { 	
	String xmlPath; 	
	
	public MurexTradeXmlParser(String xmlPath) 
	{ 		
		this.xmlPath = xmlPath; 	
	} 	
	
	/** 	 
	 *  
	 * @param string
	 * 
	 */ 	
	
	public void splitFiles(String targetDirectory) 
	{ 		
		try 
		{ 			
			int counter = 0; 			
			XMLInputFactory xif = XMLInputFactory.newInstance(); 			
			XMLStreamReader xsr = xif.createXMLStreamReader(new FileReader(xmlPath)); 			
			xsr.nextTag(); // Advance to next MxML Tag
			
			TransformerFactory tf = TransformerFactory.newInstance(); 			
			Transformer t = tf.newTransformer(); 			
			while (xsr.nextTag() == XMLStreamConstants.START_ELEMENT) { 				
				File mxmlFilePath = Paths.get(targetDirectory).resolve("Splitted_" + (++counter) + ".xml").toFile(); 				
				t.transform(new StAXSource(xsr), new StreamResult(mxmlFilePath)); 			
			}
			
			categoriseXMLFiles(targetDirectory);
			
			//Log.info(Log.CALYPSOX, "Successfully splitted the XML File, " + Paths.get(xmlPath).getFileName() 					+ " into several chunks.\nChunks stored at " + Paths.get(targetDirectory).getFileName()); 		
		} catch (XMLStreamException | TransformerException | IOException e) { 			
			//Log.error(Log.CALYPSOX, "Error occured in splitting the XML File, " + Paths.get(xmlPath).getFileName() 					+ " into several chunks.\nError Message : " + e.getMessage());
			e.printStackTrace();
		} 	
	}

	private void categoriseXMLFiles(String targetDirectory) throws IOException {
		
		String[] typologies = {"FX_FORWARD","FXO_BARRIER","FX_OPTION","FX_SWAP"};
		
		for(String typology : typologies)
			copyFilesHavingTypology(targetDirectory, typology);
		
	}

	private void copyFilesHavingTypology(String targetDirectory, String typology) throws IOException {
		
		Files.walkFileTree(Paths.get(targetDirectory), new FileVisitor<Path>() {

			@Override
			public FileVisitResult postVisitDirectory(Path path,
					IOException ioException) throws IOException {
				System.out.println("Postvisiting "+path);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(Path path,
					BasicFileAttributes basicFileAttributes) throws IOException {
				System.out.println("Previsiting "+path);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path path, BasicFileAttributes basicFileAttributes)
					throws IOException {
				System.out.println("Visiting the file : "+path);
				
				try {
					StringWriter stringWriter = new StringWriter();
					XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
					XMLEventWriter xmlEventWriter = null;
					
					XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
					XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(path.toString()));
					
					while (xmlEventReader.hasNext()) {
						
						XMLEvent xmlEvent = xmlEventReader.nextEvent();
						
						if(xmlEvent.isStartElement()){
							if(((StartElement)xmlEvent).getName().getLocalPart().equals("typology")){
								//System.out.println("This file contains a typology block");
								stringWriter = new StringWriter();
								xmlEventWriter = outputFactory.createXMLEventWriter(stringWriter);
							}
						}else if(xmlEvent.isEndElement()){
							
							if(((EndElement)xmlEvent).getName().getLocalPart().equals("typology")){
								if(xmlEventWriter != null){
									//System.out.println("typology : __"+stringWriter.toString()+"__");
									if(stringWriter.toString().equals(typology)){
										//System.out.println("This file has the typology given");
										Path dest = Paths.get(targetDirectory+"/..").toRealPath().resolve(typology);
										if(!Files.exists(dest))
											Files.createDirectories(dest);
										
										CopyOption[] copyOptions = new CopyOption[]{
											StandardCopyOption.REPLACE_EXISTING,
											StandardCopyOption.COPY_ATTRIBUTES
										};
										
										Files.copy(path, dest.resolve(path.getFileName()), copyOptions);
									}
								}
							}
							
						}else if(xmlEventWriter != null){
								xmlEventWriter.add(xmlEvent);	
						}
						
					}
					
					if(xmlEventWriter != null)
						xmlEventWriter.close();
					if(xmlEventReader != null)
						xmlEventReader.close();
					
					
				} catch (XMLStreamException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path path, IOException ioException)
					throws IOException {
				System.out.println("File visit failed at visiting the file : "+path);
				return FileVisitResult.TERMINATE;
			}
		});
		
	} 
		
	
	
}
