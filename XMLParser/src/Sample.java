import java.io.IOException;

public class Sample {

	public static void main(String[] args) throws IOException{
		
		String xmlPath = "G:\\XML parser\\sample.xml";
		String targetDirectory = "G:\\XML parser\\splitted";
		MurexTradeXmlParser murexTradeXmlParser = new MurexTradeXmlParser(xmlPath);
		murexTradeXmlParser.splitFiles(targetDirectory);
		
	}

}
